<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'queue', // The component registers own console commands
    ],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@console/migrations',
            'migrationNamespaces' => [
                'yii\queue\db\migrations',
            ],
        ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@runtime/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'ssl://smtp.yandex.ru',
                'username' => 'dgolubenko@credit-history24.ru',
                'password' => '7piAuTha',
                'port' => '465',
            ],
            'useFileTransport' => false,
        ],
        'mail' => [
            'class' => 'console\components\Message',
            'emailTo' => 'service@bki-m.ru',
            'emailFrom' => 'service@bki-m.ru',
        ],
        'amo' => [
            'class' => 'console\components\AmoCrm',
            'amoLogin' => 'dgolubenko@credit-history24.ru',
            'amoApiKey' => 'ed0fa7ba24cf1142148b5a6187dfdecc',
            'subdomain' => 'mikfinans',
        ],
        // Отправка служебных писем через локальный почтовик
        'swiftmailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
    ],
    'params' => $params,
];
