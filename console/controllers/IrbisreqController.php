<?php

namespace console\controllers;

use common\components\irbis\IrbisOrg;
use common\components\irbis\IrbisPeople;
use common\components\irbis\models\UuidResult;
use common\models\Request;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Irbis controller
 */
class IrbisreqController extends Controller
{
    /**
     * @var integer ИНН
     */
    public $inn;
    /**
     * @var integer Регион
     */
    public $regions;
    /**
     * @var string Имя
     */
    public $firstName;
    /**
     * @var string Фамилия
     */
    public $lastName;
    /**
     * @var string Отчество
     */
    public $secondName;
    /**
     * @var integer Серия паспорта
     */
    public $serPassport;
    /**
     * @var string Номер паспорта
     */
    public $numPassport;
    /**
     * @var string Дата рождения дд.мм.гггг
     */
    public $birthDate;
    /**
     * @var bool Флаг проверки учредителей
     */
    public $foundersFlCheck = false;
    /**
     * @var bool Флаг проверки руководителя
     */
    public $managerCheck = false;

    public function options($actionID)
    {
        return [
            'inn',
            'regions',
            'firstName',
            'lastName',
            'secondName',
            'serPassport',
            'numPassport',
            'birthDate',
            'foundersFlCheck',
            'managerCheck'
        ];
    }

    public function beforeAction($action)
    {
        switch ($action->id) {
            case 'legal':
                if (empty($this->inn)) {
                    $this->stdout("Введите inn \n", Console::FG_RED);
                    exit();
                }
                break;
            case 'people':
                if (empty($this->firstName) || empty($this->regions) || empty($this->lastName)) {
                    $this->stdout("Введите firstName, lastName и regions \n", Console::FG_RED);
                    exit();
                }
                break;
//            default:
//                exit();
        }
        return parent::beforeAction($action);
    }

    /**
     * Выписка по юр лицу
     */
    public function actionLegal()
    {
        //Создание запроса
        $model = new \common\models\Request([
            'inn' => $this->inn,
            'type' => \common\models\Request::REQUEST_LEGAL,
        ]);

        if (!$model->save()) {
                throw new \Exception('Не удалось сохранить request');
        }
        $request_id = $model->id;

        $irbis = new IrbisOrg([
            'field' => [
                'inn' => $this->inn,
                'request_id' => $request_id,
                'founders' => false,
                'manager' => false
            ]
        ]);
        //$result = $irbis->getUuid();
        $result = new UuidResult('cb5cb5a3-de6c-4f62-8432-e901368310d7', 4);
        $irbis->legal($result);
    }

    /**
     * Выписка по физ лицу
     */
    public function actionPeople()
    {
        //Создание запроса
        $model = new \common\models\Request([
            'first_name' => $this->firstName,
            'middle_name' => $this->secondName,
            'last_name' => $this->lastName,
            'passport_series' => $this->serPassport,
            'passport_number' => $this->numPassport,
            'region_id' => $this->regions,
            'birth_date' => $this->birthDate,
            'inn' => $this->inn,
            'type' => \common\models\Request::REQUEST_PEOPLE,
        ]);

        if (!$model->save()) {
            throw new \Exception('Не удалось сохранить request');
        }
        $request_id = $model->id;

        $irbis = new IrbisPeople([
            'field' => [
                'request_id' => $request_id,
                'firstName' => $this->firstName,
                'secondName' => $this->secondName,
                'lastName' => $this->lastName,
                'serPassport' => $this->serPassport,
                'numPassport' => $this->numPassport,
                'regions' => $this->regions,
                'birthDate' => $this->birthDate,
                'inn' => $this->inn
            ]
        ]);
        $result = $irbis->getUuid();
        $irbis->individual($result);
    }

    public function actionTest(){
        /** @var Request $res */
        $res = createRequest::find()
            ->andWhere(['id' => 22])
            ->andWhere(['type' => Request::REQUEST_PEOPLE])
            ->andWhere(['status' => Request::STATUS_NOT_READY])
            ->limit(1)
            ->one();
        if (!$res) {
            throw new \Exception('Не найден request_id ');
        }

        // Получение данных из Ирбис
        $irbis = new IrbisPeople([
            'field' => [
                'request_id' => $res->id,
                'firstName' => $res->first_name,
                'secondName' => $res->middle_name,
                'lastName' => $res->last_name,
                'serPassport' => $res->passport_series,
                'numPassport' => $res->passport_number,
                'regions' => $res->region_id,
                'birthDate' => $res->birth_date,
                'inn' => $res->inn
            ]
        ]);
        $result = $irbis->getUuid();
        $id = $irbis->individual($result);
    }
}
