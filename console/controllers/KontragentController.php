<?php
namespace console\controllers;

use console\components\Kontragents;
use yii\console\Controller;

/**
 * Kontragent controller
 */
class KontragentController extends Controller
{

    public function actionIndex(){
        $kontragent = new Kontragents();
        $kontragent->createKontragent();
    }


}
