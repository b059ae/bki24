<?php

namespace console\controllers;

use common\components\irbis\IrbisOrg;
use common\components\irbis\IrbisPeople;
use common\components\irbis\request\JudgeOrgRequest;
use yii\console\Controller;
use yii\helpers\Console;

class RestController extends Controller
{
    public $token = '-DzblNjQZ5aTqs0FRSQYfWBAo5PGh3ym';
    CONST TIMEOUT = 30;

    /**
     * Список регионов
     * @throws \Exception
     */
    public function actionRegion()
    {
        $url = 'http://api.cbr.local/rest/region';

        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_USERPWD, $token);
        curl_setopt($s, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($s);
        if($res === false){
            $code = curl_errno($s);
            $error = curl_error($s);
            throw new \Exception($error, $code);
        }

        $info = curl_getinfo($s);
        curl_close($s);


        if ($info['http_code'] != 200){
            throw new \Exception('Неверный код состояния '.$info['http_code']);
        }

        $json = json_decode($res);
        var_dump($json);
    }

    /**
     * Добавление запроса проверки организации
     * @throws \Exception
     */
    public function actionOrgCreate()
    {
        $url = 'http://api.cbr.local/rest/org/create';
        $post = [
            'inn' => 123456789012,
            'services' => [1,2],// 1 - Ирбис, 2 - НБКИ
        ];

        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_USERPWD, $this->token);
        curl_setopt($s, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($s, CURLOPT_POST, true);
        curl_setopt($s, CURLOPT_POSTFIELDS, http_build_query($post));
        $res = curl_exec($s);
        if($res === false){
            $code = curl_errno($s);
            $error = curl_error($s);
            throw new \Exception($error, $code);
        }

        $info = curl_getinfo($s);
        curl_close($s);


        if ($info['http_code'] != 201){
            throw new \Exception('Неверный код состояния '.$info['http_code']);
        }

        $json = json_decode($res);
        var_dump($json);
    }

    /**
     * Просмотр списка запросов проверки организаций
     * @throws \Exception
     */
    public function actionOrgIndex()
    {
        $url = 'http://api.cbr.local/rest/org';

        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_USERPWD, $this->token);
        curl_setopt($s, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($s);
        if($res === false){
            $code = curl_errno($s);
            $error = curl_error($s);
            throw new \Exception($error, $code);
        }

        $info = curl_getinfo($s);
        curl_close($s);


        if ($info['http_code'] != 200){
            throw new \Exception('Неверный код состояния '.$info['http_code']);
        }

        $json = json_decode($res);
        var_dump($json);
    }

    /**
     * Просмотр полной инфорфмации о запросе проверки организации
     * @throws \Exception
     */
    public function actionOrgView()
    {
        $url = 'http://api.cbr.local/rest/org/view?';
        $post = [
            'id' => 10,
        ];

        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, $url.urldecode(http_build_query($post)));
        curl_setopt($s, CURLOPT_USERPWD, $this->token);
        curl_setopt($s, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($s);
        if($res === false){
            $code = curl_errno($s);
            $error = curl_error($s);
            throw new \Exception($error, $code);
        }

        $info = curl_getinfo($s);
        curl_close($s);


        if ($info['http_code'] != 200){
            throw new \Exception('Неверный код состояния '.$info['http_code']);
        }

        $json = json_decode($res);
        var_dump($json);
    }
}
