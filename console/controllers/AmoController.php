<?php
namespace console\controllers;

use console\components\AmoCrm;
use yii\console\Controller;

/**
 * Amo controller
 */
class AmoController extends Controller
{

    public function actionGet()
    {
        \Yii::$app->amo->integrate();
        \Yii::$app->amo->accountInfo();
    }

    public function actionGetCompany(){
        \Yii::$app->amo->integrate();
        \Yii::$app->amo->getCompany();
    }


}
