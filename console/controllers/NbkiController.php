<?php

namespace console\controllers;

use common\components\irbis\IrbisOrg;
use common\components\irbis\IrbisPeople;
use common\components\irbis\request\JudgeOrgRequest;
use common\components\nbki\NbkiOrg;
use common\components\nbki\NbkiPeople;
use common\models\CreditScore;
use yii\console\Controller;
use yii\helpers\Console;

class NbkiController extends Controller
{

    /**
     * Запрос скоринга НБКИ по физлицу
     * @throws \Exception
     */
    public function actionPeople()
    {
        echo 'отключено';
        return ;
        $nbki = new NbkiPeople(
            22,
            'Александр',
            'Николаевич',
            'Попов',
            '23.05.1988',
            '6008',
            '260184',
            \Yii::$app->nbkiSp,
            \Yii::$app->cryptoPro,
            \Yii::$app->file
        );
        $result = $nbki->scoring();

        return;
    }

    /**
     * Запрос скоринга НБКИ по юрлицу
     * @throws \Exception
     */
    public function actionOrg()
    {
        $nbki = new NbkiOrg(
            22,
            '2310110423',
            '1062310001808',
            'ООО "И-ПИ-СИ-ЭМ-КОНТРАКТОР "ГОРРА"',
            \Yii::$app->nbkiSp,
            \Yii::$app->cryptoPro,
            \Yii::$app->file
        );
        $result = $nbki->scoring();

        return;
    }
}
