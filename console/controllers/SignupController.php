<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;

/**
 *
 * Use
 * php yii signup/index 'login' 'email' 'password'
 * 
 *
 * @package console\controllers
 */

class SignupController extends Controller
{
    /**
     * @param string $login
     * @param string $email
     * @param string $pass
     * @return bool
     */
    public function actionIndex($login, $email, $pass)
    {
        $user = new User();
        $user->username = $login;
        $user->email = $email;
        $user->setPassword($pass);
        $user->generateAuthKey();
        $user->generateAccessToken();

        if (!$user->validate()) {
            print_r($user->getErrors());
            return Controller::EXIT_CODE_ERROR;
        }

        if ($user->save()) {
            echo "{$login} успешно создан.\n";
            print_r($user->attributes);
            return Controller::EXIT_CODE_NORMAL;
        }

        echo "{$login} не удалось сохранить нового пользователя.\n";
        return Controller::EXIT_CODE_ERROR;
    }
}
