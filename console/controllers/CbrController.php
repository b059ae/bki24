<?php
namespace console\controllers;

use console\components\Cbr;
use yii\console\Controller;

/**
 * Cbr controller
 */
class CbrController extends Controller
{

    /**
     * Получений данных из реестра ЦБР
     */
    public function actionGetCbr(){
        $cbr = new Cbr();
        // Выгрузка из МФО
        $cbr->mfo();
        // Выгрузка из кпк
        $cbr->kpk();
        // Выгрузка из сел кпк
        $cbr->selKpk();
    }
}
