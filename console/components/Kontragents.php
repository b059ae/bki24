<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.04.17
 * Time: 12:57
 */

namespace console\components;


use console\components\irbis\IrbisOrg;
use console\models\Mfo;
use console\models\Kpk;
use console\models\SelKpk;
use console\models\Kontragent;

class Kontragents
{
    public function createKontragent(){
        $cbr = new Cbr();
        // Выгрузка из МФО
        $cbr->mfo();
        // Выгрузка из кпк
        $cbr->kpk();
        // Выгрузка из сел кпк
        $cbr->selKpk();
        // Авторизация в амо
        \Yii::$app->amo->integrate();
        // Получение компаний из амо
        \Yii::$app->amo->getCompany();
        // Запрос в Irbis и создание новых контрагентов в амо
        $this->legals();
    }

    /**
     * Получение контрагентов из реестра
     * Провекра их в ирбис
     * Добавление в амосрм
     */
    protected function legals(){
        $time = date('Y-m-d', strtotime("-2 week", time()));
        $count = 0;
        $company = '';
        $mfo = Mfo::find()
            ->andWhere(['>=', 'date_add', $time])
            ->andWhere(['status' => 1])
            ->all();
        foreach ($mfo as $item){
            if ($this->legalRequset($item)){
                $count++;
                $company .= $item->short_name . " \n";
            }
        }
        $kpk = Kpk::find()
            ->andWhere(['>=', 'date_add', $time])
            ->andWhere(['status' => 1])
            ->all();
        foreach ($kpk as $item){
            if ($this->legalRequset($item)){
                $count++;
                $company .= $item->short_name . " \n";
            }
        }
        $skpk = SelKpk::find()
            ->andWhere(['>=', 'date_add', $time])
            ->andWhere(['status' => 1])
            ->all();
        foreach ($skpk as $item){
            if ($this->legalRequset($item)){
                $count++;
                $company .= $item->short_name . " \n";
            }
        }
        print_r(date(DATE_ATOM). "\n" . 'Количество компаний: ' . $count . "\n" . 'Компании: ' . "\n" . $company);
        // Сообщение на почту
        \Yii::$app->mail->emailLocal('Выгрузка компаний из реестра ЦБР',
            date(DATE_ATOM). "\n" . 'Количество компаний: ' . $count . "\n" . 'Компании: ' . "\n" . $company);
    }

    /**
     * Проверка компании в реестрах
     * @param $item
     */
    protected function legalRequset($item){
        $irbis = new IrbisOrg(['inn' => $item->inn]);
        // Проверить организацию в реестре и базе БКИ (если её нет отправить запрос в ирбис и создать новый лид в амосрм)
        $kontragent = Kontragent::find()->where(['inn' => $item->inn])->one();
        if (!$kontragent) {
            $irbis_org_id = $irbis->legal();
            if (!empty($irbis_org_id)) {
                $org = new SendOrg(['irbis_org_id' => $irbis_org_id]);
                $org->sendOrg();
                return true;
            }
        }
        return false;
    }
}