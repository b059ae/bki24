<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.04.17
 * Time: 12:10
 */

namespace console\components;


use console\models\Kontragent;
use Exception;
use Yii;
use yii\base\Component;

class AmoCrm extends Component
{
    /**
     * @var string Login
     */
    public $amoLogin;
    /**
     * @var string Api Key
     */
    public $amoApiKey;
    /**
     * @var string Доменное имя
     */
    public $subdomain;
    /**
     * @var int Код ошибки
     */
    public $code;
    /**
     * Выполнение запроса к amoCrm
     * @param $url
     * @param $field
     * @throws Exception
     */
    protected function setUrl($url, $field = '')
    {
        $dir = Yii::getAlias('@runtime/cookie/');
        if (!file_exists($dir)){
            mkdir($dir);
        }
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $url);
        if (!empty($field)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($field));
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $dir . 'cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, $dir . 'cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $this->code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($this->code != 200 && $this->code != 204) {
            \Yii::warning("Не удалось сделать запрос, код ошибки - " . $this->code, 'amo');
            Yii::$app->cache->delete('amo-cookie');
            \Yii::$app->mail->emailLocal('Ошибка AmoCrm', "Не удалось сделать запрос, код ошибки - " . $this->code);
            print_r(date(DATE_ATOM) . "\n" . "Не удалось сделать запрос AmoCrm, код ошибки - " . $this->code);
            return false;
        }
        Yii::$app->cache->set('amo-cookie', time(), 600);
        return json_decode($out, true);
    }

    /**
     * Интеграция с амо через curl запросы с сохранением куки авторизованного пользователя
     * user_hash берется из профиля
     */
    public function integrate()
    {
        $user = [
            'USER_LOGIN' => $this->amoLogin,
            'USER_HASH' => $this->amoApiKey
        ];
        $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/auth.php?type=json';
        if (!Yii::$app->cache->get('amo-cookie') || Yii::$app->cache->get('amo-cookie') > time() + 600) {
            $this->setUrl($url, $user);
        }
    }


    /**
     * Занести данные по контрагентам из amo в таблицу
     * @throws Exception
     */
    protected function insert($item)
    {
        // Сохранение значений в таблицу
        $values = $this->getElem($item);
        if (!empty($values['inn'])) {
            $check = Kontragent::find()->where(['inn' => $values['inn']])->one();
            if (empty($check)) {
                $kontragent = new Kontragent();
                $kontragent->setAttributes([
                    'short_name' => !empty($item['name']) ? $item['name'] : null,
                    'full_name' => !empty($values['fullName']) ? $values['fullName'] : null,
                    'inn' => !empty($values['inn']) ? $values['inn'] : null,
                    'kpp' => !empty($values['kpp']) ? $values['kpp'] : null,
                    'ogrn' => !empty($values['ogrn']) ? $values['ogrn'] : null,
                    'address' => !empty($values['address']) ? $values['address'] : null,
                    'manager' => !empty($values['manager']) ? $values['manager'] : null,
                    'phone' => !empty($values['phone']) ? $values['phone'] : null,
                    'email' => !empty($values['email']) ? $values['email'] : null,
                    'status' => $kontragent::STATUS_LEGAL_ACTIVE,
                ]);
                if (!$kontragent->save()) {
                    throw new Exception("Не удалось сохранить значения в таблицу kontragent");
                }
            }
        }
    }

    /**
     * Заполненеи полей о компании в массив
     * @param $item
     * @return array
     */
    protected function getElem($item)
    {
        $keys = ['inn' => '309947', 'fullName' => '352805', 'phone' => '303349', 'email' => '303351', 'address' => '303357', 'kpp' => '309949', 'ogrn' => '349049', 'manager' => '306855'];
        $values = [];
        foreach ($item['custom_fields'] as $custom_field) {
            $value = $custom_field['values'][0]['value'];
            $key = array_search($custom_field['id'], $keys);
            $values[$key] = $value;
        }
        return $values;
    }

    /**
     * Get list company from amoCrm
     * @throws Exception
     */
    public function getCompany()
    {
        $limitOffset = 0;
        do {
            $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/company/list?full=Y&limit_rows=499&limit_offset=' . $limitOffset;
            $res = $this->setUrl($url);
            foreach ($res['response']['contacts'] as $item) {
                $this->insert($item);
                $limitOffset++;
            }
        } while (count($res['response']['contacts']) == 499);
    }

    /**
     * Добавление компаний в amoCrm
     * @param $company
     * @throws Exception
     */
    public function addCompany($company)
    {
        $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/company/set';
        $this->setUrl($url, $company);
    }


    /**
     * Add leads to amoCrm
     */
    public function addLead($lead)
    {
        $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/leads/set';
        $this->setUrl($url, $lead);
    }

    /**
     * Get different account id from amoCrm
     * @throws Exception
     */
    public function accountInfo()
    {
        $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/accounts/current';
        print_r($this->setUrl($url));
    }

    /**
     * Add from form BKI
     * @throws Exception
     */
    public function addLeadAndContact()
    {
        $this->integrate();
        // Поля полученные Post'ом
        $name = 'ИМЯ';
        $phone = 'ТЕЛЕФОН';
        $email = 'ПОЧТА';

        //Add lead
        $lead['request']['leads']['add'] = [
            [
                'name' => "Запрос из формы БКИ",
                'status_id' => 14256796,// id - Первичный контакт
                'responsible_user_id' => 1372702,// id - id ответсвенного: Наташа - 1372696, Настя - 1372711, Аня - 1372702
                "tags" => "Запрос из формы БКИ",
            ]
        ];

        $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/leads/set';
        $resLead = $this->setUrl($url, $lead);
        $linkedLead = $resLead['response']['leads']['add'][0]['id'];

        //Create contact
        $lead['request']['contacts']['add'] = [
            [
                'name' => $name,
                'responsible_user_id' => 1372702,
                'tags' => "Запрос из формы БКИ",
                'linked_leads_id' => [
                    $linkedLead
                ],
                'custom_fields' => [
                    [
                        'id' => 303349,
                        'values' =>[
                            [
                                'value' => $phone,
                                'enum' => 'WORK'
                            ]
                        ]
                    ],
                    [
                        'id' => 303351,
                        'values' =>[
                            [
                                'value' => $email,
                                'enum' => 'WORK'
                            ]
                        ]
                    ],
                ]
            ]
        ];

        $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';
        $this->setUrl($url, $lead);
    }
}