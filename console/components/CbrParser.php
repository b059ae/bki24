<?php

namespace console\components;
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.03.17
 * Time: 12:56
 */

use Yii;
use yii\base\Exception;

/**
 * Class CbrParser
 */
class CbrParser
{
    /**
     * Скачивание файла
     * @param $fileName
     */
    public function saveFile($fileName){
        $file = file_get_contents("http://www.cbr.ru/finmarkets/files/supervision/".$fileName.'.xlsx');
        $dir = Yii::getAlias('@runtime/cbr/');
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }
        try {
            file_put_contents($dir . $fileName . '.xlsx', $file);
            // Записываем в массив и возвращаем
            return $this->excelToArray($fileName);
        } catch (Exception $e){
            // Сообщение на почту
            \Yii::$app->mail->emailLocal('Ошибка выгрузки компаний из реестра ЦБР', $e->getMessage());
            print_r(date(DATE_ATOM) . "\n" . $e->getMessage());
            throw $e;
        }
    }


    /**
     * Запись xlsx файла в массив по странично
     * @param $fileName
     */
    protected function excelToArray($fileName){
        require_once 'Classes/PHPExcel.php';
        $dir = Yii::getAlias('@runtime/cbr/');
        try {
            $pExcel = \PHPExcel_IOFactory::load($dir.$fileName.'.xlsx');
            $tables = [];
            // Цикл по листам
            foreach ($pExcel->getWorksheetIterator() as $key => $worksheet) {
                $tables[] = $worksheet->toArray();
            }
            return $tables;
        } catch (Exception $e){
            // Сообщение на почту
            \Yii::$app->mail->emailLocal('Ошибка выгрузки компаний из реестра ЦБР', $e->getMessage());
            print_r(date(DATE_ATOM) . "\n" . $e->getMessage());
            throw $e;
        }
    }
}