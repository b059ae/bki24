<?php

namespace console\components;


use DateTime;
use yii\base\Exception;
use yii\helpers\VarDumper;

class Kpk
{
    /**
     * @var integer ID фирмы
     */
    public $firmId = null;
    /**
     * @var integer регион
     */
    public $region = null;
    /**
     * @var string Дата добавления
     */
    public $dateAdd = null;
    /**
     * @var string Дата исключения
     */
    public $dateExclude = null;
    /**
     * @var string ИНН
     */
    public $inn = null;
    /**
     * @var string ОГРН
     */
    public $ogrn = null;
    /**
     * @var string КПП
     */
    public $kpp = null;
    /**
     * @var string Полное наименование
     */
    public $fullName = null;
    /**
     * @var string Сокращенное наименование фирмы
     */
    public $shortName = null;
    /**
     * @var string Способ образования юр лица
     */
    public $way = null;
    /**
     * @var string Адрес
     */
    public $address = null;
    /**
     * @var string Номер бланка свидетельства
     */
    public $personWithRight = null;
    /**
     * @var integer Добавленная или исключенная фирма
     */
    public $status;

    /**
     *
     * @param $table array
     * @param $status integer статус фирмы
     */
    public function saveFirms($table, $status)
    {
        $table = $this->clearEmpty($table);
        $this->status = $status;
        foreach ($table as $items) {
            $this->addValue($items);
            $this->saveToCbr();
        }
    }

    /**
     * Удаление пустых строк
     * @param $arrayFirms
     * @return array
     */
    protected function clearEmpty($arrayFirms)
    {
        $table = [];
        unset($arrayFirms[0], $arrayFirms[1]);
        foreach ($arrayFirms as $items) {
            $clean = false;
            foreach ($items as $item) {
                if (!empty($item)) {
                    $clean = true;
                }
            }
            if ($clean) {
                $table[] = $items;
            }
        }
        return $table;
    }

    /**
     * Получение значений по каждому юр лицу
     * @param $item array массив значений по фирме
     */
    protected function addValue($item)
    {
        $dateAdd = !empty($item[2]) ? DateTime::createFromFormat("m-d-y", $item[2]) : null;
        $this->dateAdd = isset($dateAdd) ? $dateAdd->format('Y.m.d') : null;
        if (!in_array($this->status, [1,2])) {
            $dateEnd = !empty($item[13]) ? DateTime::createFromFormat("m-d-y", $item[13]) : null;
            $this->dateExclude = isset($dateEnd) ? $dateEnd->format('Y.m.d') : null;
        }
        $this->firmId = !empty($item[0]) ? $item[0] : null;
        $this->region = !empty($item[6]) ? $item[6] : null;
        $this->personWithRight = !empty($item[12]) ? $item[12] : null;
        $this->way = !empty($item[3]) ? $item[3] : null;
        $this->ogrn = !empty($item[8]) ? $item[8] : null;
        $this->inn = !empty($item[9]) ? $item[9] : null;
        $this->kpp = !empty($item[10]) ? $item[10] : null;
        $this->fullName = !empty($item[4]) ? $item[4] : null;
        $this->shortName = !empty($item[5]) ? $item[5] : null;
        $this->address = !empty($item[7]) ? $item[7] : null;
    }

    /**
     * Сохранение в значений в таблицу сbr
     * @throws Exception
     */
    protected function saveToCbr()
    {
        $kpk = new \console\models\Kpk();
        $time = time();
        $itemCbr = \console\models\Kpk::find()
            ->where(['inn' => $this->inn])
            ->one();
        // Если элемент есть, то не сохранять его
        if (!$itemCbr) {
            // Добавление в БД
            $kpk->setAttributes([
                'firm_id' => $this->firmId,
                'region' => $this->region,
                'date_add' => $this->dateAdd,
                'date_exception' => $this->dateExclude,
                'person_with_right' => $this->personWithRight,
                'way' => $this->way,
                'ogrn' => $this->ogrn,
                'inn' => $this->inn,
                'kpp' => $this->kpp,
                'full_name' => $this->fullName,
                'short_name' => $this->shortName,
                'address' => $this->address,
                'status' => $this->status,
                'created_at' => $time,
            ]);
            if (!$kpk->save()) {
                throw new Exception('Запись не добавлена в таблицу kpk, полное наименование фирмы: ' . $this->fullName);
            }
        }
    }
}