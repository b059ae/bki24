<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.04.17
 * Time: 16:52
 */

namespace console\components;

use console\models\IrbisOrg;
use console\models\Kontragent;
use console\models\Kpk;
use console\models\Mfo;
use console\models\SelKpk;
use yii\base\Exception;
use yii\base\Object;

/**
 * Класс для проверки организации полученной из ирбис в рессетре ЦБР
 * Class CheckOrg
 * @package console\components
 */
class CheckOrg extends Object
{
    /**
     * @var integer ИД организации
     */
    public $irbis_org_id;
    /**
     * @var string ИНН
     */
    public $inn;
    /**
     * @var integer Тип реестра
     */
    public $registryType;

    /**
     * Проверка организации
     */
    public function checkOrg(){
        $this->checkCbrOrg();
        $this->checkBkiOrg();
    }

    /**
     * Проверка юр лица в реестре ЦБР
     */
    public function checkCbrOrg()
    {
        $irbisOrg = IrbisOrg::find()->where(['id' => $this->irbis_org_id])->one();
        $this->inn = $irbisOrg->inn;
        $type = IrbisOrg::REGISTRY_TYPE_NOT;
        Mfo::find()->where(['inn' => $this->inn])->one() ? $type = IrbisOrg::REGISTRY_TYPE_MFO : null;
        Kpk::find()->where(['inn' => $this->inn])->one() ? $type = IrbisOrg::REGISTRY_TYPE_KPK : null;
        SelKpk::find()->where(['inn' => $this->inn])->one() ? $type = IrbisOrg::REGISTRY_TYPE_SKPK : null;
        $irbisOrg->setAttribute('registry_type', $type);
        if (!$irbisOrg->save()) {
            throw new Exception("Не удалось сохранить тип реестра");
        }
    }

    /**
     * Проверка юр лица в базе БКИ
     */
    public function checkBkiOrg()
    {
        $status = Kontragent::STATUS_LEGAL_ACTIVE;
        switch ($this->registryType) {
            case IrbisOrg::REGISTRY_TYPE_MFO:
                $org = Mfo::find()->where(['inn' => $this->inn])->one();
                $status = $org->status;
                break;
            case IrbisOrg::REGISTRY_TYPE_KPK:
                $org = Kpk::find()->where(['inn' => $this->inn])->one();
                $status = $org->status;
                break;
            case IrbisOrg::REGISTRY_TYPE_SKPK:
                $org = SelKpk::find()->where(['inn' => $this->inn])->one();
                $status = $org->status;
                break;
            default;
        }
        $kontragent = new Kontragent;
        $kontragent::find()->where(['inn' => $this->inn])->one();
        if ($kontragent) {
            $kontragent->updateAttributes([
                'status' => $status,
            ]);
            if (!$kontragent->save()) {
                throw new Exception("Не удалось сохранить статус контрагента");
            }
        }
    }
}