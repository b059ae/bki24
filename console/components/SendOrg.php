<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.04.17
 * Time: 16:52
 */

namespace console\components;

use console\models\IrbisOrg;
use console\models\ManagerFlOrg;
use Exception;
use yii\base\Object;

/**
 * Класс для отправки новой компании в амо
 * Class CheckOrg
 * @package console\components
 */
class SendOrg extends Object
{
    /**
     * @var integer ИД организации
     */
    public $irbis_org_id;
    /**
     * @var string ИНН
     */
    public $inn;

    /**
     * Отправить лид в амосрм
     */
    public function sendOrg(){
        try {
            // Добавить нового лида и контрагента
            $company = $this->getCompany();
            if ($company) {
                $lead = $this->getLead();
                \Yii::$app->amo->integrate();
                \Yii::$app->amo->addCompany($company);
                \Yii::$app->amo->addLead($lead);
            }
        } catch (Exception $e){
            // Сообщение на почту
            \Yii::$app->mail->emailLocal('Ошибка выгрузки компаний из реестра ЦБР', $e->getMessage());
            print_r(date(DATE_ATOM) . "\n" . $e->getMessage());
            throw $e;
        }
    }

    /**
     * ID ответственных:
     * Наташа - 1372696
     * Настя - 1372711
     * Аня - 1372702
     * @return mixed
     */
    public function getLead(){
        $org = IrbisOrg::find()->where(['id' => $this->irbis_org_id])->one();
        $manager = ManagerFlOrg::find()->where(['id_irbis_org' => $this->irbis_org_id])->one();
        $lead['request']['leads']['add'] = [
            [
                'name' => $org->short_name,
                'status_id' => 14256796,// id - Первичный контакт
                'responsible_user_id' => 1372702,// id ответсвенного пользователя: Наташа - 1372696, Настя - 1372711, Аня - 1372702
                "tags" => "Сделка по компании",
                'custom_fields' => [
                    [
                        'id' => 303349, // id телефона
                        'values' => [
                            'value' => !empty($manager->phones) ? $manager->phones : null,
                            'enum' => 'WORK'
                        ]
                    ],
                    [
                        'id' => 352803, // id наименования
                        'values' => [
                            'value' => $org->short_name,
                        ]
                    ]
                ]
            ]
        ];
        return $lead;
    }

    /**
     * Составление новой компании
     * @return mixed
     */
    public function getCompany()
    {
        $org = IrbisOrg::find()->where(['id' => $this->irbis_org_id])->one();
        $manager = ManagerFlOrg::find()->where(['id_irbis_org' => $this->irbis_org_id])->one();
        $customers = '';
        if (!empty($org->inn)) {
            $customers['request']['contacts']['add'] = [
                [
                    'name' => $org->short_name,
                    "tags" => "Компания из ir-bis",
                    'custom_fields' => [
                        [
                            'id' => 303349,// id телефон
                            'name' => 'Телефон',
                            'values' => [
                                [
                                    'value' => !empty($manager->phones) ? $manager->phones : null,
                                    'enum' => 'MOB'
                                ]
                            ]
                        ],
                        [
                            'id' => 303351,// id email
                            'name' => 'Email',
                            'values' => [
                                [
                                    'value' => !empty($org->email) ? $org->email : null,
                                    'enum' => 'WORK'
                                ]
                            ]
                        ],
                        [
                            'id' => 303357,// id адрес
                            'name' => 'Адрес',
                            'values' => [
                                [
                                    'value' => !empty($org->address) ? $org->address : null,
                                ]
                            ]
                        ],
                        [
                            'id' => 352805,// id полное наименование организации
                            'name' => 'Полное наименование организации',
                            'values' => [
                                [
                                    'value' => !empty($org->full_name) ? $org->full_name : null,
                                ]
                            ]
                        ],
                        [
                            'id' => 309947, // id ИНН
                            'name' => 'ИНН',
                            'values' => [
                                [
                                    'value' => !empty($org->inn) ? $org->inn : null,
                                ]
                            ]
                        ],
                        [
                            'id' => 309949, // id КПП
                            'name' => 'КПП',
                            'values' => [
                                [
                                    'value' => !empty($org->kpp) ? $org->kpp : null,
                                ]
                            ]
                        ],
                        [
                            'id' => 349049, // id ОГРН
                            'name' => 'ОГРН',
                            'values' => [
                                [
                                    'value' => !empty($org->ogrn) ? $org->ogrn : null,
                                ]
                            ]
                        ],
                        [
                            'id' => 306855,// id руководителя
                            'name' => 'Руководитель',
                            'values' => [
                                [
                                    'value' => !empty($manager->fio) ? $manager->fio : null
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }
        return $customers;
    }
}