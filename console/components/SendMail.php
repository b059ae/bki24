<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.04.17
 * Time: 10:46
 */

namespace console\components;


use console\models\IrbisOrg;
use console\models\Kontragent;
use console\models\ManagerFlOrg;
use yii\base\Exception;
use yii\base\Object;

class SendMail extends Object
{
    /**
     * @var string Тема письма
     */
    public $subject;
    /**
     * @var string Тело письма
     */
    public $body;

    /**
     * Отправка письма
     * @throws Exception
     */
    public function sendMail()
    {
        $result = \Yii::$app->mailer->compose()
            ->setFrom('dgolubenko@credit-history24.ru')
            ->setTo('dgolubenko@credit-history24.ru')
            ->setSubject($this->subject)
            ->setTextBody($this->body);
        if (!$result->send()) {
            throw new Exception("Письмо не отправлено");
        }
    }
}