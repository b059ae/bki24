<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.04.17
 * Time: 13:00
 */

namespace console\components;


use console\models\Kpk;
use console\models\SelKpk;
use Exception;

class Cbr
{
    /**
     * Парсинг реестра ЦБР КПК
     */
    public function kpk()
    {
        $fileName = "list_KPK_gov";
        $cbr = new CbrParser();
        $kpk = new \console\components\Kpk();
        try {
            // Скачать файл с сервиса и записывает содержимое в массив
            $tables = $cbr->saveFile($fileName);
            $kpk->saveFirms($tables[1], Kpk::STATUS_LEGAL_ACTIVE);
            $kpk->saveFirms($tables[2], Kpk::STATUS_LEGAL_PROCESS_LIQUIDATED);
            $kpk->saveFirms($tables[3], Kpk::STATUS_LEGAL_LIQUIDATED);
        } catch (Exception $e){
            // Сообщение на почту
            \Yii::$app->mail->emailLocal('Ошибка выгрузки компаний из реестра ЦБР', $e->getMessage());
            print_r(date(DATE_ATOM) . "\n" . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Парсинг реестра ЦБР МФО
     */
    public function mfo()
    {
        $fileName = "list_MFO";
        $cbr = new CbrParser();
        $mfo = new Mfo();
        // Скачать файл с сервиса и записывает содержимое в массив
        try {
            $tables = $cbr->saveFile($fileName);
            // Активные МФО
            $mfo->activeFirms($tables[0]);
            // Исключенные МФО
            $mfo->excludedFirms($tables[1]);
        } catch (Exception $e){
            // Сообщение на почту
            \Yii::$app->mail->emailLocal('Ошибка выгрузки компаний из реестра ЦБР', $e->getMessage());
            print_r(date(DATE_ATOM) . "\n" . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Парсинг реестра ЦБР сельскохозяйственных КПК
     */
    public function selKpk()
    {
        $fileName = "list_skpk";
        $cbr = new CbrParser();
        $skpk = new Skpk();
        try {
            // Скачать файл с сервиса и записывает содержимое в массив
            $tables = $cbr->saveFile($fileName);
            $skpk->saveFirms($tables[0], SelKpk::STATUS_LEGAL_ACTIVE);
            $skpk->saveFirms($tables[1], SelKpk::STATUS_LEGAL_PROCESS_LIQUIDATED);
            $skpk->saveFirms($tables[2], SelKpk::STATUS_LEGAL_LIQUIDATED);
        } catch (Exception $e){
            // Сообщение на почту
            \Yii::$app->mail->emailLocal('Ошибка выгрузки компаний из реестра ЦБР', $e->getMessage());
            print_r(date(DATE_ATOM) . "\n" . $e->getMessage());
            throw $e;
        }
    }
}