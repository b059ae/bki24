<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.05.17
 * Time: 14:08
 */

namespace console\components;


use Yii;
use yii\base\Component;

class Message extends Component
{

    /**
     * @var string Кому
     */
    public $emailTo;
    /**
     * @var string От кого
     */
    public $emailFrom;
    /** Отправка сообщений
     * @param $body
     * @return mixed
     */
    public function emailLocal($subject, $body){
        return Yii::$app->swiftmailer->compose()
            ->setTo($this->emailTo)
            ->setFrom($this->emailFrom)
            ->setSubject($subject)
            ->setTextBody($body)
            ->send();
    }

}