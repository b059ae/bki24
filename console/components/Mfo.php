<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09.03.17
 * Time: 8:48
 */

namespace console\components;


use DateTime;
use yii\base\Exception;
use yii\helpers\VarDumper;

class Mfo
{
    /**
     * @var integer ID фирмы
     */
    public $firmId = null;
    /**
     * @var integer регион
     */
    public $region = null;
    /**
     * @var string Дата добавления
     */
    public $dateAdd = null;
    /**
     * @var string Дата исключения
     */
    public $dateExclude = null;
    /**
     * @var string ИНН
     */
    public $inn = null;
    /**
     * @var string ОГРН
     */
    public $ogrn = null;
    /**
     * @var string Полное наименование
     */
    public $fullName = null;
    /**
     * @var string Сокращенное наименование фирмы
     */
    public $shortName = null;
    /**
     * @var string Вид мфо
     */
    public $typeFirm = null;
    /**
     * @var string Адрес
     */
    public $address = null;
    /**
     * @var string Номер бланка свидетельства
     */
    public $numBlank = null;
    /**
     * @var integer Добавленная или исключенная фирма
     */
    public $status;

    /**
     * Действующие МФО
     * @param $active array
     */
    public function activeFirms($active){
        $table = $this->clearEmpty($active);
        foreach ($table as $items) {
            $this->addValueActive($items, 1);
            $this->saveToCbr();
        }
    }

    /**
     * Исключенные МФО
     * @param $exclude array
     */
    public function excludedFirms($exclude){
        $table = $this->clearEmpty($exclude);
        foreach ($table as $items) {
            $this->addValueExclude($items, 2);
            $this->saveToCbr();
        }
    }

    /**
     * Удаление пустых строк
     * @param $arrayFirms
     * @return array
     */
    protected function clearEmpty($arrayFirms){
        $table = [];
        unset($arrayFirms[0], $arrayFirms[1], $arrayFirms[2]);
        foreach ($arrayFirms as $items){
            $clean = false;
            foreach ($items as $item){
                if (!empty($item)) {
                    $clean = true;
                }
            }
            if ($clean) {
                $table[] = $items;
            }
        }
        return $table;
    }

    /**
     * Получение значений по каждой исключенной фирме
     * @param $item array массив значений по фирме
     * @param $status integer статус фирмы
     */
    protected function addValueExclude($item, $status){
        $dateAdd = !empty($item[6]) ? DateTime::createFromFormat("d.m.Y", $item[6]) : null;
        $dateEnd = !empty($item[7]) ? DateTime::createFromFormat("d.m.Y", $item[7]) : null;
        $this->firmId = !empty($item[0]) ? $item[0] : null;
        $this->region = !empty($item[4]) ? $item[4] : null;
        $this->dateAdd = isset($dateAdd) ? $dateAdd->format('Y.m.d') : null;
        $this->dateExclude = isset($dateEnd) ? $dateEnd->format('Y.m.d') : null;
        $this->numBlank = !empty($item[8]) ? $item[8] : null;
        $this->typeFirm = !empty($item[9]) ? $item[9] : null;
        $this->ogrn = !empty($item[10]) ? $item[10] : null;
        $this->inn = !empty($item[11]) ? $item[11] : null;
        $this->fullName = !empty($item[12]) ? $item[12] : null;
        $this->shortName = !empty($item[13]) ? $item[13] : null;
        $this->address = !empty($item[14]) ? $item[14] : null;
        $this->status = $status;
    }

    /**
     * Получение значений по каждой активной фирме
     * @param $item array массив значений по фирме
     * @param $status integer статус фирмы
     */
    protected function addValueActive($item, $status){
        $dateAdd = !empty($item[6]) ? DateTime::createFromFormat("d.m.Y", $item[6]) : null;
        $this->firmId = !empty($item[0]) ? $item[0] : null;
        $this->region = !empty($item[4]) ? $item[4] : null;
        $this->dateAdd = isset($dateAdd) ? $dateAdd->format('Y.m.d') : null;
        $this->numBlank = !empty($item[7]) ? $item[7] : null;
        $this->typeFirm = !empty($item[8]) ? $item[8] : null;
        $this->ogrn = !empty($item[9]) ? $item[9] : null;
        $this->inn = !empty($item[10]) ? $item[10] : null;
        $this->fullName = !empty($item[11]) ? $item[11] : null;
        $this->shortName = !empty($item[12]) ? $item[12] : null;
        $this->address = !empty($item[13]) ? $item[13] : null;
        $this->status = $status;
    }


    /**
     * Сохранение в значений в таблицу сbr
     * @throws Exception
     */
    protected function saveToCbr(){
        $mfo = new \console\models\Mfo();
        $time = time();
        $itemCbr = \console\models\Mfo::find()
            ->where(['inn' => $this->inn])
            ->one();
        // Если элемент есть, то не сохранять его
        if (!$itemCbr) {
            // Добавление в БД
            $mfo->setAttributes([
                'firm_id' => $this->firmId,
                'region' => $this->region,
                'date_add' => $this->dateAdd,
                'date_exception' => $this->dateExclude,
                'num_blank' => $this->numBlank,
                'type_firm' => $this->typeFirm,
                'ogrn' => $this->ogrn,
                'inn' => $this->inn,
                'full_name' => $this->fullName,
                'short_name' => $this->shortName,
                'address' => $this->address,
                'status' => $this->status,
                'created_at' => $time
            ]);
            if (!$mfo->save()) {
                throw new Exception('Запись не добавлена в таблицу mfo, полное наименование фирмы: ' . $this->fullName);
            }
        }
    }
}