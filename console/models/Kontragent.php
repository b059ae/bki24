<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "kontragent".
 *
 * @property integer $id
 * @property string $full_name Полное наименование организации
 * @property string $short_name Сокращенное наименование организации
 * @property string $address Адрес
 * @property string $ogrn ОГРН
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property integer $status Статус
 * @property string $manager Руководитель
 * @property string $phone Телефон
 * @property string $email Email
 */
class Kontragent extends \yii\db\ActiveRecord
{

    // Юр лицо действет
    const STATUS_LEGAL_ACTIVE = 1;
    // Юр лицо в процессе ликвидации
    const STATUS_LEGAL_PROCESS_LIQUIDATED = 2;
    // Юр лицо ликвидировано
    const STATUS_LEGAL_LIQUIDATED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kontragent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'status', 'ogrn', 'inn', 'kpp', 'manager', 'phone', 'email',
              'full_name', 'short_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Полное наименование',
            'short_name' => 'Сокращенное наименование',
            'address' => 'Адрес',
            'ogrn' => 'ОГРН',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'manager' => 'Руководитель',
            'status' => 'Статус',
            'phone' => 'Телефон',
            'email' => 'Email',
        ];
    }
}
