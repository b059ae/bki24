<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "kpk".
 *
 * @property integer $id
 * @property string $full_name Полное наименование организации
 * @property string $short_name Сокращенное наименование организации
 * @property integer $region Региона
 * @property string $address Адрес
 * @property string $ogrn ОГРН
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property integer $status Статус
 * @property string $date_add Дата добавления в реестр
 * @property string $date_exception Дата исключения из реестра
 * @property string $created_at Дата создания записи
 * @property string $type_firm Тип фирмы
 * @property string $firm_id id фирмы
 * @property string $way Способ образования
 * @property string $person_with_right Ответственное лицо
 *
 */
class Kpk extends \yii\db\ActiveRecord
{
    // Юр лицо действет
    const STATUS_LEGAL_ACTIVE = 1;
    // Юр лицо в процессе ликвидации
    const STATUS_LEGAL_PROCESS_LIQUIDATED = 2;
    // Юр лицо ликвидировано
    const STATUS_LEGAL_LIQUIDATED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['way', 'person_with_right', 'region', 'date_add', 'date_exception', 'created_at', 'ogrn', 'inn', 'kpp', 'status', 'firm_id'], 'safe'],
            [['full_name', 'short_name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firm_id' => 'id фирмы',
            'full_name' => 'Полное наименование',
            'short_name' => 'Сокращенное наименование',
            'region' => 'Код региона',
            'address' => 'Адрес',
            'ogrn' => 'ОГРН',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'status' => 'Статус',
            'date_add' => 'Дата добавления в реестр',
            'date_exception' => 'Дата исключения из реестра',
            'created_at' => 'Дата создания',
            'way' => 'Способ образования юр лица',
            'person_with_right' => 'Ответсвенное лицо',
        ];
    }
}
