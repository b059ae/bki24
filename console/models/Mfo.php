<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "mfo".
 *
 * @property integer $id
 * @property string $full_name Полное наименование организации
 * @property string $short_name Сокращенное наименование организации
 * @property integer $region Региона
 * @property string $address Адрес
 * @property string $ogrn ОГРН
 * @property string $inn ИНН
 * @property integer $status Статус
 * @property string $date_add Дата добавления в реестр
 * @property string $date_exception Дата исключения из реестра
 * @property string $created_at Дата создания записи
 * @property string $type_firm Тип фирмы
 * @property string $num_blank Номер выданного бланка
 * @property string $firm_id id фирмы
 */
class Mfo extends \yii\db\ActiveRecord
{
    // Фирма активна
    const STATUS_FIRM_ACTIVE = 1;
    // Фирма исключена
    const STATUS_FIRM_EXCLUDED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region', 'date_add', 'date_exception', 'created_at', 'ogrn', 'inn', 'status', 'firm_id'], 'safe'],
            [['full_name', 'short_name', 'address', 'num_blank', 'type_firm'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firm_id' => 'id фирмы',
            'num_blank' => 'Номер бланка',
            'full_name' => 'Полное наименование',
            'short_name' => 'Сокращенное наименование',
            'type_firm' => 'Тип компании',
            'region' => 'Код региона',
            'address' => 'Адрес',
            'ogrn' => 'ОГРН',
            'inn' => 'ИНН',
            'status' => 'Статус',
            'date_add' => 'Дата добавления в реестр',
            'date_exception' => 'Дата исключения из реестра',
            'created_at' => 'Дата создания',
        ];
    }
}
