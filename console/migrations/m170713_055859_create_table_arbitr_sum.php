<?php

use yii\db\Migration;

class m170713_055859_create_table_arbitr_sum extends Migration
{
    public function safeUp()
    {
        $this->createTable('arbitr_sum', [
            'id' => $this->primaryKey(),
            'irbis_request_id' => $this->integer(),
            'sum' => $this->string(),
            'year' => $this->string(),
            'count' => $this->integer(),
            'type' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->addForeignKey('fk_arbitr_sum_org_id', 'arbitr_sum', 'irbis_request_id', 'irbis_request', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('arbitr_sum');
    }


}
