<?php

use yii\db\Migration;

class m170718_060549_add_column_to_bankrot extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bankrot_org', 'case_number', $this->string());
        $this->addColumn('bankrot_org', 'url', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('bankrot_org', 'case_number');
        $this->dropColumn('bankrot_org', 'url');
    }
}
