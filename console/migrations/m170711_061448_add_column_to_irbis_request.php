<?php

use yii\db\Migration;

class m170711_061448_add_column_to_irbis_request extends Migration
{
    public function safeUp()
    {
        $this->addColumn('irbis_request', 'inn', $this->string());
        $this->addColumn('irbis_request', 'first_name', $this->string());
        $this->addColumn('irbis_request', 'last_name', $this->string());
        $this->addColumn('irbis_request', 'second_name', $this->string());
        $this->addColumn('irbis_request', 'num_passport', $this->string());
        $this->addColumn('irbis_request', 'ser_passport', $this->string());
        $this->addColumn('irbis_request', 'birth_date', $this->date());
        $this->addColumn('irbis_request', 'region', $this->string());
        $this->addColumn('irbis_request', 'check_manager', $this->string());
        $this->addColumn('irbis_request', 'check_founders', $this->string());

    }

    public function safeDown()
    {
        $this->dropColumn('irbis_request', 'inn');
        $this->dropColumn('irbis_request', 'first_name');
        $this->dropColumn('irbis_request', 'last_name');
        $this->dropColumn('irbis_request', 'second_name');
        $this->dropColumn('irbis_request', 'num_passport');
        $this->dropColumn('irbis_request', 'ser_passport');
        $this->dropColumn('irbis_request', 'birth_date');
        $this->dropColumn('irbis_request', 'region');
        $this->dropColumn('irbis_request', 'check_manager');
        $this->dropColumn('irbis_request', 'check_founders');
    }
}
