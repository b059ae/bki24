<?php

use yii\db\Migration;

class m170713_085736_drop_fk_irbis_people extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_irbis_people_irbis_org_id', 'irbis_people');
        $this->addForeignKey('fk_irbis_people_irbis_org_id', 'irbis_people', 'org_id', 'irbis_request', 'id');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_irbis_people_irbis_org_id', 'irbis_people');
        $this->addForeignKey('fk_irbis_people_irbis_org_id', 'irbis_people', 'org_id', 'irbis_org', 'id');
    }
}
