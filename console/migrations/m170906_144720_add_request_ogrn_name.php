<?php

use yii\db\Migration;

class m170906_144720_add_request_ogrn_name extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('request', 'inn', $this->string(12));
        $this->addColumn('request', 'ogrn', $this->string(13)->comment('ОГРН'));
        $this->addColumn('request', 'name', $this->string()->comment('Название организации'));
    }

    public function safeDown()
    {
        $this->dropColumn('request', 'ogrn');
        $this->dropColumn('request', 'name');
    }
}
