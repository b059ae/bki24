<?php

use yii\db\Migration;

class m170706_133500_create_table_okved_org extends Migration
{
    public function up()
    {
        $this->createTable('okved_org', [
            'id' => $this->primaryKey(),
            'irbis_request_id' => $this->integer(),
            'code' => $this->string(),
            'name' => $this->string(),
            'type' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->addForeignKey('fk_okved_org_org_id', 'okved_org', 'irbis_request_id', 'irbis_request', 'id');
    }

    public function down()
    {
        $this->dropTable('okved_org');
    }

}
