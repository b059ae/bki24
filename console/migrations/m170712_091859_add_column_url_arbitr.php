<?php

use yii\db\Migration;

class m170712_091859_add_column_url_arbitr extends Migration
{
    public function safeUp()
    {
        $this->addColumn('arbitr_org', 'url', $this->string());
        $this->addColumn('arbitr_org', 'name', $this->string());
        $this->addColumn('arbitr_people', 'url', $this->string());
        $this->addColumn('arbitr_people', 'name', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('arbitr_org', 'url');
        $this->dropColumn('arbitr_org', 'name');
        $this->dropColumn('arbitr_people', 'url');
        $this->dropColumn('arbitr_people', 'name');
    }

}
