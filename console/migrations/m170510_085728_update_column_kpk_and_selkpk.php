<?php

use yii\db\Migration;

class m170510_085728_update_column_kpk_and_selkpk extends Migration
{
    public function up()
    {
        $this->alterColumn('kpk', 'person_with_right', $this->text());
        $this->alterColumn('sel_kpk', 'person_with_right', $this->text());
    }

    public function down()
    {
        $this->alterColumn('kpk', 'person_with_right', $this->string());
        $this->alterColumn('sel_kpk', 'person_with_right', $this->string());
    }
}
