<?php

use yii\db\Migration;

class m170830_160339_add_access_token_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'access_token', $this->string()->notNull()->unique()->comment('Токен для доступа'));
        $this->update('user', [
            'access_token' => Yii::$app->security->generateRandomString(),
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'access_token');
    }
}
