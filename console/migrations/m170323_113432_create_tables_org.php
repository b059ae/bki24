<?php

use yii\db\Migration;

class m170323_113432_create_tables_org extends Migration
{
    public function up()
    {
        $this->createTable('irbis_org', [
            'id' => $this->primaryKey(),
            'id_irbis_request' => $this->integer(),
            'inn' => $this->string(),
            'ogrn' => $this->string(),
            'kpp' => $this->string(),
            'full_name' => $this->string(),
            'short_name' => $this->string(),
            'date_born' => $this->date(),
            'address' => $this->string(),
            'capital' => $this->string(),
            'reg_org_name' => $this->string(),
            'email' => $this->string(),
            'death_date' => $this->string(),
            'death_cause' => $this->string(),
            'registry_type' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_irbis_org_irbis_request_id', 'irbis_org', 'id_irbis_request', 'irbis_request', 'id');

        $this->createTable('judge_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'type' => $this->string(),
            'count' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_judge_org_org_id', 'judge_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('balance_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'sales' => $this->string(),
            'net_profit' => $this->string(),
            'balance' => $this->string(),
            'year' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_balance_org_irbis_org_id', 'balance_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('arbitr_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'type_request' => $this->string(),
            'member' => $this->string(),
            'address' => $this->string(),
            'type_member' => $this->string(),
            'judge' => $this->string(),
            'date' => $this->date(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_arbitr_org_irbis_org_id', 'arbitr_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('arrest_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'number' => $this->string(),
            'res_date' => $this->string(),
            'nalog_code' => $this->string(),
            'bik' => $this->string(),
            'bank' => $this->string(),
            'reg_date' => $this->string(),
            'mobCol' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_arrest_org_irbis_org_id', 'arrest_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('founders_legal_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'ogrn' => $this->string(),
            'inn' => $this->string(),
            'name' => $this->string(),
            'share_capital' => $this->string(),
            'grn_date' => $this->date(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_founders_legal_org_irbis_org_id', 'founders_legal_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('bankrot_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'type_request' => $this->integer(),
            'bankrot' => $this->string(),
            'address' => $this->string(),
            'judge' => $this->string(),
            'date_bankrot' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_bankrot_org_irbis_org_id', 'bankrot_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('fssp_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'name' => $this->string(),
            'address' => $this->string(),
            'code_ip' => $this->string(),
            'date_ip' => $this->date(),
            'subject' => $this->string(),
            'sum' => $this->string(),
            'fssp_department' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_fssp_org_irbis_org_id', 'fssp_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('gc_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'customer_name' => $this->string(),
            'customer_inn' => $this->string(),
            'customer_kpp' => $this->string(),
            'date' => $this->date(),
            'sum' => $this->string(),
            'mob_col' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_gc_org_irbis_org_id', 'gc_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('founders_fl_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'fio' => $this->string(),
            'inn' => $this->string(),
            'grn_date' => $this->date(),
            'share_capital' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_founders_fl_org_irbis_org_id', 'founders_fl_org', 'id_irbis_org', 'irbis_org', 'id');

        $this->createTable('manager_fl_org', [
            'id' => $this->primaryKey(),
            'id_irbis_org' => $this->integer(),
            'fio' => $this->string(),
            'inn' => $this->string(),
            'grn_date' => $this->date(),
            'role_name' => $this->string(),
            'role_type' => $this->string(),
            'phones' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_manager_fl_org_irbis_org_id', 'manager_fl_org', 'id_irbis_org', 'irbis_org', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('irbis_org');
        $this->dropTable('balance_org');
        $this->dropTable('judge_org');
        $this->dropTable('arbitr_org');
        $this->dropTable('arrest_org');
        $this->dropTable('founders_legal_org');
        $this->dropTable('bankrot_org');
        $this->dropTable('fssp_org');
        $this->dropTable('gc_org');
        $this->dropTable('founders_fl_org');
        $this->dropTable('manager_fl_org');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
