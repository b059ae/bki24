<?php

use yii\db\Migration;

class m170706_084914_rename_id_irbis_people extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_irbis_people_irbis_request_id', 'irbis_people');
        $this->renameColumn('irbis_people', 'id_irbis_request', 'irbis_request_id');
        $this->addForeignKey('fk_irbis_people_irbis_request_id', 'irbis_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_judge_people_irbis_people_id', 'judge_people');
        $this->renameColumn('judge_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_judge_people_irbis_people_id', 'judge_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_fssp_people_irbis_people_id', 'fssp_people');
        $this->renameColumn('fssp_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_fssp_people_irbis_people_id', 'fssp_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_bankrot_people_irbis_people_id', 'bankrot_people');
        $this->renameColumn('bankrot_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_bankrot_people_irbis_people_id', 'bankrot_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_gc_people_irbis_people_id', 'gc_people');
        $this->renameColumn('gc_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_gc_people_irbis_people_id', 'gc_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_organisation_people_irbis_people_id', 'organisation_people');
        $this->renameColumn('organisation_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_organisation_people_irbis_people_id', 'organisation_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_arbitr_people_irbis_people_id', 'arbitr_people');
        $this->renameColumn('arbitr_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_arbitr_people_irbis_people_id', 'arbitr_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_pledge_people_irbis_people_id', 'pledge_people');
        $this->renameColumn('pledge_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_pledge_people_irbis_people_id', 'pledge_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_archive_judge_people_irbis_people_id', 'archive_judge_people');
        $this->renameColumn('archive_judge_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_archive_judge_people_irbis_people_id', 'archive_judge_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_archive_people_irbis_people_id', 'archive_people');
        $this->renameColumn('archive_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_archive_people_irbis_people_id', 'archive_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_disqualified_people_irbis_people_id', 'disqualified_people');
        $this->renameColumn('disqualified_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_disqualified_people_irbis_people_id', 'disqualified_people', 'irbis_request_id', 'irbis_request', 'id');

        $this->dropForeignKey('fk_interest_people_irbis_people_id', 'interest_people');
        $this->renameColumn('interest_people', 'id_irbis_people', 'irbis_request_id');
        $this->addForeignKey('fk_interest_people_irbis_people_id', 'interest_people', 'irbis_request_id', 'irbis_request', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_irbis_people_irbis_request_id', 'irbis_people');
        $this->renameColumn('irbis_people',  'irbis_request_id', 'id_irbis_request');
        $this->addForeignKey('fk_irbis_people_irbis_request_id', 'irbis_people', 'id_irbis_request', 'irbis_request', 'id');

        $this->dropForeignKey('fk_judge_people_irbis_people_id', 'judge_people');
        $this->renameColumn('judge_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_judge_people_irbis_people_id', 'judge_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_fssp_people_irbis_people_id', 'fssp_people');
        $this->renameColumn('fssp_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_fssp_people_irbis_people_id', 'fssp_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_bankrot_people_irbis_people_id', 'bankrot_people');
        $this->renameColumn('bankrot_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_bankrot_people_irbis_people_id', 'bankrot_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_gc_people_irbis_people_id', 'gc_people');
        $this->renameColumn('gc_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_gc_people_irbis_people_id', 'gc_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_organisation_people_irbis_people_id', 'organisation_people');
        $this->renameColumn('organisation_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_organisation_people_irbis_people_id', 'organisation_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_arbitr_people_irbis_people_id', 'arbitr_people');
        $this->renameColumn('arbitr_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_arbitr_people_irbis_people_id', 'arbitr_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_pledge_people_irbis_people_id', 'pledge_people');
        $this->renameColumn('pledge_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_pledge_people_irbis_people_id', 'pledge_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_archive_judge_people_irbis_people_id', 'archive_judge_people');
        $this->renameColumn('archive_judge_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_archive_judge_people_irbis_people_id', 'archive_judge_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_archive_people_irbis_people_id', 'archive_people');
        $this->renameColumn('archive_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_archive_people_irbis_people_id', 'archive_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_disqualified_people_irbis_people_id', 'disqualified_people');
        $this->renameColumn('disqualified_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_disqualified_people_irbis_people_id', 'disqualified_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->dropForeignKey('fk_interest_people_irbis_people_id', 'interest_people');
        $this->renameColumn('interest_people', 'irbis_request_id', 'id_irbis_people');
        $this->addForeignKey('fk_interest_people_irbis_people_id', 'interest_people', 'id_irbis_people', 'irbis_people', 'id');
    }
}
