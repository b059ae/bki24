<?php

use yii\db\Migration;

class m170718_091400_add_column_to_judge_people extends Migration
{
    public function safeUp()
    {
        $this->addColumn('judge_people', 'case_number', $this->string());
        $this->addColumn('judge_people', 'end_date', $this->string());
        $this->addColumn('judge_people', 'resolution', $this->string());
        $this->addColumn('judge_people', 'court_name', $this->string());
        $this->addColumn('judge_people', 'role', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('judge_people', 'case_number');
        $this->dropColumn('judge_people', 'end_date');
        $this->dropColumn('judge_people', 'resolution');
        $this->dropColumn('judge_people', 'court_name');
        $this->dropColumn('judge_people', 'role');
    }
}
