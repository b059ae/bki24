<?php

use yii\db\Migration;

class m170905_145931_archive_judge_people_head_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('archive_judge_people', 'head', $this->text());
    }

    public function safeDown()
    {
    }

}
