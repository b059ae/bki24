<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bad_contract`.
 */
class m170719_143738_create_bad_contract_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bad_contract', [
            'id' => $this->primaryKey(),
            'irbis_request_id' => $this->integer(),
            'date_start' => $this->date(),
            'date_end' => $this->date(),
            'date_contract' => $this->date(),
            'sum' => $this->string(),
            'url' => $this->string(),
            'reason' => $this->string(),
            'registry_number' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->addForeignKey('fk_bad_contract_org_id', 'bad_contract', 'irbis_request_id', 'irbis_request', 'id');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bad_contract');
    }
}
