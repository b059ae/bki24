<?php

use yii\db\Migration;

class m170706_083024_rename_col_id_irbis_org extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_irbis_org_irbis_request_id', 'irbis_org');
        $this->renameColumn('irbis_org', 'id_irbis_request', 'irbis_request_id');
        $this->addForeignKey('fk_irbis_org_irbis_request_id', 'irbis_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_judge_org_org_id', 'judge_org');
        $this->renameColumn('judge_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_judge_org_org_id', 'judge_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_balance_org_irbis_org_id', 'balance_org');
        $this->renameColumn('balance_org','id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_balance_org_irbis_org_id', 'balance_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_arbitr_org_irbis_org_id', 'arbitr_org');
        $this->renameColumn('arbitr_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_arbitr_org_irbis_org_id', 'arbitr_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_arrest_org_irbis_org_id', 'arrest_org');
        $this->renameColumn('arrest_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_arrest_org_irbis_org_id', 'arrest_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_founders_legal_org_irbis_org_id', 'founders_legal_org');
        $this->renameColumn('founders_legal_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_founders_legal_org_irbis_org_id', 'founders_legal_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_bankrot_org_irbis_org_id', 'bankrot_org');
        $this->renameColumn('bankrot_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_bankrot_org_irbis_org_id', 'bankrot_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_fssp_org_irbis_org_id', 'fssp_org');
        $this->renameColumn('fssp_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_fssp_org_irbis_org_id', 'fssp_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_gc_org_irbis_org_id', 'gc_org');
        $this->renameColumn('gc_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_gc_org_irbis_org_id', 'gc_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_founders_fl_org_irbis_org_id', 'founders_fl_org');
        $this->renameColumn('founders_fl_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_founders_fl_org_irbis_org_id', 'founders_fl_org', 'irbis_request_id', 'irbis_request', 'id');


        $this->dropForeignKey('fk_manager_fl_org_irbis_org_id', 'manager_fl_org');
        $this->renameColumn('manager_fl_org', 'id_irbis_org', 'irbis_request_id');
        $this->addForeignKey('fk_manager_fl_org_irbis_org_id', 'manager_fl_org', 'irbis_request_id', 'irbis_request', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_irbis_org_irbis_request_id', 'irbis_org');
        $this->renameColumn('irbis_org', 'irbis_request_id', 'id_irbis_request');
        $this->addForeignKey('fk_irbis_org_irbis_request_id', 'irbis_org', 'id_irbis_request', 'irbis_request', 'id');

        $this->dropForeignKey('fk_judge_org_org_id', 'judge_org');
        $this->renameColumn('judge_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_judge_org_org_id', 'judge_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_balance_org_irbis_org_id', 'balance_org');
        $this->renameColumn('balance_org','irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_balance_org_irbis_org_id', 'balance_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_arbitr_org_irbis_org_id', 'arbitr_org');
        $this->renameColumn('arbitr_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_arbitr_org_irbis_org_id', 'arbitr_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_arrest_org_irbis_org_id', 'arrest_org');
        $this->renameColumn('arrest_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_arrest_org_irbis_org_id', 'arrest_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_founders_legal_org_irbis_org_id', 'founders_legal_org');
        $this->renameColumn('founders_legal_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_founders_legal_org_irbis_org_id', 'founders_legal_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_bankrot_org_irbis_org_id', 'bankrot_org');
        $this->renameColumn('bankrot_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_bankrot_org_irbis_org_id', 'bankrot_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_fssp_org_irbis_org_id', 'fssp_org');
        $this->renameColumn('fssp_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_fssp_org_irbis_org_id', 'fssp_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_gc_org_irbis_org_id', 'gc_org');
        $this->renameColumn('gc_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_gc_org_irbis_org_id', 'gc_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_founders_fl_org_irbis_org_id', 'founders_fl_org');
        $this->renameColumn('founders_fl_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_founders_fl_org_irbis_org_id', 'founders_fl_org', 'id_irbis_org', 'irbis_org', 'id');


        $this->dropForeignKey('fk_manager_fl_org_irbis_org_id', 'manager_fl_org');
        $this->renameColumn('manager_fl_org', 'irbis_request_id', 'id_irbis_org');
        $this->addForeignKey('fk_manager_fl_org_irbis_org_id', 'manager_fl_org', 'id_irbis_org', 'irbis_org', 'id');
    }


}
