<?php

use yii\db\Migration;

class m170721_084040_add_column_irbis_people extends Migration
{
    public function safeUp()
    {
        $this->addColumn('irbis_people', 'founders_id', $this->integer());
        $this->addColumn('irbis_people', 'manager_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('irbis_people', 'founders_id');
        $this->dropColumn('irbis_people', 'manager_id');
    }
}
