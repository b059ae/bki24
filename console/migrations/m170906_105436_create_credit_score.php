<?php

use yii\db\Migration;

class m170906_105436_create_credit_score extends Migration
{
    public function safeUp()
    {
        $this->createTable('credit_score', [
            'id' => $this->primaryKey(),
            'score' => $this->integer()->comment('Скоринг'),
            'reason_1' => $this->text()->comment('Причина снижения балла 1'),
            'reason_2' => $this->text()->comment('Причина снижения балла 2'),
            'reason_3' => $this->text()->comment('Причина снижения балла 3'),
            'reason_4' => $this->text()->comment('Причина снижения балла 4'),
            'request_id' => $this->integer()->notNull()->comment('ID запроса из request'),
            'created_at' => $this->integer()->notNull()->comment('Дата создания'),

        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createIndex('ix_credit_score_request_id', 'credit_score', 'request_id');
        $this->addForeignKey('fk_credit_score_to_request', 'credit_score', 'request_id', 'request', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropForeignKey('fk_credit_score_to_request', '{{%credit_score}}');
        $this->dropIndex('ix_credit_score_request_id', '{{%credit_score}}');
        $this->dropTable('credit_score');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
