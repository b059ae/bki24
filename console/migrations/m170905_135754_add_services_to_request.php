<?php

use yii\db\Migration;

class m170905_135754_add_services_to_request extends Migration
{
    public function safeUp()
    {
        $this->addColumn('request', 'services', $this->string()->notNull()->defaultValue(json_encode([]))->comment('JSON массив услуг'));
    }

    public function safeDown()
    {
        $this->dropColumn('request', 'services');
    }
}
