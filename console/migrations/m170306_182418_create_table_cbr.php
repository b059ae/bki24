<?php

use yii\db\Migration;

class m170306_182418_create_table_cbr extends Migration
{
    public function up()
    {
        $this->createTable('mfo', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(),
            'short_name' => $this->string(),
            'firm_id' => $this->string(),
            'num_blank' => $this->string(),
            'type_firm' => $this->string(),
            'region' => $this->string(),
            'address' => $this->string(),
            'ogrn' => $this->string(),
            'inn' => $this->string(),
            'status' => $this->string(),
            'date_add' => $this->date(),
            'date_exception' => $this->date(),
            'created_at' => $this->string(),
        ],'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createTable('kpk', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(),
            'short_name' => $this->string(),
            'firm_id' => $this->string(),
            'way' => $this->string(),
            'region' => $this->string(),
            'address' => $this->string(),
            'ogrn' => $this->string(),
            'inn' => $this->string(),
            'kpp' => $this->string(),
            'status' => $this->string(),
            'date_add' => $this->date(),
            'date_exception' => $this->date(),
            'person_with_right' => $this->string(),
            'created_at' => $this->string(),
        ],'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createTable('sel_kpk', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(),
            'short_name' => $this->string(),
            'firm_id' => $this->string(),
            'way' => $this->string(),
            'region' => $this->string(),
            'address' => $this->string(),
            'ogrn' => $this->string(),
            'inn' => $this->string(),
            'status' => $this->string(),
            'date_add' => $this->date(),
            'date_exception' => $this->date(),
            'person_with_right' => $this->string(),
            'created_at' => $this->string(),
        ],'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    public function down()
    {
        $this->dropTable('mfo');
        $this->dropTable('kpk');
        $this->dropTable('sel_kpk');
    }
}
