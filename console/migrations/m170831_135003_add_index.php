<?php

use yii\db\Migration;

class m170831_135003_add_index extends Migration
{
    public function up()
    {
        $this->createIndex('ix_irbis_request_type', 'irbis_request', 'request_type');
        $this->createIndex('ix_request_type', 'request', 'type');
    }

    public function down()
    {
        $this->dropIndex('ix_irbis_request_type', 'irbis_request');
        $this->dropIndex('ix_request_type', 'request');
    }
}
