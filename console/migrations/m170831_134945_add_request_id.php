<?php

use yii\db\Migration;

class m170831_134945_add_request_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('irbis_request', 'request_id', $this->integer()->notNull()->comment('ID запроса из request'));
        // Внешний ключ на таблицу request
        $this->addForeignKey('fk_irbis_request_id', 'irbis_request', 'request_id', 'request', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('irbis_request', 'request_id');
    }
}
