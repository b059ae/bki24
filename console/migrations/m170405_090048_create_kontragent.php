<?php

use yii\db\Migration;

class m170405_090048_create_kontragent extends Migration
{
    public function up()
    {
        $this->createTable('kontragent',[
            'id' => $this->primaryKey(),
            'full_name' => $this->string(),
            'short_name' => $this->string(),
            'inn' => $this->string(),
            'kpp' => $this->string(),
            'ogrn' => $this->string(),
            'address' => $this->string(),
            'manager' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'status' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    public function down()
    {
        $this->dropTable('kontragent');
    }
}
