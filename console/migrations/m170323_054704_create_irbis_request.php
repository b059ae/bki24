<?php

use yii\db\Migration;

class m170323_054704_create_irbis_request extends Migration
{
    public function up()
    {
        $this->createTable('irbis_request', [
            'id' => $this->primaryKey(),
            'uuid' => $this->string(),
            'request_type' => $this->integer(),
            'request_data' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    public function down()
    {
        $this->dropTable('irbis_request');
    }
}
