<?php

use yii\db\Migration;

class m170831_125147_add_user_id_irbis_request extends Migration
{
    public function safeUp()
    {
        $this->addColumn('irbis_request', 'created_at', $this->integer()->notNull()->comment('Дата создания'));
    }

    public function safeDown()
    {
        $this->dropColumn('irbis_request', 'created_at');
    }
}
