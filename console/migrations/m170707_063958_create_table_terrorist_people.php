<?php

use yii\db\Migration;

class m170707_063958_create_table_terrorist_people extends Migration
{
    public function up()
    {
        $this->createTable('terrorist_people', [
            'id' => $this->primaryKey(),
            'irbis_request_id' => $this->integer(),
            'name' => $this->string(),
            'birth_date' => $this->string(),
            'birth_place' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
        $this->addForeignKey('fk_terrorist_people_id', 'terrorist_people', 'irbis_request_id', 'irbis_request', 'id');

    }

    public function down()
    {
        $this->dropTable('terrorist_people');
    }
}
