<?php

use yii\db\Migration;

class m170330_075603_create_tables_irbis_people extends Migration
{
    public function up()
    {
        $this->createTable('irbis_people', [
            'id' => $this->primaryKey(),
            'id_irbis_request' => $this->integer(),
            'fio' => $this->string(),
            'birth_date' => $this->date(),
            'fms' => $this->integer(),
            'terrorist' => $this->string(),
            'region' => $this->string(),
            'org_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_irbis_people_irbis_request_id', 'irbis_people', 'id_irbis_request', 'irbis_request', 'id');
        $this->addForeignKey('fk_irbis_people_irbis_org_id', 'irbis_people', 'org_id', 'irbis_org', 'id');

        $this->createTable('judge_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'date' => $this->string(),
            'judge' => $this->string(),
            'type_cause' => $this->string(),
            'description' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_judge_people_irbis_people_id', 'judge_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('fssp_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'name' => $this->string(),
            'code' => $this->string(),
            'fssp_department' => $this->string(),
            'subject' => $this->string(),
            'date_end' => $this->date(),
            'sum' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_fssp_people_irbis_people_id', 'fssp_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('bankrot_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'bankrot' => $this->string(),
            'judge' => $this->string(),
            'code' => $this->string(),
            'date_bankrot' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_bankrot_people_irbis_people_id', 'bankrot_people', 'id_irbis_people', 'irbis_people', 'id');


        $this->createTable('gc_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'result' => $this->string(),
            'customer' => $this->string(),
            'date' => $this->date(),
            'sum' => $this->string(),
            'status' => $this->string(),
            'mobCol' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_gc_people_irbis_people_id', 'gc_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('organisation_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'name' => $this->string(),
            'inn' => $this->string(),
            'ogrn' => $this->string(),
            'address' => $this->string(),
            'position' => $this->string(),
            'status' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_organisation_people_irbis_people_id', 'organisation_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('arbitr_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'member' => $this->string(),
            'type_member' => $this->string(),
            'judge' => $this->string(),
            'date' => $this->date(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_arbitr_people_irbis_people_id', 'arbitr_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('pledge_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'fio' => $this->string(),
            'passport' => $this->string(),
            'birth_date' => $this->date(),
            'pawnbroker' => $this->string(),
            'type_pledge' => $this->string(),
            'date_add' => $this->date(),
            'date_end' => $this->date(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_pledge_people_irbis_people_id', 'pledge_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('archive_judge_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'head' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_archive_judge_people_irbis_people_id', 'archive_judge_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('archive_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'full_name' => $this->string(),
            'inn' => $this->string(),
            'result' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_archive_people_irbis_people_id', 'archive_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('disqualified_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'fio' => $this->string(),
            'birth_date' => $this->date(),
            'bornplace' => $this->string(),
            'legal_name' => $this->string(),
            'start_date_disq' => $this->date(),
            'end_date_disq' => $this->date(),
            'office' => $this->string(),
            'department' => $this->string(),
            'article' => $this->string(),
            'fio_judge' => $this->string(),
            'office_judge' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_disqualified_people_irbis_people_id', 'disqualified_people', 'id_irbis_people', 'irbis_people', 'id');

        $this->createTable('interest_people', [
            'id' => $this->primaryKey(),
            'id_irbis_people' => $this->integer(),
            'name' => $this->string(),
            'request' => $this->string(),
            'date_request' => $this->date(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('fk_interest_people_irbis_people_id', 'interest_people', 'id_irbis_people', 'irbis_people', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('irbis_people');
        $this->dropTable('judge_people');
        $this->dropTable('fssp_people');
        $this->dropTable('bankrot_people');
        $this->dropTable('gc_people');
        $this->dropTable('organisation_people');
        $this->dropTable('arbitr_people');
        $this->dropTable('pledge_people');
        $this->dropTable('archive_judge_people');
        $this->dropTable('archive_people');
        $this->dropTable('disqualified_people');
        $this->dropTable('interest_people');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }

}
