<?php

use yii\db\Migration;

class m170831_133440_add_request extends Migration
{
    public function up()
    {
        $this->createTable('request',[
            'id' => $this->primaryKey(),
            'last_name' => $this->string()->comment('Фамилия'),
            'first_name' => $this->string()->comment('Имя'),
            'middle_name' => $this->string()->comment('Отчество'),
            'birth_date' => $this->date()->comment('Дата рождения'),
            'passport_series' => $this->integer(4)->comment('Серия паспорта'),
            'passport_number' => $this->integer(6)->comment('Номер паспорта'),
            'inn' => $this->string(12)->comment('ИНН'),
            'region_id' => $this->integer(3)->comment('Код региона'),
            'note' => $this->text()->comment('Примечание'),
            'status' => $this->integer(2)->notNull()->defaultValue(\common\models\interfaces\RequestInterface::STATUS_NOT_READY)->comment('Статус выполнения'),
            'type' => $this->integer(2)->notNull()->comment('Тип запроса (физлицо, юрлицо)'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь из users, который сделал запрос'),
            'created_at' => $this->integer()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->notNull()->comment('Дата обновления'),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        // Внешний ключ на таблицу region
        $this->addForeignKey('fk_request_region_id', 'request', 'region_id', 'regions', 'id');

        // Внешний ключ на таблицу user
        $this->addForeignKey('fk_request_user_id', 'request', 'user_id', 'user', 'id');
    }

    public function down()
    {
        $this->dropTable('request');
    }
}
