<?php

namespace api\modules\rest\controllers;

use yii\filters\auth\HttpBasicAuth;

/**
 * Справочник регионов
 */
class RegionController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => \api\modules\rest\controllers\actions\region\IndexAction::class,
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
        ];
    }
}
