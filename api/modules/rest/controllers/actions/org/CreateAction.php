<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.08.17
 * Time: 11:42
 */

namespace api\modules\rest\controllers\actions\org;


use api\modules\rest\models\forms\Org;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\base\Action;
use yii\web\ServerErrorHttpException;

class CreateAction extends Action
{
    /**
     * Добавление нового запроса по физлицу
     *
     * @return Model the model newly created
     * @throws ServerErrorHttpException if there is any error when creating the model
     */
    public function run()
    {
        /* @var $model Model */
        $model = new Org();

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->create(Yii::$app->queue)) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
}