<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:15
 */

namespace api\modules\rest\controllers\actions\org;


use yii\base\Action;
use yii\web\NotFoundHttpException;

class ViewAction extends Action
{
    /**
     * Просмотр информации о запросе по физлицу
     *
     * @param string $id the primary key of the model.
     * @return \yii\db\ActiveRecordInterface the model being displayed
     */
    public function run($id)
    {
        $model = \api\modules\rest\models\dataProviders\Org::view($id, \Yii::$app->user->id);

        if ($model) {
            return $model;
        }

        throw new NotFoundHttpException("Object not found: $id");
    }

}