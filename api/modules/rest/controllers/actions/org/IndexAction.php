<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:15
 */

namespace api\modules\rest\controllers\actions\org;

use yii\base\Action;
use yii\data\ActiveDataProvider;

class IndexAction extends Action
{
    /**
     * Список запросов по физлицам с пагинацией
     *
     * @return ActiveDataProvider
     */
    public function run()
    {
        return \api\modules\rest\models\dataProviders\Org::index(\Yii::$app->user->id);
    }

}