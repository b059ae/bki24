<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:15
 */

namespace api\modules\rest\controllers\actions\region;


use yii\base\Action;
use yii\data\ActiveDataProvider;

class IndexAction extends Action
{
    /**
     * @return ActiveDataProvider
     */
    public function run()
    {
        return \api\modules\rest\models\dataProviders\Region::all();
    }

}