<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:15
 */

namespace api\modules\rest\controllers\actions\people;


use yii\base\Action;
use yii\data\ActiveDataProvider;

class IndexAction extends Action
{
    /**
     * Список запросов по физлицам с пагинацией
     *
     * @return ActiveDataProvider
     */
    public function run()
    {
        return \api\modules\rest\models\dataProviders\People::index(\Yii::$app->user->id);
    }

}