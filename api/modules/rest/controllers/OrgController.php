<?php

namespace api\modules\rest\controllers;

use api\modules\rest\models\forms\org;
use yii\filters\auth\HttpBasicAuth;

/**
 * Получение информации о юрлицах
 */
class OrgController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'view' => \api\modules\rest\controllers\actions\org\ViewAction::class,
            'index' => \api\modules\rest\controllers\actions\org\IndexAction::class,
            'create' => \api\modules\rest\controllers\actions\org\CreateAction::class,
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
        ];
    }
}
