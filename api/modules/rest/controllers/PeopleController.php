<?php

namespace api\modules\rest\controllers;

use api\modules\rest\models\forms\People;
use yii\filters\auth\HttpBasicAuth;

/**
 * Получение информации о физлицах
 */
class PeopleController extends \yii\rest\Controller
{
    public $modelClass = People::class;
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'view' => \api\modules\rest\controllers\actions\people\ViewAction::class,
            'index' => \api\modules\rest\controllers\actions\people\IndexAction::class,
            'create' => \api\modules\rest\controllers\actions\people\CreateAction::class,
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
        ];
    }
}
