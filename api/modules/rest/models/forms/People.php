<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:10
 */

namespace api\modules\rest\models\forms;

use common\models\Regions;
use common\models\Services;
use common\queue\PeopleJob;
use common\validators\ValidatorHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\queue\Queue;

class People extends Model
{

    public $id;
    public $last_name;
    public $first_name;
    public $middle_name;
    public $region_id;
    public $birth_date;
    public $passport_series;
    public $passport_number;
    public $inn;
    public $services;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            [
                [['last_name', 'first_name', 'region_id', 'birth_date', 'passport_series', 'passport_number', 'services'], 'required'],
                ['services', 'each', 'rule' => ['in', 'range' => Services::getArray()]],
            ],
            ValidatorHelper::nameValidators(['last_name', 'first_name', 'middle_name']),
            ValidatorHelper::innValidators(['inn']),
            ValidatorHelper::dateValidators(['birth_date']),
            ValidatorHelper::passportSeriesValidators(['passport_series']),
            ValidatorHelper::passportNumberValidators(['passport_number']),
            [
                [['region_id'], 'integer'],
                [
                    ['region_id'],
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => Regions::className(),
                    'targetAttribute' => ['region_id' => 'code']
                ],
            ]
        );
    }

    /**
     * Добавление нового запроса по физлицу
     *
     * @param Queue $queue
     * @return bool|int
     */
    public function create(Queue $queue)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new \common\models\Request([
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'region_id' => $this->region_id,
            'birth_date' => \Yii::$app->formatter->asDate($this->birth_date, 'php:Y-m-d'),
            'passport_series' => $this->passport_series,
            'passport_number' => $this->passport_number,
            'inn' => $this->inn,
            'type' => \common\models\Request::REQUEST_PEOPLE,
            'services' => json_encode($this->services),
        ]);

        if ($model->save()) {
            $this->id = $model->id;
            // Постановка задания в очередь
            $queue->push(new PeopleJob([
                'request_id' => $model->id,
            ]));

            return true;
        }

        return false;
    }
}

