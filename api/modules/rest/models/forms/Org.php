<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:10
 */

namespace api\modules\rest\models\forms;

use common\models\Services;
use common\queue\OrgJob;
use common\validators\ValidatorHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\queue\Queue;

class Org extends Model
{

    public $id;
    public $inn;
    public $ogrn;
    public $name;
    public $services = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            [
                [['inn', 'services'], 'required'],
                ['services', 'each', 'rule' => ['integer']],
                ['services', 'each', 'rule' => ['in', 'range' => [Services::IRBIS]]],
                // Оставляем только ИРБИС потому что нет скоринга НБКИ по юрлицам
                /*['services', 'each', 'rule' => ['in', 'range' => Services::getArray()]],
                [
                    // Требуем ОГРН и Названия при скоринге НБКИ
                    ['ogrn', 'name'],
                    'required',
                    'when' => function ($model) {
                        return in_array(Services::NBKI, $model->services);
                    }
                ],*/
            ],
            ValidatorHelper::innValidators(['inn']),
            ValidatorHelper::ogrnValidators(['ogrn']),
            ValidatorHelper::stringValidators(['name'])
        );
    }

    /**
     * Добавление нового запроса по физлицу
     *
     * @param Queue $queue
     * @return bool|int
     */
    public function create(Queue $queue)
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new \common\models\Request([
            'inn' => $this->inn,
            'ogrn' => $this->ogrn,
            'name' => $this->name,
            'services' => json_encode($this->services),
            'type' => \common\models\Request::REQUEST_LEGAL,
        ]);

        if ($model->save()) {
            $this->id = $model->id;
            // Постановка задания в очередь
            $queue->push(new OrgJob([
                'request_id' => $model->id,
            ]));

            return true;
        }
        return false;
    }
}

