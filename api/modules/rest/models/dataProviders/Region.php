<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:10
 */

namespace api\modules\rest\models\dataProviders;

use common\models\Regions;
use yii\data\ActiveDataProvider;

/**
 * Region represents the model behind the search form about `common\models\Region`.
 */
class Region
{
    /**
     * Полный список регионов без пагинации
     *
     * @return ActiveDataProvider
     */
    public static function all()
    {
        $query = Regions::find()
            ->select(['id', 'name']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ]
        ]);

        return $dataProvider;
    }
}

