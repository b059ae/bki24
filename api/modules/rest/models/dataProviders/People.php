<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:10
 */

namespace api\modules\rest\models\dataProviders;

use common\components\nbki\catalogs\ScoringReasonTypes;
use common\models\CreditScore;
use common\models\IrbisRequest;
use common\models\Request;
use yii\data\ActiveDataProvider;

class People
{
    /**
     * Список запросов по физлицам с пагинацией
     *
     * @param integer $userId
     * @return ActiveDataProvider
     */
    public static function index($userId)
    {
        $query = Request::find()
            ->select([
                'id',
                'last_name',
                'first_name',
                'middle_name',
                'birth_date',
                'passport_series',
                'passport_number',
                'inn',
                'status',
                'created_at'
            ])
            ->andWhere(['user_id' => $userId])
            ->andWhere(['type' => Request::REQUEST_PEOPLE])
            ->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    /**
     * Просмотр информации о запросе по физлицу
     *
     * @param integer $id
     * @param integer $userId
     * @return object
     */
    public static function view($id, $userId)
    {
        /** @var Request $res */
        $res = Request::find()
            ->select([
                'id',
                'last_name',
                'first_name',
                'middle_name',
                'birth_date',
                'passport_series',
                'passport_number',
                'inn',
                'status',
                'created_at'
            ])
            ->andWhere(['id' => $id])
            ->andWhere(['type' => Request::REQUEST_PEOPLE])
            ->andWhere(['user_id' => $userId])
            ->limit(1)
            ->one();

        if (!$res) {
            return null;
        }

        /** @var IrbisRequest $irbisRequest */
        $irbisRequest = $res->irbisRequest;

        /** @var CreditScore $irbisRequest */
        $creditScore = $res->creditScore;

        return ((object)array_merge([
            'id' => $res->id,
            'last_name' => $res->last_name,
            'first_name' => $res->first_name,
            'middle_name' => $res->middle_name,
            'birth_date' => $res->birth_date,
            'passport_series' => $res->passport_series,
            'passport_number' => $res->passport_number,
            'inn' => $res->inn,
            'status' => $res->status,
            'status_name' => $res->getStatusName(),
            'created_at' => $res->created_at,
        ], ($irbisRequest instanceof IrbisRequest && !empty($res->inn))// Был введен ИНН, иначе совпадение неточное
            ? [
                'bankrot' => self::bankrot($irbisRequest),
                'org' => self::org($irbisRequest),
            ]
            : [],
            ($irbisRequest instanceof IrbisRequest)
                ? [
                'fssp' => self::fssp($irbisRequest),
                'people' => self::people($irbisRequest),
                'pledge' => self::pledge($irbisRequest),
                'bad_contracts' => self::bad_contracts($irbisRequest),
//                'arbitr' => self::arbitrPeople($irbisRequest), // Поиск ведется только по ФИО
//                'gc' => self::gc($irbisRequest), // Поиск ведется только по ФИО
//                'judge' => self::judge($irbisRequest), // Поиск ведется только по ФИО
                //'interest' => self::interest($irbisRequest),
            ]
                : [],
            ($creditScore instanceof CreditScore)
                ? [
                'credit_score' => self::score($creditScore),
            ]
                : []
        ));
    }

    /**
     * Арбитраж
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function arbitrPeople(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->arbitrPeoples as $item) {
            $data[] = ((object)[
                'name' => $item->name,
                'member' => $item->member,
                'type' => $item->type_member,
                'typeMember' => $item->getTypeMember(),
                'judge' => $item->judge,
                'date' => \Yii::$app->formatter->asDate($item->date),
                'url' => $item->getFullUrl(),
            ]);
        }
        return $data;
    }

    /**
     * Банкрот
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function bankrot(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->bankrotPeoples as $item) {
            $data[] = ((object)[
                'bankrot' => $item->bankrot,
                'code' => $item->code,
                'judge' => $item->judge,
                'date' => \Yii::$app->formatter->asDate($item->date_bankrot),
            ]);
        }
        return $data;
    }

    /**
     * ФССП
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function fssp(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->fsspPeoples as $item) {
            $data[] = ((object)[
                'name' => $item->name,
                'code_ip' => $item->code,
                'date' => \Yii::$app->formatter->asDate($item->date_end),
                'subject' => $item->subject,
                'sum' => $item->sum,
                'fssp_department' => $item->fssp_department,
            ]);
        }
        return $data;
    }

    /**
     * Государственные контракты
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function gc(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->gcPeoples as $item) {
            $data[] = ((object)[
                'result' => $item->result,
                'customer' => $item->customer,
                'date' => \Yii::$app->formatter->asDate($item->date),
                'sum' => $item->sum,
                'status' => $item->status,
            ]);
        }
        return $data;
    }

    /**
     * Судебные дел
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function judge(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->judgePeoples as $item) {
            $data[] = ((object)[
                'judge' => $item->judge,
                'case_number' => $item->case_number,
                'type_cause' => $item->type_cause,
                'type' => $item->getType(),
                'resolution' => $item->resolution,
                'court_name' => $item->court_name,
                'role' => $item->getRole(),
                'date' => \Yii::$app->formatter->asDate($item->date),
                'end_date' => \Yii::$app->formatter->asDate($item->end_date),

            ]);
        }
        return $data;
    }

    /**
     * Участие в организациях
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function org(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->organisationPeoples as $item) {
            $data[] = ((object)[
                'name' => $item->name,
                'position' => $item->position,
                'inn' => $item->inn,
                'ogrn' => $item->ogrn,
                'status' => $item->getStatus(),
                'address' => $item->address,
            ]);
        }
        return $data;
    }

    /**
     * Информация о физлице
     * @param IrbisRequest $irbisRequest
     * @return object
     */
    private static function people(IrbisRequest $irbisRequest)
    {

        return ((object)[
            'fio' => $irbisRequest->irbisPeoples->fio,
            'birth_date' => \Yii::$app->formatter->asDate($irbisRequest->irbisPeoples->birth_date),
            'fms' => $irbisRequest->irbisPeoples->fms,
            'terrorist' => $irbisRequest->irbisPeoples->terrorist,

        ]);
    }

    /**
     * Залоги
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function pledge(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->pledgePeoples as $item) {
            $data[] = ((object)[
                'fio' => $item->fio,
                'passport' => $item->passport,
                'birth_date' => \Yii::$app->formatter->asDate($item->birth_date),
                'pawnbroker' => $item->pawnbroker,
                'type_pledge' => $item->type_pledge,
                'date_add' => \Yii::$app->formatter->asDate($item->date_add),
                'date_end' => \Yii::$app->formatter->asDate($item->date_end),
            ]);
        }
        return $data;
    }

    /**
     * Список интересовавшихся
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    /*private static function interest(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->interestPeoples as $item) {
            $data[] = ((object)[
                'name' => $item->name,
                'request' => $item->request,
                'date' => \Yii::$app->formatter->asDate($item->date_request),
            ]);
        }
        return $data;
    }*/

    /**
     * Список недобросовестных поставщиков
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function bad_contracts(IrbisRequest $irbisRequest)
    {

        $data = [];
        foreach ($irbisRequest->badContracts as $item) {
            $data[] = ((object)[
                'registry_number' => $item->registry_number,
                'sum' => $item->sum,
                'reason' => $item->reason,
                'date_start' => \Yii::$app->formatter->asDate($item->date_start),
                'date_end' => \Yii::$app->formatter->asDate($item->date_end),
                'date_contract' => \Yii::$app->formatter->asDate($item->date_contract),
                'url' => $item->url,
            ]);
        }
        return $data;
    }

    /**
     * Кредитный скоринг НБКИ
     * @param CreditScore $creditScore
     * @return object
     */
    private static function score(CreditScore $creditScore)
    {
        return ((object)[
            'score' => $creditScore->score,
            'reason_1' => ScoringReasonTypes::getInstant()
                ->getByIndex(
                    ScoringReasonTypes::NBKI_ID,
                    $creditScore->reason_1,
                    ScoringReasonTypes::TITLE
                ),
            'reason_2' => ScoringReasonTypes::getInstant()
                ->getByIndex(
                    ScoringReasonTypes::NBKI_ID,
                    $creditScore->reason_2,
                    ScoringReasonTypes::TITLE
                ),
            'reason_3' => ScoringReasonTypes::getInstant()
                ->getByIndex(
                    ScoringReasonTypes::NBKI_ID,
                    $creditScore->reason_3,
                    ScoringReasonTypes::TITLE
                ),
            'reason_4' => ScoringReasonTypes::getInstant()
                ->getByIndex(
                    ScoringReasonTypes::NBKI_ID,
                    $creditScore->reason_4,
                    ScoringReasonTypes::TITLE
                ),
        ]);
    }
}

