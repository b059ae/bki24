<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 24.08.17
 * Time: 18:10
 */

namespace api\modules\rest\models\dataProviders;

use common\models\IrbisRequest;
use common\models\Request;
use yii\data\ActiveDataProvider;

class Org
{
    /**
     * Список запросов по юрлицам с пагинацией
     *
     * @param integer $userId
     * @return ActiveDataProvider
     */
    public static function index($userId)
    {
        $query = Request::find()
            ->select([
                'id',
                'inn',
                'status',
                'created_at'
            ])
            ->andWhere(['user_id' => $userId])
            ->andWhere(['type' => Request::REQUEST_LEGAL])
            ->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    /**
     * Просмотр информации о запросе по юрлицу
     *
     * @param integer $id
     * @param integer $userId
     * @return object
     */
    public static function view($id, $userId)
    {
        /** @var Request $res */
        $res = Request::find()
            ->select([
                'id',
                'inn',
                'status',
                'created_at',
            ])
            ->andWhere(['id' => $id])
            ->andWhere(['type' => Request::REQUEST_LEGAL])
            ->andWhere(['user_id' => $userId])
            ->limit(1)
            ->one();

        if (!$res){
            return null;
        }

        /** @var IrbisRequest $irbisRequest */
        $irbisRequest = $res->irbisRequest;

        return ((object)array_merge([
            'id' => $res->id,
            'inn' => $res->inn,
            'status' => $res->status,
            'status_name' => $res->getStatusName(),
            'created_at' => $res->created_at,
        ], ($irbisRequest instanceof IrbisRequest)
            ? [
                'org' => self::org($irbisRequest),
                // judge' => self::judge($irbisRequest), Не используем т.к. поиск идет неточный, по совпадению наименования
                'arbitr_sum' => self::arbitrSum($irbisRequest),
                'arbitr_org' => self::arbitrOrg($irbisRequest),
                'balance' => self::balance($irbisRequest),
                'bankrot' => self::bankrot($irbisRequest),
                'fssp' => self::fssp($irbisRequest),
                'gc' => self::gc($irbisRequest),
                'founders_fl' => self::founders_fl($irbisRequest),
                'founders_org' => self::founders_org($irbisRequest),
                'managers' => self::managers_org($irbisRequest),
                'okved' => self::okved($irbisRequest),
                'bad_contracts' => self::bad_contracts($irbisRequest),
            ]
            : []
        ));
    }
    /**
     * Количество судебных дел
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function judge(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->judgeOrgs as $item) {
            $data[] = ((object)[
                'type' => $item->type,
                'count' => $item->count,
            ]);
        }
        return $data;
    }

    /**
     * Количество арбитражей
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function arbitrSum(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->arbitrSum as $item) {
            $data[] = ((object)[
                'type' => $item->type,
                'typeMember' => $item->getTypeMember(),
                'count' => $item->count,
                'sum' => $item->sum,
                'year' => $item->year,
            ]);
        }
        return $data;
    }

    /**
     * Арбитраж
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function arbitrOrg(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->arbitrOrgs as $item) {
            $data[] = ((object)[
                'name' => $item->name,
                'member' => $item->member,
                'address' => $item->address,
                'type' => $item->type_member,
                'typeMember' => $item->getTypeMember(),
                'judge' => $item->judge,
                'date' => \Yii::$app->formatter->asDate($item->date),
                'url' => $item->getFullUrl(),
            ]);
        }
        return $data;
    }

    /**
     * Баланс
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function balance(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->balanceOrgs as $item) {
            $data[] = ((object)[
                'sales' => $item->sales,
                'net_profit' => $item->net_profit,
                'balance' => $item->balance,
                'year' => $item->year,
            ]);
        }
        return $data;
    }

    /**
     * Банкрот
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function bankrot(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->bankrotOrgs as $item) {
            $data[] = ((object)[
                'bankrot' => $item->bankrot,
                'address' => $item->address,
                'judge' => $item->judge,
                'date' => \Yii::$app->formatter->asDate($item->date_bankrot),
            ]);
        }
        return $data;
    }

    /**
     * ФССП
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function fssp(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->fsspOrgs as $item) {
            $data[] = ((object)[
                'name' => $item->name,
                'address' => $item->address,
                'code_ip' => $item->code_ip,
                'date' => \Yii::$app->formatter->asDate($item->date_ip),
                'subject' => $item->subject,
                'sum' => $item->sum,
                'fssp_department' => $item->fssp_department,
            ]);
        }
        return $data;
    }

    /**
     * Государственные контракты
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function gc(IrbisRequest $irbisRequest)
    {
        $data = [];
        foreach ($irbisRequest->gcOrgs as $item) {
            $data[] = ((object)[
                'customer_name' => $item->customer_name,
                'customer_kpp' => $item->customer_kpp,
                'date' => \Yii::$app->formatter->asDate($item->date),
                'sum' => $item->sum,
            ]);
        }
        return $data;
    }

    /**
     * Информация об организации
     * @param IrbisRequest $irbisRequest
     * @return object
     */
    private static function org(IrbisRequest $irbisRequest)
    {

        return ((object)[
            'inn' => $irbisRequest->irbisOrgs->inn,
            'ogrn' => $irbisRequest->irbisOrgs->ogrn,
            'kpp' => $irbisRequest->irbisOrgs->kpp,
            'full_name' => $irbisRequest->irbisOrgs->full_name,
            'short_name' => $irbisRequest->irbisOrgs->short_name,
            'date_born' => \Yii::$app->formatter->asDate($irbisRequest->irbisOrgs->date_born),
            'address' => $irbisRequest->irbisOrgs->address,
            'capital' => $irbisRequest->irbisOrgs->capital,
            'reg_org_name' => $irbisRequest->irbisOrgs->reg_org_name,
            'email' => $irbisRequest->irbisOrgs->email,
            'registry_type' => $irbisRequest->irbisOrgs->registry_type,
            'death_date' => \Yii::$app->formatter->asDate($irbisRequest->irbisOrgs->death_date),
            'death_cause' => $irbisRequest->irbisOrgs->death_cause,
        ]);
    }

    /**
     * Учередители (физлица)
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function founders_fl(IrbisRequest $irbisRequest)
    {

        $data = [];
        foreach ($irbisRequest->foundersFlOrgs as $item) {
            $data[] = ((object)[
                'fio' => $item->fio,
                'inn' => $item->inn,
                'date' => \Yii::$app->formatter->asDate($item->grn_date),
                'share_capital' => $item->share_capital,
            ]);
        }
        return $data;
    }

    /**
     * Учередители (юрлица)
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function founders_org(IrbisRequest $irbisRequest)
    {

        $data = [];
        foreach ($irbisRequest->foundersLegalOrgs as $item) {
            $data[] = ((object)[
                'ogrn' => $item->ogrn,
                'inn' => $item->inn,
                'name' => $item->name,
                'date' => \Yii::$app->formatter->asDate($item->grn_date),
                'share_capital' => $item->share_capital,
            ]);
        }
        return $data;
    }

    /**
     * Руководители
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function managers_org(IrbisRequest $irbisRequest)
    {

        $data = [];
        foreach ($irbisRequest->managerFlOrgs as $item) {
            $data[] = ((object)[
                'fio' => $item->fio,
                'inn' => $item->inn,
                'date' => \Yii::$app->formatter->asDate($item->grn_date),
                'role_name' => $item->role_name,
                'role_type' => $item->role_type,
                'phones' => $item->phones,
            ]);
        }
        return $data;
    }

    /**
     * ОКВЭД
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function okved(IrbisRequest $irbisRequest)
    {

        $data = [];
        foreach ($irbisRequest->okvedOrgs as $item) {
            $data[] = ((object)[
                'code' => $item->code,
                'name' => $item->name,
                'type' => $item->type,
                'typeName' => $item->getTypeName(),
            ]);
        }
        return $data;
    }

    /**
     * Список недобросовестных поставщиков
     * @param IrbisRequest $irbisRequest
     * @return array
     */
    private static function bad_contracts(IrbisRequest $irbisRequest)
    {

        $data = [];
        foreach ($irbisRequest->badContracts as $item) {
            $data[] = ((object)[
                'registry_number' => $item->registry_number,
                'sum' => $item->sum,
                'reason' => $item->reason,
                'date_start' => \Yii::$app->formatter->asDate($item->date_start),
                'date_end' => \Yii::$app->formatter->asDate($item->date_end),
                'date_contract' => \Yii::$app->formatter->asDate($item->date_contract),
                'url' => $item->url,
            ]);
        }
        return $data;
    }
}