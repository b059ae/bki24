<?php

namespace frontend\modules\request\models;

use Yii;
use yii\base\Model;

class LegalForm extends Model
{
    public $inn;
    public $founders;
    public $manager;


    const STATUS_ERROR = 0;
    const STATUS_SUCCESS = 1;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn'], 'required', 'message'=>'"{attribute}" является обязательным для заполнения'],
            [['inn'], 'number', 'message'=>'ИНН должен состоять только из цифр'],
            [['founders'], 'boolean'],
            [['manager'], 'boolean'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inn' => 'ИНН',
            'founders' => 'Проверить учредителей',
            'manager' => 'Проверить руководителя'
        ];
    }
} 