<?php

namespace frontend\modules\request\models;

use Yii;
use yii\base\Model;

class IpForm extends Model
{
    public $firstName;
    public $secondName;
    public $lastName;
    public $birthDate;
    public $regions;
    public $serPassport;
    public $numPassport;
    public $inn;


    const STATUS_ERROR = 0;
    const STATUS_SUCCESS = 1;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'regions', 'inn', 'secondName'], 'required', 'message'=>'"{attribute}" является обязательным для заполнения'],
            [['serPassport', 'numPassport', 'birthDate'], 'safe'],
            [['birthDate'],'date', 'format'=>'dd-mm-yyyy'],
            [['inn'], 'number', 'message'=>'ИНН должен состоять только из цифр'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inn' => 'ИНН',
            'firstName' => 'Имя',
            'secondName' => 'Отчетство',
            'lastName' => 'Фамилия',
            'birthDate' => 'Дата рождения',
            'regions' => 'Регион',
            'serPassport' => 'Серия паспорта',
            'numPassport' => 'Номер паспорта',
        ];
    }
}