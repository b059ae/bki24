<?php

namespace frontend\modules\request\helpers;

use common\models\BalanceOrg;

class BalanceHelper
{
    /**
     * Получение последней балансовой модели
     *
     * @param \common\models\BalanceOrg[] $balances
     * @return \common\models\BalanceOrg|null
     */
    public static function getLastPeriod($balances)
    {
        /** @var \common\models\BalanceOrg $last */
        $last = null;
        foreach ($balances as $balance) {
            if (is_null($last)) {
                $last = $balance;
                continue;
            }
            if ($last->year < $balance->year) {
                $last = $balance;
            }
        }
        if (is_null($last)) {
            $last = new BalanceOrg([
                'year' => date('Y'),
                'sales' => 0,
                'net_profit' => 0,
                'balance' => 0,
            ]);
        }
        return $last;
    }
}
