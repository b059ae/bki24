<?php
/**
 * @var $this yii\web\View
 * @var $request \common\models\IrbisRequest
 */

$this->title = $request->subject->getName();
$this->params['breadcrumbs'][] = ['url' => '/request', 'label' => 'Список запросов'];
$this->params['breadcrumbs'][] = $this->title;

$view = 'legal';
if ($request->request_type == \common\models\IrbisRequest::IRBIS_REQUEST_PEOPLE) {
    $view = 'people';
}
echo $this->render('view/' . $view, [
    'model' => $request,
]);
