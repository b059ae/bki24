<?php
/**
 * @var $this yii\web\View
 * @var $modelIp \frontend\modules\request\models\IpForm
 */

use frontend\modules\request\widgets\IndividualWidget;

$this->title = 'Запрос по физическому лицу';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="error-request" class="callout callout-danger" style="display: none;"></div>
<?= IndividualWidget::widget([
    'modelIp' => $modelIp,
]); ?>
