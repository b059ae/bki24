<?php
/**
 * @var $this yii\web\View
 * @var $modelLegal \frontend\modules\request\models\LegalForm
 */

use frontend\modules\request\widgets\LegalWidget;

$this->title = 'Запрос по юридическому лицу';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= LegalWidget::widget([
    'modelLegal' => $modelLegal,
]); ?>
