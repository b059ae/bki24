<?php
/**
 * @var $this yii\web\View
 * @var $model \common\models\IrbisRequest
 */
use common\models\ManagerFlOrg;
use frontend\modules\request\helpers\BalanceHelper;
use frontend\modules\request\widgets\legal\ArbitrationWidget;
use frontend\modules\request\widgets\legal\ArbitrOrgWidget;
use frontend\modules\request\widgets\legal\BadProviderCountOrgWidget;
use frontend\modules\request\widgets\legal\BalanceChartWidget;
use frontend\modules\request\widgets\legal\BancrotOrgWidget;
use frontend\modules\request\widgets\legal\FinanceOrgCheckWidget;
use frontend\modules\request\widgets\legal\FoundersOrgWidget;
use frontend\modules\request\widgets\legal\FsspOrgWidget;
use frontend\modules\request\widgets\legal\FsspWidget;
use frontend\modules\request\widgets\legal\GosContractOrgWidget;
use frontend\modules\request\widgets\legal\OkvedOrgWidget;
use frontend\modules\request\widgets\legal\SubjectOrgCheckWidget;
use frontend\modules\request\widgets\people\BadProviderWidget;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;

$manager = $model->managerFlOrgs;
if (!empty($manager)) {
    $manager = array_shift($manager);
} else {
    $manager = new ManagerFlOrg([
        'fio' => 'РУКОВОДИТЕЛЬ ОТСУТСТВУЕТ',
    ]);
}
/** @var ManagerFlOrg $manager */

$lastBalance = BalanceHelper::getLastPeriod($model->balanceOrgs);

?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_main" data-toggle="tab">Основная информация</a></li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                Подробная информация <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#tab_fssp" data-toggle="tab">ФССП</a></li>
                <li><a href="#tab_arbitr" data-toggle="tab">Арбитражные суды</a></li>
                <li><a href="#tab_bankrot" data-toggle="tab">Банкротства</a></li>
                <li><a href="#tab_gc" data-toggle="tab">Государсвенные контракты</a></li>
                <li><a href="#tab_okved" data-toggle="tab">Виды деятельности</a></li>
                <li><a href="#tab_founders" data-toggle="tab">Учредители</a></li>
                <li><a href="#tab_bad_provider" data-toggle="tab">Недобросовестные поставщики</a></li>
            </ul>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_main">

            <div class="row">
                <div class="col-md-4">
                    <div class="box box-primary collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $manager->fio; ?></h3>
                            <?php if (!empty($manager->inn)) : ?>
                            <p>
                                <strong>ИНН:</strong> <?= $manager->inn; ?>
                                <strong><?= $manager->role_name; ?></strong> <?= date('d.m.Y', strtotime($manager->grn_date)); ?>
                            </p>
                            <?php endif; ?>
                            <?php if ($manager->getManagerUrl()) : ?>
                                <p>
                                    <a target="_blank" href="<?=Url::to(['/request/default/view']) . '?id=' . $manager->getManagerUrl()?>">Подробнее</a>
                                </p>
                            <?php endif; ?>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="box box-primary collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $model->subject->full_name; ?></h3>
                            <p>
                                <strong>ИНН:</strong> <?= $model->subject->inn; ?>
                                <strong>ОГНР:</strong> <?= $model->subject->ogrn; ?>
                                <strong>КПП:</strong> <?= $model->subject->kpp; ?>
                            </p>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <?php
                            $subject = $model->subject;
                            $mainOkved = \common\models\OkvedOrg::findOne([
                                'irbis_request_id' => $model->id,
                                'type' => \common\models\OkvedOrg::BASE,
                            ]);
                            ?>
                            <table class="table table-striped table-bordered detail-view">
                                <tr>
                                    <th>Адрес</th>
                                    <td><?= $subject->address; ?></td>
                                </tr>
                                <?php if (!empty($mainOkved)) : ?>
                                    <tr>
                                        <th>Основной вид деятельности</th>
                                        <td><?= $mainOkved->code . ' ' . $mainOkved->name; ?></td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>Дата регистрации</th>
                                    <td><?= date('d.m.Y', strtotime($subject->date_born)); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?= SubjectOrgCheckWidget::widget([
                        'model' => $model->subject,
                    ])?>
                </div>
                <div class="col-md-3">
                    <?= FinanceOrgCheckWidget::widget([
                        'models' => $model->balanceOrgs,
                    ])?>
                </div>
                <div class="col-md-3">
                    <?= BadProviderCountOrgWidget::widget([
                        'model' => $model->badContracts,
                    ]) ?>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Исполнительные производства</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <?= FsspWidget::widget([
                                'models' => $model->fsspOrgs,
                            ]); ?>
                        </div>
                    </div>

                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Арбитражные суды</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <?= ArbitrationWidget::widget([
                                'models' => $model->arbitrSum,
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Финансовые результаты</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <p><strong>Баланс:</strong> <?= number_format($lastBalance->balance, 2, '.', ' '); ?></p>
                            <?= BalanceChartWidget::widget([
                                'models' => $model->balanceOrgs,
                                'layout' => 'balance-table',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane" id="tab_fssp">
            <h3 class="box-title"><strong>ФССП</strong></h3>
            <?= FsspOrgWidget::widget([
                    'models' => $model->fsspOrgs,
            ]); ?>
        </div>

        <div class="tab-pane" id="tab_arbitr">
            <h3 class="box-title"><strong>Арбитражные суды</strong></h3>
            <?= ArbitrOrgWidget::widget([
                'models' => $model->arbitrOrgs,
            ]); ?>
        </div>

        <div class="tab-pane" id="tab_bankrot">
            <h3 class="box-title"><strong>Банкротства</strong></h3>
            <?= BancrotOrgWidget::widget([
                'models' => $model->bankrotOrgs,
            ]); ?>
        </div>

        <div class="tab-pane" id="tab_gc">
            <h3 class="box-title"><strong>Государственные контракты</strong></h3>
            <?= GosContractOrgWidget::widget([
                'models' => $model->gcOrgs,
            ])?>
        </div>

        <div class="tab-pane" id="tab_okved">
            <h3 class="box-title"><strong>Виды деятельности</strong></h3>
            <?= OkvedOrgWidget::widget([
                'models' => $model->okvedOrgs,
            ]); ?>
        </div>

        <div class="tab-pane" id="tab_founders">
            <h3 class="box-title"><strong>Учредители</strong></h3>
            <?= FoundersOrgWidget::widget([
                'models' => $model->foundersFlOrgs,
            ]); ?>
        </div>
        <div id="tab_bad_provider" class="tab-pane">
            <h3 class="box-title"><strong>Список недобросовестных поставщиков</strong></h3>
            <?= BadProviderWidget::widget([
                'model' => $model->badContracts,
            ]); ?>
        </div>
    </div>
</div>
<?php $this->registerJs(<<<JS
    $('.open-tab').on("click", function() {
        $(".nav-tabs").find("li").removeClass("active");
    })
JS
    , View::POS_END);