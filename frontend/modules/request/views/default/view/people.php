<?php
/**
 * @var $this yii\web\View
 * @var $model \common\models\IrbisRequest
 */
use frontend\modules\request\widgets\people\ArbitrCountPeopleWidget;
use frontend\modules\request\widgets\people\ArbitrPeopleWidget;
use frontend\modules\request\widgets\people\BadProviderCountPeopleWidget;
use frontend\modules\request\widgets\people\BadProviderWidget;
use frontend\modules\request\widgets\people\BankrotPeopleWidget;
use frontend\modules\request\widgets\people\FsspPeopleCheckWidget;
use frontend\modules\request\widgets\people\FsspPeopleWidget;
use frontend\modules\request\widgets\people\GcPeopleWidget;
use frontend\modules\request\widgets\people\JudgeCountPeopleWidget;
use frontend\modules\request\widgets\people\JudgePeopleCheckWidget;
use frontend\modules\request\widgets\people\JudgePeopleWidget;
use frontend\modules\request\widgets\people\OrganisationPeopleWidget;
use frontend\modules\request\widgets\people\PledgePeopleCheckWidget;
use frontend\modules\request\widgets\people\PledgePeopleWidget;
use frontend\modules\request\widgets\people\SubjectPeopleCheckWidget;
use frontend\modules\request\widgets\people\TerroristWidget;
use yii\web\View;

?>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Основная информация</a></li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Подробная информация <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#tab_2" data-toggle="tab">ФССП</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Арбитражные суды</a></li>
                    <li><a href="#tab_4" data-toggle="tab">Банкротства</a></li>
                    <li><a href="#tab_5" data-toggle="tab">Государсвенные контракты</a></li>
                    <li><a href="#tab_6" data-toggle="tab">Организации</a></li>
                    <li><a href="#tab_7" data-toggle="tab">Список террористов</a></li>
                    <li><a href="#tab_8" data-toggle="tab">Залоги</a></li>
                    <li><a href="#tab_9" data-toggle="tab">Недобросовестные поставщики</a></li>
                    <li><a href="#tab_10" data-toggle="tab">Суды</a></li>
                </ul>
            </li>
        </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><strong><?= $model->subject->getName(); ?></strong></h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <?php if ($model->subject->getBirthDate()) : ?>
                                        <p><strong>Дата рождения: </strong><?= $model->subject->getBirthDate(); ?></p>
                                    <?php endif; ?>
                                    <?php if ($model->subject->getRegion()) : ?>
                                        <p><strong>Регион проижвания: </strong><?= $model->subject->getRegion(); ?></p>
                                    <?php endif; ?>
                                    <?php if ($model->subject->getPhone()) : ?>
                                        <p><strong>Телефон: </strong><?= $model->subject->getPhone(); ?></p>
                                    <?php endif; ?>
                                    <?php if ($model->subject->getOrg()) : ?>
                                        <p><strong>Руководитель компании: <br> </strong><?= $model->subject->getOrg(); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?= SubjectPeopleCheckWidget::widget([
                                'model' => $model->subject,
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?= JudgePeopleCheckWidget::widget([
                                'model' => $model->judgePeoples,
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= FsspPeopleCheckWidget::widget([
                                'model' => $model->fsspPeoples,
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= BadProviderCountPeopleWidget::widget([
                                'model' => $model->badContracts,
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= PledgePeopleCheckWidget::widget([
                                'model' => $model->pledgePeoples,
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><strong>Арбитражные суды</strong></h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <?= ArbitrCountPeopleWidget::widget([
                                        'models' => $model->arbitrPeoples,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><strong>Суды</strong></h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <?= JudgeCountPeopleWidget::widget([
                                        'models' => $model->judgePeoples,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tab_2" class="tab-pane">
            <h3 class="box-title"><strong>Список испольнительных производств</strong></h3>
            <?= FsspPeopleWidget::widget([
                'model' => $model->fsspPeoples,
            ]); ?>
        </div>
        <div id="tab_3" class="tab-pane">
            <h3 class="box-title"><strong>Арбитражные суды</strong></h3>
            <?= ArbitrPeopleWidget::widget([
                'model' => $model->arbitrPeoples,
            ]); ?>
        </div>
        <div id="tab_4" class="tab-pane">
            <h3 class="box-title"><strong>Банкротство</strong></h3>
            <?= BankrotPeopleWidget::widget([
                'model' => $model->bankrotPeoples,
            ]); ?>
        </div>
        <div id="tab_5" class="tab-pane">
            <h3 class="box-title"><strong>Список государственных контрактов</strong></h3>
            <?= GcPeopleWidget::widget([
                'model' => $model->gcPeoples,
            ]); ?>
        </div>
        <div id="tab_6" class="tab-pane">
            <h3 class="box-title"><strong>Список связанных организаций</strong></h3>
            <?= OrganisationPeopleWidget::widget([
                'model' => $model->organisationPeoples,
            ]); ?>
        </div>
        <div id="tab_7" class="tab-pane">
            <h3 class="box-title"><strong>Список террористов</strong></h3>
            <?= TerroristWidget::widget([
                'model' => $model->terroristPeoples,
            ]); ?>
        </div>
        <div id="tab_8" class="tab-pane">
            <h3 class="box-title"><strong>Список залогов</strong></h3>
            <?= PledgePeopleWidget::widget([
                'model' => $model->pledgePeoples,
            ]); ?>
        </div>
        <div id="tab_9" class="tab-pane">
            <h3 class="box-title"><strong>Список недобросовестных поставщиков</strong></h3>
            <?= BadProviderWidget::widget([
                'model' => $model->badContracts,
            ]); ?>
        </div>
        <div id="tab_10" class="tab-pane">
            <h3 class="box-title"><strong>Суды</strong></h3>
            <?= JudgePeopleWidget::widget([
                'model' => $model->judgePeoples,
            ]); ?>
        </div>
    </div>
</div>

<?php $this->registerJs(<<<JS
    $('.open-tab').on("click", function() {
        $(".nav-tabs").find("li").removeClass("active");
    })
JS
, View::POS_END);