<?php
/**
 * @var $this yii\web\View
 * @var $provider \yii\data\ActiveDataProvider
 * @var $modelLegal \frontend\modules\request\models\LegalForm
 * @var $modelIp \frontend\modules\request\models\IpForm
 */

use frontend\modules\request\widgets\IndividualWidget;
use frontend\modules\request\widgets\LegalWidget;

$this->title = 'Список запросов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Список запросов</h3>
    </div>
    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'layout' => "{items}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'request_type',
                    'value' => function ($data) {
                        /** @var $data \common\models\IrbisRequest */
                        return $data->getRequestType();
                    },
                ],
                [
                    'label' => 'Наименование',
                    'value' => function ($data) {
                        /** @var $data \common\models\IrbisRequest */
                        return $data->subject->getName();
                    },
                ],
                [
                    'label' => 'Идентификация',
                    'value' => function ($data) {
                        /** @var $data \common\models\IrbisRequest */
                        return $data->subject->getMainNumber();
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {delete}',
                ],
            ],
        ]); ?>
    </div>
</div>