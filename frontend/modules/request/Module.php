<?php

namespace frontend\modules\request;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\request\controllers';
}
