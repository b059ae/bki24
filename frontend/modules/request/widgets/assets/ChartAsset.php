<?php

namespace frontend\modules\request\widgets\assets;

use yii\web\AssetBundle;

class ChartAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/request/widgets/assets';
    public $js = [
        'js/Chart.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
