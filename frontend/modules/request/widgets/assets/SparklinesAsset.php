<?php

namespace frontend\modules\request\widgets\assets;

use yii\web\AssetBundle;

class SparklinesAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins/sparkline';
    public $js = [
        'jquery.sparkline.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
