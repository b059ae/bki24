<?php

namespace frontend\modules\request\widgets\people;

use common\components\irbis\request\PeopleRequest;
use common\models\BankrotOrg;
use common\models\BankrotPeople;
use common\models\FsspPeople;
use common\models\IrbisPeople;
use common\models\TerroristPeople;
use frontend\modules\request\widgets\AbstractCheckWidget;

class FsspPeopleCheckWidget extends AbstractCheckWidget
{
    /**
     * @var FsspPeople
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_2';
    /**
     * @var string
     */
    public $icon = 'ion ion-stats-bars';

    public function run()
    {
        parent::run();
        $signals = [];
        $data = [
            'count' => 0,
            'openIp' => 0,
            'closeIp' => 0,
        ];
        foreach ($this->model as $item){
            $data['count']++;
            if (!empty($item->date_end)) {
                $data['closeIp']++;
            } else {
                $data['openIp']++;
            }
        }
        if ($data['openIp'] > 0) {
            $signals[] = 'Имеются открытые ИП!';
        }

        if (empty($signals) && $data['count'] == 0) {
            return $this->renderSuccess('ФССП', 'Не обнаружено ИП');
        } elseif ($data['closeIp'] > 0) {
            return $this->renderInfo('ФССП', 'Присутствуют закрытые ИП');
        } else {
            return $this->renderWarning('ФССП', $signals);
        }
    }
}
