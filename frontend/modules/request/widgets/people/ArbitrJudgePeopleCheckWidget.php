<?php

namespace frontend\modules\request\widgets\people;

use common\models\ArbitrPeople;
use frontend\modules\request\widgets\AbstractCheckWidget;

class ArbitrJudgePeopleCheckWidget extends AbstractCheckWidget
{
    /**
     * @var ArbitrPeople
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_3';
    /**
     * @var string
     */
    public $icon = 'ion ion-person-stalker';

    public function run()
    {
        parent::run();
        $signals = [];
        $data = [
            'count' => 0,
            'other' => 0,
            'R' => 0,
            'P' => 0,
        ];
        foreach ($this->model as $model) {
            $data['count']++;
            if (isset($data[$model->type_member])) {
                $data[$model->type_member]++;
            } else {
                $data['other']++;
            }
        }

        if ($data['count'] > 0 && $data['R'] > 0) {
            $signals[] = 'Выступал ответчиком: ' . $data['R'];
        }

        if (empty($signals)) {
            return $this->renderSuccess('Арбитраж', 'Суды отсутствуют');
        } else {
            return $this->renderWarning('Арбитраж', $signals);
        }
    }
}
