<?php

namespace frontend\modules\request\widgets\people;

use common\models\BadContract;
use frontend\modules\request\widgets\AbstractCheckWidget;

class BadProviderCountPeopleWidget extends AbstractCheckWidget
{
    /**
     * @var BadContract[]
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_9';
    /**
     * @var string
     */
    public $icon = 'ion ion-clipboard';

    public function run()
    {
        parent::run();
        $count = count($this->model);
        if ($count > 0) {
            $signals[] = 'Недобросовестный поставщик';
        }
        if (empty($signals)) {
            return $this->renderSuccess('Поставщик', 'Добросовестный поставщик');
        } else {
            return $this->renderWarning('Поставщик', $signals);
        }
    }
}
