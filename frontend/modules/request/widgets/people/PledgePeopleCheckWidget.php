<?php

namespace frontend\modules\request\widgets\people;

use common\models\FsspPeople;
use common\models\PledgePeople;
use frontend\modules\request\widgets\AbstractCheckWidget;

class PledgePeopleCheckWidget extends AbstractCheckWidget
{
    /**
     * @var PledgePeople
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_8';
    /**
     * @var string
     */
    public $icon = 'ion ion-model-s';

    public function run()
    {
        parent::run();
        $signals = [];
        $count = 0;
        foreach ($this->model as $item){
            $count++;
        }
        if ($count > 0) {
            $signals[] = 'Имеются зологи!';
        }

        if (empty($signals)) {
            return $this->renderSuccess('Залоги', 'Залоги отсутствуют');
        } else {
            return $this->renderWarning('Залоги', $signals);
        }
    }
}
