<?php

namespace frontend\modules\request\widgets\people;

use common\models\ArbitrPeople;
use yii\base\Widget;

class ArbitrPeopleWidget extends Widget
{
    /**
     * @var ArbitrPeople[]
     */
    public $model;

    public $layout = 'arbitr-people';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
