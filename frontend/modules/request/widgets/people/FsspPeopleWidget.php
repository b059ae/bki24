<?php

namespace frontend\modules\request\widgets\people;

use common\models\FsspPeople;
use yii\base\Widget;

class FsspPeopleWidget extends Widget
{
    /**
     * @var FsspPeople[]
     */
    public $model;

    public $layout = 'fssp-people';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
