<?php

namespace frontend\modules\request\widgets\people;

use common\models\BankrotPeople;
use yii\base\Widget;

class BankrotPeopleWidget extends Widget
{
    /**
     * @var BankrotPeople[]
     */
    public $model;

    public $layout = 'bankrot-people';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
