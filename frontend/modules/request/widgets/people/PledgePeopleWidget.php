<?php

namespace frontend\modules\request\widgets\people;

use common\models\PledgePeople;
use yii\base\Widget;

class PledgePeopleWidget extends Widget
{
    /**
     * @var PledgePeople[]
     */
    public $model;

    public $layout = 'pledge-people';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
