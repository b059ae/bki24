<?php

namespace frontend\modules\request\widgets\people;

use common\models\BankrotPeople;
use frontend\modules\request\widgets\AbstractCheckWidget;
use yii\base\Widget;

class BankrotCountPeopleWidget extends AbstractCheckWidget
{
    /**
     * @var BankrotPeople[]
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_1';
    /**
     * @var string
     */
    public $icon = 'ion ion-clipboard';

    public function run()
    {
        parent::run();
        $count = count($this->model);
        if ($count > 0) {
            $signals[] = 'Объявлен банкротом!';
            $this->link = 'tab_4';
        }
        if (empty($signals)) {
            return $this->renderSuccess('Банкротство', 'Не является банкротом');
        } else {
            return $this->renderWarning('Банкрот!', $signals);
        }
    }
}
