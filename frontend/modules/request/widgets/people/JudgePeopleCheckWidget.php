<?php

namespace frontend\modules\request\widgets\people;

use common\models\FsspPeople;
use common\models\JudgePeople;
use common\models\PledgePeople;
use frontend\modules\request\widgets\AbstractCheckWidget;

class JudgePeopleCheckWidget extends AbstractCheckWidget
{
    /**
     * @var JudgePeople
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_10';
    /**
     * @var string
     */
    public $icon = 'ion ion-document';

    public function run()
    {
        parent::run();
        $signals = [];
        $data = [
            'count' => 0,
            'A' => 0,
            'U' => 0,
            'G' => 0,
            'O' => 0,
            'def' => 0,
            'plan' => 0,
        ];
        foreach ($this->model as $item) {
            $data['count']++;
            if (isset($data[$item->type_cause])) {
                $data[$item->type_cause]++;
            }
            if (isset($data[$item->role])) {
                $data[$item->role]++;
            }
        }
        if ($data['count'] > 0 && $data['U'] > 0) {
            $signals[] = 'Участник уголовного дела!';
        }

        if (empty($signals)) {
            return $this->renderSuccess('Суды', 'Не участвовал в судебных процессах');
        } else {
            return $this->renderWarning('Суды', $signals);
        }
    }
}
