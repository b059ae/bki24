<?php

namespace frontend\modules\request\widgets\people;

use common\components\irbis\request\PeopleRequest;
use common\models\BadContract;
use common\models\BankrotOrg;
use common\models\BankrotPeople;
use common\models\IrbisPeople;
use common\models\TerroristPeople;
use frontend\modules\request\widgets\AbstractCheckWidget;

class SubjectPeopleCheckWidget extends AbstractCheckWidget
{
    /**
     * @var IrbisPeople
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_1';
    /**
     * @var string
     */
    public $icon = 'ion ion-person';

    public function run()
    {
        parent::run();
        $signals = [];

        $fms = IrbisPeople::findOne([
            'irbis_request_id' => $this->model->irbis_request_id,
            'fms' => IrbisPeople::FMS_NO_VALID
        ]);
        if (!empty($fms)) {
            $signals[] = 'Паспорт недействителен!';
            $this->link = 'tab_1';
        }

        $contract = BadContract::findOne([
            'irbis_request_id' => $this->model->irbis_request_id,
        ]);
        if (!empty($contract)) {
            $signals[] = 'Недобросовестный поставщик!';
            $this->link = 'tab_9';
        }

        $bankrot = BankrotPeople::findOne([
            'irbis_request_id' => $this->model->irbis_request_id,
        ]);
        if (!empty($bankrot)) {
            $signals[] = 'Объявлен банкротом!';
            $this->link = 'tab_4';
        }

        $terrorist = TerroristPeople::findOne([
            'irbis_request_id' => $this->model->irbis_request_id,
        ]);
        if (!empty($terrorist)) {
            $signals[] = 'Находится в списке террористов!';
            $this->link = 'tab_7';
        }

        if (empty($signals)) {
            return $this->renderSuccess('Актуально', 'Не обнаружено серьезных проблем');
        } else {
            return $this->renderWarning('Внимание!', $signals);
        }
    }
}
