<?php

namespace frontend\modules\request\widgets\people;

use common\models\JudgePeople;
use yii\base\Widget;

class JudgePeopleWidget extends Widget
{
    /**
     * @var JudgePeople[]
     */
    public $model;

    public $layout = 'judge-people';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
