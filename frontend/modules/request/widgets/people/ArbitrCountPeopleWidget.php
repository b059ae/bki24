<?php

namespace frontend\modules\request\widgets\people;

use common\models\ArbitrPeople;
use yii\base\Widget;

class ArbitrCountPeopleWidget extends Widget
{
    /**
     * @var ArbitrPeople[]
     */
    public $models;

    public $layout = 'arbitr-count-people';

    public function run()
    {
        parent::run();
        $data = [
            'count' => 0,
            'R' => 0,
            'P' => 0,
            'other' => 0,
        ];
        foreach ($this->models as $model) {
            $data['count']++;
            if (isset($data[$model->type_member])) {
                $data[$model->type_member]++;
            } else {
                $data['other']++;
            }
        }

        return $this->render($this->layout, $data);
    }
}
