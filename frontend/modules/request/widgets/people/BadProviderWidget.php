<?php

namespace frontend\modules\request\widgets\people;

use common\models\BadContract;
use yii\base\Widget;

class BadProviderWidget extends Widget
{
    /**
     * @var BadContract[]
     */
    public $model;

    public $layout = 'bad-provider';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
