<?php
/**
 * @var int $count
 * @var int $R
 * @var int $P
 * @var int $other
 * @var $this yii\web\View
 */
use dosamigos\chartjs\ChartJs;
use frontend\modules\request\widgets\assets\ChartAsset;
use yii\web\View;

//ChartAsset::register($this);
?>

<?php if ($count > 0): ?>
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th class="text-center">Количество</th>
            <th class="text-center">Истец</th>
            <th class="text-center">Ответчик</th>
            <th class="text-center">Другие</th>
        </tr>
        <tr>
            <td class="text-center"><?= $count; ?></td>
            <td class="text-center"><?= $P ?> (<?=round($P/$count*100, 2)?>%)</td>
            <td class="text-center"><?= $R ?> (<?=round($R/$count*100, 2)?>%)</td>
            <td class="text-center"><?= $other ?> (<?=round($other/$count*100, 2)?>%)</td>
        </tr>
        </tbody>
    </table>
    <?= ChartJs::widget([
            'type' => 'pie',
            'clientOptions' => [
                'legend' => [
                    'position' => 'right'
                ],
            ],
            'data' => [
                'labels' => ['Истец', 'Ответчик', 'Другие'],
                'datasets' => [
                    [
                        'data' => [$P, $R, $other],
                        'backgroundColor' => ["#00a65a", "#f56954", "#f39c12"]
                    ],
                ]
            ]
    ]);?>
<?php else:?>
    Не участвовал в судебных процессах
<?php endif;?>
