<?php
/**
 * @var $model \common\models\JudgePeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">Номер дела</th>
            <th class="text-left">Тип дела</th>
            <th class="text-left">Решение</th>
            <th class="text-left">Роль</th>
            <th class="text-left">Дата создания</th>
            <th class="text-left">Дата закрытия</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->case_number ?></td>
                <td class="text-left"><?=$item->getType(); ?></td>
                <td class="text-left"><?=$item->resolution; ?></td>
                <td class="text-left"><?=$item->getRole(); ?></td>
                <td class="text-left"><?=Yii::$app->formatter->asDate($item->date, 'php:d.m.Y') ?></td>
                <td class="text-left"><?=Yii::$app->formatter->asDate($item->end_date, 'php:d.m.Y') ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
