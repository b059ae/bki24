<?php
/**
 * @var $model \common\models\TerroristPeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">ФИО</th>
            <th class="text-left">Дата рождения</th>
            <th class="text-left">Место рождения</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->name ?></td>
                <td class="text-left"><?=Yii::$app->formatter->asDate($item->birth_date, 'php:d.m.Y') ?></td>
                <td class="text-left"><?=$item->birth_place ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
