<?php
/**
 * @var $model \common\models\OrganisationPeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">Наименование</th>
            <th class="text-left">Должность</th>
            <th class="text-left">ИНН</th>
            <th class="text-left">ОГРН</th>
            <th class="text-left">Статус</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->name ?></td>
                <td class="text-left"><?=$item->position ?></td>
                <td class="text-left"><?=$item->inn ?></td>
                <td class="text-left"><?=$item->ogrn ?></td>
                <td class="text-left"><?=$item->getStatus(); ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
