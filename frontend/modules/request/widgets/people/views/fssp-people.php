<?php
/**
 * @var $model \common\models\FsspPeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">Должник</th>
            <th class="text-left">Код</th>
            <th class="text-left">Отдел ФССП</th>
            <th class="text-left">Предмет исполнения</th>
            <th class="text-left">Сумма</th>
            <th class="text-left">Дата завершения ИП</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->name ?></td>
                <td class="text-left"><?=$item->code ?></td>
                <td class="text-left"><?=$item->fssp_department ?></td>
                <td class="text-left"><?=$item->subject ?></td>
                <td class="text-left"><?=$item->sum ?></td>
                <td class="text-left"><?=\Yii::$app->formatter->asDate($item->date_end, 'php:d.m.Y') ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
