<?php
/**
 * @var $model \common\models\GcPeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">Результат</th>
            <th class="text-left">Заказчик</th>
            <th class="text-left">Дата заключения</th>
            <th class="text-left">Сумма</th>
            <th class="text-left">Статус</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->result ?></td>
                <td class="text-left"><?=$item->customer ?></td>
                <td class="text-left"><?=$item->date ?></td>
                <td class="text-left"><?=$item->sum ?></td>
                <td class="text-left"><?=$item->status; ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
