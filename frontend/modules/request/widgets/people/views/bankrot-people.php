<?php
/**
 * @var $model \common\models\BankrotPeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">Банкрот</th>
            <th class="text-left">Суд</th>
            <th class="text-left">Код</th>
            <th class="text-left">Дата</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->bankrot ?></td>
                <td class="text-left"><?=$item->judge ?></td>
                <td class="text-left"><?=$item->code ?></td>
                <td class="text-left"><?=Yii::$app->formatter->asDate($item->date_bankrot, 'php:d.m.Y') ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
