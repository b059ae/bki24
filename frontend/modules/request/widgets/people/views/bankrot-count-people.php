<?php
/**
 * @var int $count
 */
?>
<?php if ($count > 0):?>
<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-right" style="width: 50%;">Количество</th>
            <td class="text-left"><?= $count; ?></td>
        </tr>
    </tbody>
</table>
<?php else:?>
    Не является банкротом
<?php endif;?>