<?php
/**
 * @var $model \common\models\PledgePeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">ФИО</th>
            <th class="text-left">Паспорт</th>
            <th class="text-left">Дата рождения</th>
            <th class="text-left">Залогодержатель</th>
            <th class="text-left">Тип залога</th>
            <th class="text-left">Дата начала</th>
            <th class="text-left">Дата окончания</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->fio ?></td>
                <td class="text-left"><?=$item->passport ?></td>
                <td class="text-left"><?=$item->birth_date ?></td>
                <td class="text-left"><?=$item->pawnbroker ?></td>
                <td class="text-left"><?=$item->type_pledge; ?></td>
                <td class="text-left"><?=$item->date_add; ?></td>
                <td class="text-left"><?=$item->date_end; ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
