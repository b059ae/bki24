<?php
/**
 * @var int $count
 * @var int $sum
 */

?>

<?php if ($count > 0):?>
<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-center">Количество контрактов</th>
            <td class="text-center"><?= $count; ?></td>
        </tr>
        <tr>
            <th class="text-center">Общая сумма</th>
            <td class="text-center"><?= number_format($sum, 2, ',', ' '); ?> руб.</td>
        </tr>
    </tbody>
</table>
<?php else:?>
    Отсутсвует в списках недобросовестных поставщиков
<?php endif;?>