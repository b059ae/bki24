<?php
/**
 * @var $model \common\models\BadContract
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">Реестровый номер</th>
            <th class="text-left">Дата включения</th>
            <th class="text-left">Дата исключения</th>
            <th class="text-left">Дата заключения контракта</th>
            <th class="text-left">Сумма</th>
            <th class="text-left">Причина</th>
            <th class="text-left">Выписка</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->registry_number ?></td>
                <td class="text-left"><?=$item->date_start ?></td>
                <td class="text-left"><?=$item->date_end ?></td>
                <td class="text-left"><?=$item->date_contract ?></td>
                <td class="text-left"><?=$item->sum; ?></td>
                <td class="text-left"><?=$item->reason; ?></td>
                <td class="text-left"><a target="_blank" href="<?=$item->url;?>">Подробнее</a></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
