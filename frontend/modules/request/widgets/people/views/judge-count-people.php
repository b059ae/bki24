<?php
/**
 * @var int $count
 * @var int $def
 * @var int $plan
 * @var int $A
 * @var int $U
 * @var int $G
 * @var int $O
 */
use dosamigos\chartjs\ChartJs;
use frontend\modules\request\widgets\assets\ChartAsset;
use yii\web\View;

//ChartAsset::register($this);
?>

<?php if ($count > 0): ?>
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th class="text-center">Количество</th>
            <th class="text-center">Истец</th>
            <th class="text-center">Ответчик</th>
        </tr>
        <tr>
            <td class="text-center"><?= $count; ?></td>
            <td class="text-center"><?= $plan ?> (<?=round($plan/$count*100, 2)?>%)</td>
            <td class="text-center"><?= $def ?> (<?=round($def/$count*100, 2)?>%)</td>
        </tr>
        </tbody>
    </table>
    <?= ChartJs::widget([
            'type' => 'pie',
            'clientOptions' => [
                'legend' => [
                    'position' => 'right'
                ],
            ],
            'data' => [
                'labels' => ['Административные дела', 'Уголовные дела', 'Гражданские дела', 'Оспаривание решений'],
                'datasets' => [
                    [
                        'data' => [$A, $U, $G, $O],
                        'backgroundColor' => ["#00c0ef", "#f56954", "#f39c12", "#3c8dbc"]
                    ],
                ]
            ],
    ]);?>
<?php else:?>
    Не участвовал в судебных процессах
<?php endif;?>


