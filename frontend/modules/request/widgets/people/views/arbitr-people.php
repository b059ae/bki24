<?php
/**
 * @var $model \common\models\ArbitrPeople
 */
?>

<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-left">Номер дела</th>
            <th class="text-left">Участник</th>
            <th class="text-left">Тип участника</th>
            <th class="text-left">Суд</th>
            <th class="text-left">Дата</th>
            <th class="text-left">Подробнее</th>
        </tr>
        <?php foreach ($model as $item) : ?>
            <tr>
                <td class="text-left"><?=$item->name ?></td>
                <td class="text-left"><?=$item->member ?></td>
                <td class="text-left"><?=$item->getTypeMember(); ?></td>
                <td class="text-left"><?=$item->judge ?></td>
                <td class="text-left"><?=Yii::$app->formatter->asDate($item->date, 'php:d.m.Y') ?></td>
                <td class="text-left"><a target="_blank" href="<?= $item->getFullUrl(); ?>">Открыть дело</a></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
