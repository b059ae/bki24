<?php

namespace frontend\modules\request\widgets\people;

use common\models\TerroristPeople;
use yii\base\Widget;

class TerroristWidget extends Widget
{
    /**
     * @var TerroristPeople[]
     */
    public $model;

    public $layout = 'terrorist';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
