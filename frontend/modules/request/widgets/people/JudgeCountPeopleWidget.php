<?php

namespace frontend\modules\request\widgets\people;

use common\models\JudgePeople;
use yii\base\Widget;

class JudgeCountPeopleWidget extends Widget
{
    /**
     * @var JudgePeople[]
     */
    public $models;

    public $layout = 'judge-count-people';

    public function run()
    {
        parent::run();
        $data = [
            'count' => 0,
            'A' => 0,
            'U' => 0,
            'G' => 0,
            'O' => 0,
            'def' => 0,
            'plan' => 0,
        ];
        foreach ($this->models as $model) {
            $data['count']++;
            if (isset($data[$model->type_cause])) {
                $data[$model->type_cause]++;
            }
            if (isset($data[$model->role])) {
                $data[$model->role]++;
            }
        }

        return $this->render($this->layout, $data);
    }
}
