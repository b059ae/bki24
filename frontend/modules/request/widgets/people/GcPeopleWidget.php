<?php

namespace frontend\modules\request\widgets\people;

use common\models\GcPeople;
use yii\base\Widget;

class GcPeopleWidget extends Widget
{
    /**
     * @var GcPeople[]
     */
    public $model;

    public $layout = 'gc-people';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
