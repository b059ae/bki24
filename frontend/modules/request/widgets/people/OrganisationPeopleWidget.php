<?php

namespace frontend\modules\request\widgets\people;

use common\models\OrganisationPeople;
use yii\base\Widget;

class OrganisationPeopleWidget extends Widget
{
    /**
     * @var OrganisationPeople[]
     */
    public $model;

    public $layout = 'organisation';

    public function run()
    {
        parent::run();
        return $this->render($this->layout, ['model' => $this->model]);
    }
}
