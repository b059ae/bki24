<?php

namespace frontend\modules\request\widgets;

class IndividualWidget extends \yii\base\Widget
{
    public $modelIp;

    function run()
    {
        return $this->render('individual', [
            'modelIp' => $this->modelIp,
        ]);
    }
}
