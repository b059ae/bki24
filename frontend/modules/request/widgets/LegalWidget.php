<?php

namespace frontend\modules\request\widgets;

class LegalWidget extends \yii\base\Widget
{
    public $modelLegal;

    function run()
    {
        return $this->render('legal', [
            'modelLegal' => $this->modelLegal,
        ]);
    }
}
