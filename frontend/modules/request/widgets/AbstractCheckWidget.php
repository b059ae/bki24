<?php

namespace frontend\modules\request\widgets;


use yii\base\Widget;

abstract class AbstractCheckWidget extends Widget
{
    /**
     * @var string
     */
    public $icon;
    /**
     * @var string
     */
    public $link;

    /**
     * @var string
     */
    public $layoutSuccess = 'signal-success';
    /**
     * @var string
     */
    public $layoutInfo = 'signal-info';
    /**
     * @var string
     */
    public $layoutWarning = 'signal-wrong';

    /**
     * Рендеринг плитки с успешной проверкой
     *
     * @param string $head
     * @param string $title
     * @return string
     */
    protected function renderSuccess($head, $title)
    {
        return $this->render($this->layoutSuccess, [
            'head' => $head,
            'info' => $title,
            'icon' => $this->icon,
            'link' => $this->link,
        ]);
    }

    /**
     * Рендеринг плитки с имеющей мелкие проблемы проверкой
     *
     * @param string $head
     * @param string $title
     * @return string
     */
    protected function renderInfo($head, $title)
    {
        return $this->render($this->layoutInfo, [
            'head' => $head,
            'info' => $title,
            'icon' => $this->icon,
            'link' => $this->link,
        ]);
    }

    /**
     * Рендеринг плитки с имеющей серьезные проблемы проверкой
     *
     * @param string $head
     * @param array $signals
     * @return string
     */
    protected function renderWarning($head, $signals)
    {
        return $this->render($this->layoutWarning, [
            'head' => $head,
            'signals' => $signals,
            'icon' => $this->icon,
            'link' => $this->link,
        ]);
    }
}
