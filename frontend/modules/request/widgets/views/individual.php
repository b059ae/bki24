<?php
/**
 * @var $this yii\web\View
 * @var $modelIp \frontend\modules\request\models\IpForm
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

?>

<div class="container">
    <div class="col-md-6 col-md-push-3 col-sm-push-0 col-sm-12">
        <div class="box box-primary">

            <div class="box-header with-border">
                <h3 class="box-title">Форма запроса</h3>
            </div>

            <div class="box-body">
                <div id="error-request" class="callout callout-danger" style="display: none;"></div>
                <?php $form = ActiveForm::begin([
                    'id' => 'ip-form',
                    'options' => [
                        'class' => 'reg-form'
                    ]
                ]); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?=$form->field($modelIp, 'lastName', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg',
                                'id' => "lastName",
                            ],
                        ])->textInput([
                            'placeholder' => $modelIp->getAttributeLabel('lastName').'*'
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($modelIp, 'firstName', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg',
                                'id' => "firstName",
                                'enableClientValidation' => false,
                            ],
                        ])->textInput([
                            'placeholder' => $modelIp->getAttributeLabel('firstName').'*'
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($modelIp, 'secondName', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg',
                                'id' => "secondName",
                                'enableClientValidation' => false,
                            ],
                        ])->textInput([
                            'placeholder' => $modelIp->getAttributeLabel('secondName')
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($modelIp, 'serPassport', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg',
                                'id' => "serPassport",
                            ],
                        ])->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '9999'
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($modelIp, 'numPassport', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg',
                                'id' => "numPassport",
                            ],
                        ])->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '999999'
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($modelIp, 'birthDate', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg ',
                                'id' => "birthDate",
                            ],
                        ])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => ['alias' =>  'dd-mm-yyyy']
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($modelIp, 'inn', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg',
                                'id' => 'inn',
                            ],
                        ])->textInput([
                            'placeholder' => $modelIp->getAttributeLabel('inn')
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($modelIp, 'regions', [
                            'template' => "{label}\n{input}\n{error}",
                            'inputOptions' => [
                                'class' => 'form-control input-lg',
                                'id' => 'regions',
                                'enableClientValidation' => false,
                            ],
                        ])->dropDownList(ArrayHelper::map(\common\models\Regions::find()->all(), 'code', 'name'), [
                            'prompt' => 'Регион*',
                        ]); ?>
                    </div>
                </div>

                <div class="col-md-12 text-right">
                    <?= Html::submitButton('Проверить', ['id' => 'btn-ip', 'class' => 'btn btn-primary enter']); ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="wait-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Выполняется запрос...</h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100"
                         aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
$url = Url::to(['/request/default/individual-request']);
$this->registerJs(<<<JS
    $(document).on("submit", "#ip-form", function(e) {
        e.preventDefault();
        var form = $(this);
        var modal = $("#wait-modal");
        var error = $("#error-request");
        $.ajax({
            url: "$url",
            dataType: "json",
            type: "post",
            data: form.serialize(),
            beforeSend: function() {
                modal.modal('show');
            }
        })
        .done(function(response) {
            modal.modal('hide');
            error.hide().empty();
            switch (response.status) {
                case 0: case "0":
                    error.show().html(response.errors);
                    break;
                case 1: case "1":
                    window.location.href = response.url;
                    break;
                default: 
                    break;
            }
        })
        .fail(function(XHR, textStatus, responseText) {
            modal.modal('hide');
            error.show().text(textStatus + ': ' + XHR.responseTexts);
        });
    });
JS
, View::POS_END);
?>