<?php

namespace frontend\modules\request\widgets\legal;

use common\models\BalanceOrg;
use frontend\modules\request\widgets\AbstractCheckWidget;

class FinanceOrgCheckWidget extends AbstractCheckWidget
{
    /**
     * @var BalanceOrg[]
     */
    public $models;
    /**
     * @var string
     */
    public $link = 'finance';
    /**
     * @var string
     */
    public $icon = 'ion ion-stats-bars';

    public function run()
    {
        parent::run();

        if (empty($this->models)) {
            return $this->renderInfo('0 / 0', 'Нет финансовых данных');
        }

        /** @var BalanceOrg[] $byYears */
        $byYears = [];
        foreach ($this->models as $balance) {
            $byYears[$balance->year] = $balance;
        }
        krsort($byYears);

        $last = array_shift($byYears);
        $prev = array_shift($byYears);

        $lastProfit = 0;
        if ($last->sales != 0) {
            $lastProfit = round($last->net_profit / $last->sales * 100, 2);
        }
        $prevProfit = 0;
        if ($prev->sales != 0) {
            $prevProfit = round($prev->net_profit / $prev->sales * 100, 2);
        }

        $head = $lastProfit . '% / ' . $prevProfit . '%';
        if ($lastProfit > 0 && $prevProfit > 0) {
            return $this->renderSuccess($head, 'Стабильная прибыль');
        }
        if ($lastProfit > 0 || $prevProfit > 0) {
            return $this->renderInfo($head, 'Имеются проблемы с прибылью');
        }
        return $this->renderWarning($head, [
            'Отсутствует положительная прибыль'
        ]);
    }
}
