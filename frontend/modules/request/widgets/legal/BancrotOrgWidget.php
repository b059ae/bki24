<?php

namespace frontend\modules\request\widgets\legal;

use common\models\BankrotOrg;
use yii\base\Widget;

class BancrotOrgWidget extends Widget
{
    /**
     * @var BankrotOrg[]
     */
    public $models;
    /**
     * @var string
     */
    public $layout = 'bankrot-org-table';

    public function run()
    {
        parent::run();

        $data = [
            'inn' => [],
            'name' => [],
        ];

        foreach ($this->models as $model) {
            if ($model->type_request == BankrotOrg::TYPE_REQUEST_INN) {
                $data['inn'][] = $model;
            } else {
                $data['name'][] = $model;
            }
        }

        return $this->render($this->layout, $data);
    }
}
