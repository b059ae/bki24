<?php

namespace frontend\modules\request\widgets\legal;


class BalanceChartWidget extends \yii\base\Widget
{
    /**
     * @var \common\models\BalanceOrg[]
     */
    public $models;

    public $layout = 'balance-chart';

    public $steps = 4;

    public function run()
    {
        parent::run();
        $data = [
            'size' => 1000,
            'steps' => $this->steps,
            'label' => [],
            'profit' => [],
            'sales' => [],
            'spend' => [],
        ];
        $max = 0;
        foreach ($this->models as $model) {
            $data['label'][] = $model->year;
            $data['profit'][] = $model->net_profit;
            $data['sales'][] = $model->sales;
            $data['spend'][] = $model->sales - $model->net_profit;

            $max = max($max, $model->net_profit, $model->sales, $model->sales - $model->net_profit);
        }

        $max = $max / $this->steps;
        $data['size'] = round($max, -(mb_strlen($max)-1));

        return $this->render($this->layout, $data);
    }
}
