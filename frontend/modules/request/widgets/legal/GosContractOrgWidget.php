<?php

namespace frontend\modules\request\widgets\legal;

use common\models\GcOrg;
use yii\base\Widget;

class GosContractOrgWidget extends Widget
{
    /**
     * @var GcOrg[]
     */
    public $models;
    /**
     * @var string
     */
    public $layout = 'gc-org-table';

    public function run()
    {
        parent::run();

        return $this->render($this->layout, [
            'models' => $this->models,
        ]);
    }
}
