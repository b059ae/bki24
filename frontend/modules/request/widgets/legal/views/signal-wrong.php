<?php
/**
 * @var string $head
 * @var array $signals
 * @var string $icon
 * @var string $link
 */
?>

<div class="small-box bg-red">
    <div class="inner">
        <h3><?= $head; ?></h3>
        <ul><?php foreach ($signals as $signal) : ?>
            <li><?= $signal; ?></li>
        <?php endforeach; ?></ul>
    </div>
    <div class="icon">
        <i class="<?= $icon; ?>"></i>
    </div>
    <a href="#<?= $link; ?>" data-toggle="tab" class="small-box-footer open-tab">
        Подробнее <i class="fa fa-arrow-circle-right"></i>
    </a>
</div>