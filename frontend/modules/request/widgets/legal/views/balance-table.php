<?php
/**
 * @var $this yii\web\View
 * @var array $label
 * @var array $profit
 * @var array $sales
 * @var array $spend
 * @var integer $size
 * @var integer $steps
 */

?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>Год</th>
        <th>Выручка</th>
        <th>Затраты</th>
        <th>Чистая прибыль</th>
        <th>Прибыльность</th>
        <th>Прирост прибыли</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $c = count($label) - 1;
    for($i=$c; $i>=0; $i--) :
        $class = '';
        if ($profit[$i] < 0) {
            $class = ' class="text-red"';
        }
        $previous = '';
        if ($i > 0) {
            if ($profit[$i] < $profit[$i-1]) {
                $division = empty($profit[$i]) ? $profit[$i-1] : $profit[$i];
                $previous = -round(abs($profit[$i-1]/$division) * 100, 2);
            } else {
                $division = empty($profit[$i-1]) ? $profit[$i] : $profit[$i-1];
                $previous = round(abs($profit[$i]/$division) * 100, 2);
            }
        }
        ?>
        <tr<?= $class; ?>>
            <td><?= $label[$i]; ?></td>
            <td class="text-right"><?= number_format($sales[$i], 2, '.', ' '); ?></td>
            <td class="text-right"><?= number_format($spend[$i], 2, '.', ' '); ?></td>
            <td class="text-right"><?= number_format($profit[$i], 2, '.', ' '); ?></td>
            <td class="text-center"><?= ($sales[$i]) ? round($profit[$i]/$sales[$i] * 100, 2) . '%' : '-'; ?></td>
            <td class="text-center"><?= !empty($previous) ? $previous . '%' : '-'; ?></td>
        </tr>
    <?php endfor; ?>
    </tbody>
</table>
