<?php
/**
 * @var \common\models\FsspOrg[] $models
 */
?>

<table class="table table-bordered">
    <tbody>
    <tr>
        <th class="text-left">Наименование должника</th>
        <th class="text-left">Адрес должника</th>
        <th class="text-left">Номер ИП</th>
        <th class="text-left">Дата ИП</th>
        <th class="text-left">Предмет исполнения</th>
        <th class="text-left">Общая сумма</th>
        <th class="text-left">Отдел ФССП</th>
    </tr>
    <?php foreach ($models as $model) : ?>
        <tr>
            <td class="text-left"><?= $model->name; ?></td>
            <td class="text-left"><?= $model->address; ?></td>
            <td class="text-left"><?= $model->code_ip; ?></td>
            <td class="text-left"><?= date('d.m.Y', strtotime($model->date_ip)); ?></td>
            <td class="text-left"><?= $model->subject; ?></td>
            <td class="text-left"><?= $model->sum; ?></td>
            <td class="text-left"><?= $model->fssp_department; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
