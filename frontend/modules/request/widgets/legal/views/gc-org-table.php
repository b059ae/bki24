<?php
/**
 * @var \common\models\GcOrg[] $models
 */
?>

<table class="table table-bordered">
    <tbody>
    <tr>
        <th class="text-left">Наименование заказчика</th>
        <th class="text-left">ИНН заказчика</th>
        <th class="text-left">КПП заказчика</th>
        <th class="text-left">Дата заключения контракта</th>
        <th class="text-left">Сумма</th>
    </tr>
    <?php foreach ($models as $model) : ?>
        <tr>
            <td class="text-left"><?= $model->customer_name; ?></td>
            <td class="text-left"><?= $model->customer_inn; ?></td>
            <td class="text-left"><?= $model->customer_kpp; ?></td>
            <td class="text-left"><?= date('d.m.Y', strtotime($model->date)); ?></td>
            <td class="text-left"><?= $model->sum; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
