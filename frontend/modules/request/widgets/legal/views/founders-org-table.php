<?php
/**
 * @var \common\models\FoundersFlOrg[] $models
 */
use common\models\IrbisPeople;
use yii\helpers\Url;

?>

<table class="table table-bordered">
    <tbody>
    <tr>
        <th class="text-left">ФИО</th>
        <th class="text-left">ИНН</th>
        <th class="text-left">Дата вложения</th>
        <th class="text-left">Доля</th>
        <th class="text-left">Подробнее</th>
    </tr>
    <?php foreach ($models as $model) : ?>
        <?php $irbisPeople = IrbisPeople::findOne(['founders_id' => $model->id]); ?>
        <tr>
            <td class="text-left"><?= $model->fio; ?></td>
            <td class="text-left"><?= $model->inn; ?></td>
            <td class="text-left"><?= date('d.m.Y', strtotime($model->grn_date)); ?></td>
            <td class="text-left"><?= round($model->share_capital * 100, 2); ?>%</td>
            <?php if ($irbisPeople):?>
                <td class="text-left"><a target="_blank" href="<?=Url::to(['/request/default/view']) . '?id=' . $irbisPeople->irbis_request_id; ?>">Открыть</a></td>
            <?php endif;?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
