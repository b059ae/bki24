<?php
/**
 * @var int $count
 * @var float $sum
 * @var float $avg
 */
?>

<?php if ($count > 0):?>
<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="text-right" style="width: 50%;">Количество</th>
            <td class="text-left"><?= $count; ?></td>
        </tr>
        <tr>
            <th class="text-right">Сумма</th>
            <td class="text-left"><?= number_format($sum, 2, '.', ' '); ?></td>
        </tr>
        <tr>
            <th class="text-right">Среднее</th>
            <td class="text-left"><?= number_format($avg, 2, '.', ' '); ?></td>
        </tr>
    </tbody>
</table>
<?php else:?>
    Не участвовал в исполнительных производствах
<?php endif;?>