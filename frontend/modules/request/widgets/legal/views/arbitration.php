<?php
/**
 * @var int $chart
 * @var int $legend
 * @var int $last
 * @var int $all
 */
use common\models\ArbitrSum;
use frontend\modules\request\widgets\assets\SparklinesAsset;

SparklinesAsset::register($this);

?>

<!--<div id="arbitr-sum"></div>-->

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Период</th>
            <th>Всего</th>
            <th>Ответчик</th>
            <th>Истец</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $last['period']; ?></td>
            <td class="text-center"><?= number_format($last['sum'], 2, '.', ' '); ?> <span>(<?= $last['count']; ?>)</span></td>
            <td class="text-center"><?= number_format($last[ArbitrSum::TYPE_PLAINTIFF]['sum'], 2, '.', ' '); ?> <span>(<?= $last[ArbitrSum::TYPE_PLAINTIFF]['count']; ?>)</span></td>
            <td class="text-center"><?= number_format($last[ArbitrSum::TYPE_DEFENDANT]['sum'], 2, '.', ' '); ?> <span>(<?= $last[ArbitrSum::TYPE_DEFENDANT]['count']; ?>)</span></td>
        </tr>
        <tr>
            <td>За все время</td>
            <td class="text-center"><?= number_format($last['sum'], 2, '.', ' '); ?> <span>(<?= $all['count']; ?>)</span></td>
            <td class="text-center"><?= number_format($all[ArbitrSum::TYPE_PLAINTIFF]['sum'], 2, '.', ' '); ?> <span>(<?= $all[ArbitrSum::TYPE_PLAINTIFF]['count']; ?>)</span></td>
            <td class="text-center"><?= number_format($all[ArbitrSum::TYPE_DEFENDANT]['sum'], 2, '.', ' '); ?> <span>(<?= $all[ArbitrSum::TYPE_DEFENDANT]['count']; ?>)</span></td>
        </tr>
    </tbody>
</table>

<?php
$js = <<<JS
$("#arbitr-sum").each(function () {
    var element = $(this);
    element.sparkline([$chart], {
        type: "line",
        spotRadius: "3",
        highlightSpotColor: "#f39c12",
        highlightLineColor: "#222",
        maxSpotColor: "#f56954",
        minSpotColor: "#00a65a",
        spotColor: "#39CCCC",
        offset: "90",
        width: "100%",
        height: "100px",
        lineWidth: "2",
        lineColor: "#39CCCC",
        fillColor: "rgba(57, 204, 204, 0.08)",
        tooltipFormat: '{{offset:offset}}',
        tooltipValueLookups: {
            'offset': $legend
        }
    });
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>
