<?php
/**
 * @var $this yii\web\View
 * @var array $label
 * @var array $profit
 * @var array $sales
 * @var array $spend
 * @var integer $size
 * @var integer $steps
 */
use frontend\modules\request\widgets\assets\ChartAsset;

?>

<!-- BAR CHART -->
<div class="chart">
    <canvas id="barChart" style="height:400px"></canvas>
    <div id="barLegend" class="text-center"></div>
</div>

<?php
$label = json_encode($label);
$sales = json_encode($sales);
$spend = json_encode($spend);
$profit = json_encode($profit);
$js = <<<JS

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
}

var barChartData = {
    labels  : $label,
    datasets: [
        {
            label               : 'Выручка',
            fillColor           : '#d2d6de',
            strokeColor         : '#d2d6de',
            pointColor          : '#d2d6de',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: '#dcdcdc',
            data                : $sales
        },
        {
            label               : 'Затраты',
            fillColor           : '#ea0003',
            strokeColor         : '#ea0003',
            pointColor          : '#ea0003',
            pointStrokeColor    : '#ea0003',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: '#ea0003',
            data                : $spend
        },
        {
            label               : 'Чистая прибыль',
            fillColor           : '#00a65a',
            strokeColor         : '#00a65a',
            pointColor          : '#00a65a',
            pointStrokeColor    : '#00a65a',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: '#00a65a',
            data                : $profit
        }
    ]
};
var barChartCanvas = $('#barChart').get(0).getContext('2d');
var barChart = new Chart(barChartCanvas);
var barChartOptions = {
    scaleBeginAtZero        : true,
    scaleShowGridLines      : true,
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    scaleGridLineWidth      : 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines  : true,
    scaleOverride           : true,
    scaleSteps              : $steps,
    scaleStepWidth          : $size,
    scaleStartValue         : 0, 
    barShowStroke           : true,
    barStrokeWidth          : 2,
    barValueSpacing         : 5,
    barDatasetSpacing       : 1,
    multiTooltipTemplate    : '<%= addCommas(value) %>',
    legendTemplate          : '<ul class="chart-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
    responsive              : true,
    maintainAspectRatio     : true
};

barChartOptions.datasetFill = false;
var temp = barChart.Bar(barChartData, barChartOptions);
$('#barLegend').html(temp.generateLegend());
JS;
ChartAsset::register($this);
$this->registerJs($js, \yii\web\View::POS_END);
?>
