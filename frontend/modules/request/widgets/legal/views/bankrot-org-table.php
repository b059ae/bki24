<?php
/**
 * @var \common\models\BankrotOrg[] $inn
 * @var \common\models\BankrotOrg[] $name
 */
?>

<table class="table table-bordered">
    <thead>
    <tr>
        <?php /*<th class="text-center">Номер дела</th> */ ?>
        <th class="text-left">Банкрот</th>
        <th class="text-left">Адрес</th>
        <th class="text-left">Судебный участок</th>
        <th class="text-left">Дата</th>
        <?php /*<th class="text-center">Подробнее</th> */ ?>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($inn)) : ?>
        <tr>
            <th colspan="7" class="text-center">Совпадения по ИНН</th>
        </tr>
        <?php foreach ($inn as $model) : ?>
            <tr>
                <td class="text-left"><?= $model->bankrot; ?></td>
                <td class="text-left"><?= $model->address; ?></td>
                <td class="text-left"><?= $model->judge; ?></td>
                <td class="text-left"><?= date('d.m.Y', strtotime($model->date_bankrot)); ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (!empty($name)) : ?>
        <tr>
            <th colspan="7" class="text-center">Совпадения по Имени</th>
        </tr>
        <?php foreach ($name as $model) : ?>
            <tr>
                <td class="text-left"><?= $model->bankrot; ?></td>
                <td class="text-left"><?= $model->address; ?></td>
                <td class="text-left"><?= $model->judge; ?></td>
                <td class="text-left"><?= date('d.m.Y', strtotime($model->date_bankrot)); ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
