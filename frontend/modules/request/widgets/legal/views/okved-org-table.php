<?php
/**
 * @var OkvedOrg[] $models
 */
use common\models\OkvedOrg;

?>

<table class="table table-bordered">
    <tbody>
    <?php foreach ($models as $model) :
        if ($model->type == OkvedOrg::BASE) {
            continue;
        } ?>
        <tr>
            <td class="text-left"><?= $model->code; ?></td>
            <td class="text-left"><?= $model->name; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
