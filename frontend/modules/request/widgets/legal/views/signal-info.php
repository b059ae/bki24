<?php
/**
 * @var string $head
 * @var string $info
 * @var string $icon
 * @var string $link
 */
?>

<div class="small-box bg-yellow">
    <div class="inner">
        <h3><?= $head; ?></h3>
        <p><?= $info; ?></p>
    </div>
    <div class="icon">
        <i class="<?= $icon; ?>"></i>
    </div>
    <a href="#<?= $link; ?>" data-toggle="tab" class="small-box-footer open-tab">
        Подробнее <i class="fa fa-arrow-circle-right"></i>
    </a>
</div>
