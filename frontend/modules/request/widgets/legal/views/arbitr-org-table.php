<?php
/**
 * @var \common\models\ArbitrOrg[] $inn
 * @var \common\models\ArbitrOrg[] $name
 */
?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th class="text-left">Номер дела</th>
        <th class="text-left">Участник</th>
        <th class="text-left">Адрес</th>
        <th class="text-left">Тип участника</th>
        <th class="text-left">Судебный участок</th>
        <th class="text-left">Дата</th>
        <th class="text-left">Подробнее</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($inn)) : ?>
        <tr>
            <th colspan="7" class="text-center">Совпадения по ИНН</th>
        </tr>
        <?php foreach ($inn as $model) : ?>
            <tr>
                <td class="text-left"><?= $model->name; ?></td>
                <td class="text-left"><?= $model->member; ?></td>
                <td class="text-left"><?= $model->address; ?></td>
                <td class="text-left"><?= $model->getTypeMember(); ?></td>
                <td class="text-left"><?= $model->judge; ?></td>
                <td class="text-left"><?= date('d.m.Y', strtotime($model->date)); ?></td>
                <td class="text-left"><a target="_blank" href="<?= $model->getFullUrl(); ?>">Открыть дело</a></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (!empty($name)) : ?>
        <tr>
            <th colspan="7" class="text-center">Совпадения по Имени</th>
        </tr>
        <?php foreach ($name as $model) : ?>
            <tr>
                <td class="text-left"><?= $model->name; ?></td>
                <td class="text-left"><?= $model->member; ?></td>
                <td class="text-left"><?= $model->address; ?></td>
                <td class="text-left"><?= $model->getTypeMember(); ?></td>
                <td class="text-left"><?= $model->judge; ?></td>
                <td class="text-left"><?= date('d.m.Y', strtotime($model->date)); ?></td>
                <td class="text-left"><a target="_blank" href="<?= $model->getFullUrl(); ?>">Открыть дело</a></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
