<?php

namespace frontend\modules\request\widgets\legal;

use common\models\BadContract;
use frontend\modules\request\widgets\AbstractCheckWidget;

class BadProviderCountOrgWidget extends AbstractCheckWidget
{
    /**
     * @var BadContract[]
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'tab_bad_provider';
    /**
     * @var string
     */
    public $icon = 'ion ion-clipboard';

    public function run()
    {
        parent::run();
        $count = count($this->model);
        if ($count > 0) {
            $signals[] = 'Недобросовестный поставщик';
        }
        if (empty($signals)) {
            return $this->renderSuccess('Поставщик', 'Добросовестный поставщик');
        } else {
            return $this->renderWarning('Поставщик', $signals);
        }
    }
}
