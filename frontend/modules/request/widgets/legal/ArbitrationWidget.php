<?php

namespace frontend\modules\request\widgets\legal;

use common\models\ArbitrSum;
use yii\base\Widget;

class ArbitrationWidget extends Widget
{
    /**
     * @var ArbitrSum[]
     */
    public $models;

    public $layout = 'arbitration';

    public function run()
    {
        parent::run();
        $all = [
            'count' => 0,
            'sum' => 0,
            ArbitrSum::TYPE_PLAINTIFF => [
                'count' => 0,
                'sum' => 0,
            ],
            ArbitrSum::TYPE_DEFENDANT => [
                'count' => 0,
                'sum' => 0,
            ],
        ];
        $data = [];
        foreach ($this->models as $model) {
            // Прочие типы дела
            if (!isset($all[$model->type])) {
                continue;
            }
            // Воссоздаем суммы по годам
            if (!isset($data[$model->year])) {
                $data[$model->year] = [
                    'period' => $model->year,
                    'count' => 0,
                    'sum' => 0,
                    ArbitrSum::TYPE_PLAINTIFF => [
                        'count' => 0,
                        'sum' => 0,
                    ],
                    ArbitrSum::TYPE_DEFENDANT => [
                        'count' => 0,
                        'sum' => 0,
                    ],
                ];
            }

            // Заполняем всего
            $all['count'] += $model->count;
            $all['sum'] += (float)$model->sum;

            // Заполняем всего по ролям
            $all[$model->type]['count'] += $model->count;
            $all[$model->type]['sum'] += (float)$model->sum;

            // Заполняем всего текущий год
            $data[$model->year]['count'] += $model->count;
            $data[$model->year]['sum'] += (float)$model->sum;

            // Заполняем текущий год
            $data[$model->year][$model->type]['count'] += $model->count;
            $data[$model->year][$model->type]['sum'] += (float)$model->sum;
        }
        krsort($data);

        // Подготовка данных для графика
        $chart = [];
        $legends = [];
        foreach ($data as $cell) {
            $sum = $cell[ArbitrSum::TYPE_PLAINTIFF]['sum'] + $cell[ArbitrSum::TYPE_DEFENDANT]['sum'];
            array_unshift($chart, number_format($sum, 2, '.', ''));
            array_unshift($legends, '"' . $cell['period'] . ': ' . number_format($sum, 2, '.', ' ') . '"');
        }
        $legend = [];
        foreach ($legends as $key => $value) {
            $legend[] = "{$key}: {$value}";
        }

        // Текущий период
        $last = array_shift($data);

        return $this->render($this->layout, [
            'chart' => implode(', ', $chart),
            'legend' => '{' . implode(', ', $legend) . '}',
            'last' => $last,
            'all' => $all,
        ]);
    }
}
