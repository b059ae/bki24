<?php

namespace frontend\modules\request\widgets\legal;

use common\models\FsspOrg;
use common\models\FsspPeople;
use yii\base\Widget;

class FsspWidget extends Widget
{
    /**
     * @var FsspOrg[]|FsspPeople[]
     */
    public $models;

    public $layout = 'fssp';

    public function run()
    {
        parent::run();
        $data = [
            'count' => 0,
            'sum' => 0,
            'avg' => 0,
        ];
        if (!empty($this->models)) {
            foreach ($this->models as $model) {
                $data['count']++;
                $data['sum'] += (float)$model->sum;
            }
            $data['avg'] = round($data['sum'] / $data['count'], 2);
        }

        return $this->render($this->layout, $data);
    }
}
