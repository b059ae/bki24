<?php

namespace frontend\modules\request\widgets\legal;

use common\models\ArbitrOrg;
use yii\base\Widget;

class ArbitrOrgWidget extends Widget
{
    /**
     * @var ArbitrOrg[]
     */
    public $models;
    /**
     * @var string
     */
    public $layout = 'arbitr-org-table';

    public function run()
    {
        parent::run();

        $data = [
            'inn' => [],
            'name' => [],
        ];

        foreach ($this->models as $model) {
            if ($model->type_request == ArbitrOrg::TYPE_REQUEST_INN) {
                $data['inn'][] = $model;
            } else {
                $data['name'][] = $model;
            }
        }

        return $this->render($this->layout, $data);
    }
}
