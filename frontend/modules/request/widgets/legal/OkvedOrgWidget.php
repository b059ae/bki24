<?php

namespace frontend\modules\request\widgets\legal;

use common\models\OkvedOrg;
use yii\base\Widget;

class OkvedOrgWidget extends Widget
{
    /**
     * @var OkvedOrg[]
     */
    public $models;
    /**
     * @var string
     */
    public $layout = 'okved-org-table';

    public function run()
    {
        parent::run();

        return $this->render($this->layout, [
            'models' => $this->models,
        ]);
    }
}
