<?php

namespace frontend\modules\request\widgets\legal;

use common\models\FoundersFlOrg;
use yii\base\Widget;

class FoundersOrgWidget extends Widget
{
    /**
     * @var FoundersFlOrg[]
     */
    public $models;
    /**
     * @var string
     */
    public $layout = 'founders-org-table';

    public function run()
    {
        parent::run();

        return $this->render($this->layout, [
            'models' => $this->models,
        ]);
    }
}
