<?php

namespace frontend\modules\request\widgets\legal;

use common\models\BankrotOrg;
use common\models\IrbisOrg;
use frontend\modules\request\widgets\AbstractCheckWidget;

class SubjectOrgCheckWidget extends AbstractCheckWidget
{
    /**
     * @var IrbisOrg
     */
    public $model;
    /**
     * @var string
     */
    public $link = 'subject';
    /**
     * @var string
     */
    public $icon = 'ion ion-person-stalker';

    public function run()
    {
        parent::run();
        $signals = [];
        if (!empty($this->model->death_date)) {
            $signals[] = 'Организация закрыта ' . date('d.m.Y', strtotime($this->model->death_date));
        }
        $bankruptcy = BankrotOrg::findOne([
            'irbis_request_id' => $this->model->irbis_request_id,
            'type_request' => 2,
        ]);
        if (!empty($bankruptcy)) {
            $link = 'tab_bankrot';
            $signals[] = 'Организация объявлена банкротом!';
        }

        // TODO: Директор/Учредитель террорист
        // TODO: Недействительный паспорт у Директора/Учредителя

        if (empty($signals)) {
            return $this->renderSuccess('Актуально', 'Не обнаружено серьезных проблем');
        } else {
            return $this->renderWarning('Внимание!', $signals);
        }
    }
}
