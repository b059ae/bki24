<?php

namespace frontend\modules\request\widgets\legal;

use common\models\FsspOrg;
use yii\base\Widget;

class FsspOrgWidget extends Widget
{
    /**
     * @var FsspOrg[]
     */
    public $models;
    /**
     * @var string
     */
    public $layout = 'fssp-org-table';

    public function run()
    {
        parent::run();

        return $this->render($this->layout, [
            'models' => $this->models,
        ]);

    }
}
