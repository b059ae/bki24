<?php

namespace frontend\modules\request\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use frontend\modules\request\models\IpForm;
use frontend\modules\request\models\LegalForm;
use common\models\IrbisRequest;
use common\components\irbis\IrbisOrg;
use common\components\irbis\IrbisPeople;

class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Список предыдущих запросов и форма нового запроса
     *
     * @return string
     */
    public function actionIndex()
    {
        $modelLegal = new LegalForm();
        $modelIp = new IpForm();
        $dataProvider = new ActiveDataProvider([
            'query' => IrbisRequest::find(),
            'sort' => [
                'defaultOrder'=> [
                    'id'=>SORT_DESC
                ]
            ]
        ]);

        return $this->render('index', [
            'provider' => $dataProvider,
            'modelLegal' => $modelLegal,
            'modelIp' => $modelIp
        ]);
    }

    public function actionLegal()
    {
        $modelLegal = new LegalForm();
        return $this->render('legal', [
            'modelLegal' => $modelLegal
        ]);
    }

    public function actionIndividual()
    {
        $modelIp = new IpForm();
        return $this->render('individual', [
            'modelIp' => $modelIp
        ]);
    }

    /**
     * Отображение информации о субъекте по запросу
     *
     * @param int $id
     * @return string
     */
    public function actionView($id)
    {
        $request = IrbisRequest::findOne($id);
        /*if ($request->request_type == \common\models\IrbisRequest::IRBIS_REQUEST_PEOPLE) {
            $irbis = new IrbisPeople();
            $irbis->updateFields($id);
        }
        if ($request->request_type == \common\models\IrbisRequest::IRBIS_REQUEST_LEGAL) {
            $irbis = new IrbisOrg();
            $irbis->updateFields($id);
        }*/
        return $this->render('view', [
            'request' => $request,
        ]);
    }

    /**
     * Запрос в Ирбис данных по Юр лицам
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionLegalRequest()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelLegal = new LegalForm();
        $data = [
            'status' => $modelLegal::STATUS_ERROR,
            'errors' => null,
            'url' => '',
        ];
        try {
            $modelLegal->load(Yii::$app->request->post());
            if ($modelLegal->validate()) {
                //Создание запроса
                $model = new \common\models\Request([
                    'inn' => $this->inn,
                    'type' => \common\models\Request::REQUEST_LEGAL,
                ]);

                if (!$model->save()) {
                    throw new \Exception('Не удалось сохранить request');
                }
                $request_id = $model->id;

                $irbis = new IrbisOrg([
                    'field' => [
                        'request_id' => $request_id,
                        'inn' => $modelLegal->inn,
                        'founders' => $modelLegal->founders,
                        'manager' => $modelLegal->manager
                    ]
                ]);
                $result = $irbis->getUuid();
                $id = $irbis->legal($result);
                $data['status'] = $modelLegal::STATUS_SUCCESS;
                $data['url'] = Url::to(['default/view', 'id' => $id]);
            } else {
                $data['errors'] = Html::errorSummary($modelLegal, [
                    'header' => '',
                ]);
            }
        } catch (\Exception $e) {
            $data['errors'] = $e->getMessage();
        }
        return $data;
    }

    /**
     * Запрос в Ирбис по физ лицам
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionIndividualRequest()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelIp = new IpForm();
        $data = [
            'status' => $modelIp::STATUS_ERROR,
            'errors' => null,
            'url' => '',
        ];
        try {
            $modelIp->load(Yii::$app->request->post());
            if ($modelIp->validate()) {
                //Создание запроса
                $model = new \common\models\Request([
                    'first_name' => $this->firstName,
                    'middle_name' => $this->secondName,
                    'last_name' => $this->lastName,
                    'passport_series' => $this->serPassport,
                    'passport_number' => $this->numPassport,
                    'region_id' => $this->regions,
                    'birth_date' => $this->birthDate,
                    'inn' => $this->inn,
                    'type' => \common\models\Request::REQUEST_PEOPLE,
                ]);

                if (!$model->save()) {
                    throw new \Exception('Не удалось сохранить request');
                }
                $request_id = $model->id;

                $irbis = new IrbisPeople([
                    'field' => [
                        'request_id' => $request_id,
                        'firstName' => $modelIp->firstName,
                        'secondName' => $modelIp->secondName,
                        'lastName' => $modelIp->lastName,
                        'serPassport' => $modelIp->serPassport,
                        'numPassport' => $modelIp->numPassport,
                        'regions' => $modelIp->regions,
                        'birthDate' => $modelIp->birthDate,
                        'inn' => $modelIp->inn,
                    ]
                ]);
                $result = $irbis->getUuid();
                $id = $irbis->individual($result);
                $data['status'] = $modelIp::STATUS_SUCCESS;
                $data['url'] = Url::to(['default/view', 'id' => $id]);
            } else {
                $data['errors'] = Html::errorSummary($modelIp, [
                    'header' => '',
                ]);
            }
        } catch (\Exception $e) {
            $data['errors'] = $e->getMessage();
        }
        return $data;
    }
}
