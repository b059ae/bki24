<?php
return [
    'name' => 'cbr',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        // Запросы в Ирбис ir-bis.org
        'irbis' => [
            'class' => 'common\components\irbis\Irbis',
            'token' => 'da4197a87670817b218decf51248144e',
        ],
        // Скоринг НБКИ nbki.ru
        'nbkiSp' => [
            'class' => 'common\components\nbki\NbkiSp',
            'url' => 'http://127.0.0.1:8181/score3ncr',
            'memberCode' => 'M801LL000000',
            'login' => 'M801LL000002',
            'password' => 'dQge37A8',
        ],
        // Очереди
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db', // Компонент подключения к БД или его конфиг
            'tableName' => '{{%queue}}', // Имя таблицы
            'channel' => 'default', // Выбранный для очереди канал
            'mutex' => \yii\mutex\MysqlMutex::class, // Мьютекс для синхронизации запросов
            'deleteReleased' => false, // Не удалять выполненные
            'as log' => \yii\queue\LogBehavior::class
        ],
        //Компонент для работы с шифрованием и подписями файлов по  ГОСТ-алгоритму
        'cryptoPro' => [
            'class' => 'common\components\CryptoPro',
            'pathToOpenSSL' => '/usr/local/openssl-1.0.1f/bin/openssl',
        ],
        'file' => [//Компонент для хранения файлов отчетов
            'class' => 'common\components\File',
            'folder' => '@runtime/nbki',
        ],
    ],
];
