<?php


namespace common\components;


class Formatter extends \yii\i18n\Formatter
{
    public $dateFormat = 'php:d.m.Y';
}