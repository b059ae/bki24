<?php

namespace common\components\nbki\catalogs;

/**
 * Интерфес Справочников в формате синглтона.
 * Инициализация и хранение данных с индексами.
 *
 * @package api\catalogs
 */
trait CatalogTrait
{
    /**
     * @var array Элементы справочника
     */
    protected $catalog;
    /**
     * @var array Индексы для простого поиска
     */
    protected $indexes;
    /**
     * @var static Единая ссылка на объект
     */
    protected static $instant = null;

    /**
     * Единая точка создания объекта
     *
     * @return static
     */
    public static function getInstant()
    {
        if (is_null(static::$instant)) {
            static::$instant = new static();
        }
        return static::$instant;
    }

    /**
     * Закрытый конструктор справочника
     */
    protected function __construct()
    {
        $this->catalog = [];
        $this->indexes = [];
        $this->init();
    }

    /**
     * Наполнение данными Справочника
     *
     * @return mixed
     */
    abstract protected function init();

    /**
     * Добавление элемента в справочник
     *
     * @param $id
     * @param $refId
     * @param $title
     */
    abstract protected function add($id, $refId, $title);

    /**
     * Получение данных справочника по ID
     *
     * @param $id
     * @param null $element
     * @return null
     */
    public function get($id, $element = null)
    {
        if (isset($this->catalog[$id])) {
            return (is_null($element))
                ? $this->catalog[$id]
                : $this->catalog[$id][$element];
        }
        return null;
    }

    /**
     * Полный список элементов из справочника
     *
     * @param string|null $element
     * @return array
     */
    public function getAll($element = null)
    {
        $list = [];
        foreach ($this->catalog as $id => $item) {
            $list[$id] = (is_null($element))
                ? $item
                : $item[$element];
        }
        return $list;
    }

    /**
     * Поиск по индексу
     *
     * @param $index
     * @param $id
     * @param null $element
     * @return null
     */
    public function getByIndex($index, $id, $element = null)
    {
        $get = $this->getIdByIndex($index, $id);
        if (!is_null($get)) {
            return $this->get($get, $element);
        }
        return null;
    }

    /**
     * Получение обработно совместимого ID
     *
     * @param $index
     * @param $id
     * @return null
     */
    public function getIdByIndex($index, $id)
    {
        if (isset($this->indexes[$index])) {
            if (isset($this->indexes[$index][$id])) {
                return $this->indexes[$index][$id];
            }
        }
        return null;
    }

    /**
     * Допустимые типы документов
     *
     * @return array
     */
    public function getAccessedType()
    {
        return array_keys($this->catalog);
    }

    /**
     * Добавление индекса
     *
     * @param $index
     * @param $from
     * @param $to
     */
    protected function addIndex($index, $from, $to) {
        if (!isset($this->indexes[$index])) {
            $this->indexes[$index] = [];
        }
        $this->indexes[$index][$from] = $to;
    }
}
