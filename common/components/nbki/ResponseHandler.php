<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 13:48
 */

namespace common\components\nbki;


use common\components\CryptoProInterface;
use common\components\FileInterface;
use common\models\CreditScore;

abstract class ResponseHandler
{
    /** @var integer ID запроса */
    protected $requestId;

    // Компоненты для работы
    /** @var  CryptoProInterface */
    protected $cryptoPro;
    /** @var  FileInterface */
    protected $file;

    public function handleResponse($response){
        // Извлекает из подписанного файла чистый XML
        $xml = $this->extractResponse($response);

        // Сохраняем результат в таблицу
        $this->save($xml);
    }

    /**
     * Извлекает из подписанного файла чистый XML
     * @param string $response
     * @return bool|string
     * @throws \Exception
     */
    private function extractResponse($response){
        //Сохраняем кредитный отчет в подписанном виде
        $filename = $this->file->save($response, 'sig', false);

        // Снимаем подпись с файла
        $filePath = $filename.'.xml';
        if (!$this->cryptoPro->extract($filename, $filePath)){
            throw new \Exception('Не удалось снять подпись с файла '.$filename);
        }
        return file_get_contents($filePath);
    }

    /**
     * Сохранение скоринга и причин в БД
     * @param $xmlString
     * @throws \Exception
     */
    private function save($xmlString){
        /** @var \SimpleXMLElement $xml */
        $xml = simplexml_load_string($xmlString);

        $creditScore = new CreditScore();
        $creditScore->request_id = $this->requestId;
        if (isset($xml->ficoRisk)) {
            /** @var \SimpleXMLElement $scoring */
            $scoring = $xml->ficoRisk;

            $creditScore->score = (int)$scoring->score;

            $creditScore->reason_1 = (string)$scoring->reasonCode1;
            $creditScore->reason_2 = (string)$scoring->reasonCode2;
            $creditScore->reason_3 = (string)$scoring->reasonCode3;
            $creditScore->reason_4 = (string)$scoring->reasonCode4;
        }

        if (!$creditScore->save()) {
            throw new \Exception("Значения не сохранены в таблицу credit_score\n" . print_r($creditScore->errors, true));
        }
    }
}