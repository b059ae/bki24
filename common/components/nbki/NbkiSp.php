<?php

namespace common\components\nbki;

use yii\base\Component;

/**
 * Компонент интеграции с НБКИ от юр лица (как сервис-провайдер) для получения дешевых цен для скоринга
 *
 * @package common\components\nbki\request
 */
class NbkiSp extends Component
{
    /**
     * @var string Адрес отправки запросов
     */
    public $url;
    /**
     * @var string MemberCode, предоставленный НБКИ
     */
    public $memberCode;
    /**
     * @var string Имя пользователя для авторизации
     */
    public $login;
    /**
     * @var string Пароль для авторизации
     */
    public $password;


    /**
     * Отправляет XML-запрос в НБКИ
     * @param string $url
     * @param string $xml
     * @return string
     * @throws \Exception
     */
    public function send($xml)
    {
        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, $this->url);
        curl_setopt($s, CURLOPT_HTTPHEADER, ['Content-Type: text/xml']);
        curl_setopt($s, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($s, CURLOPT_TIMEOUT, 100);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($s, CURLOPT_POST, true);
        curl_setopt($s, CURLOPT_POSTFIELDS, $xml);
        $res = curl_exec($s);
        if ($res === false) {
            $code = curl_errno($s);
            $error = curl_error($s);
            throw new \Exception('Получен неверный ответ от НБКИ, код: ' . $code . ', сообщение: ' . $error);
//            throw new \Exception($error, $code); Были проблемы с параметрами, закоментировано временно
        }
        curl_close($s);
        return $res;
    }


}
