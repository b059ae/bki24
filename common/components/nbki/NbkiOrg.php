<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 10:29
 */

namespace common\components\nbki;


use common\components\CryptoProInterface;
use common\components\FileInterface;
use common\components\nbki\request\OrgXml;

class NbkiOrg extends ResponseHandler
{
    /** @var string ИНН */
    private $inn;
    /** @var string ОГРН */
    private $ogrn;
    /** @var string Название организации */
    private $name;

    // Компоненты для работы
    /** @var  NbkiSp */
    private $nbki;

    public function __construct(
        $requestId,
        $inn,
        $ogrn,
        $name,
        NbkiSp $nbki,
        CryptoProInterface $cryptoPro,
        FileInterface $file
    ) {
        $this->requestId = $requestId;
        $this->inn = $inn;
        $this->ogrn = $ogrn;
        $this->name = $name;

        $this->nbki = $nbki;
        $this->cryptoPro = $cryptoPro;
        $this->file = $file;
    }

    /**
     * Запрос скоринга в НБКИ и обработка ответа
     */
    public function scoring(){
        $xml = new OrgXml(
            $this->nbki->memberCode,
            $this->nbki->login,
            $this->nbki->password,
            $this->inn,
            $this->ogrn,
            $this->name
        );
        $response = $this->nbki->send($xml->build());
        $this->handleResponse($response);
    }
}