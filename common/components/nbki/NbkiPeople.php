<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 10:29
 */

namespace common\components\nbki;


use common\components\CryptoProInterface;
use common\components\FileInterface;
use common\components\nbki\request\PeopleXml;

class NbkiPeople extends ResponseHandler
{
    /** @var string Имя */
    private $firstName;
    /** @var string Отчество */
    private $middleName;
    /** @var string Фамилия */
    private $lastName;
    /** @var string Дата рождения */
    private $birthDate;
    /** @var string Серия паспорта */
    private $passportSeries;
    /** @var string Номер паспорта */
    private $passportNumber;

    // Компоненты для работы
    /** @var  NbkiSp */
    private $nbki;

    public function __construct(
        $requestId,
        $firstName,
        $middleName,
        $lastName,
        $birthDate,
        $passportSeries,
        $passportNumber,
        NbkiSp $nbki,
        CryptoProInterface $cryptoPro,
        FileInterface $file
    ) {
        $this->requestId = $requestId;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
        $this->birthDate = $birthDate;
        $this->passportSeries = $passportSeries;
        $this->passportNumber = $passportNumber;

        $this->nbki = $nbki;
        $this->cryptoPro = $cryptoPro;
        $this->file = $file;
    }

    /**
     * Запрос скоринга в НБКИ и обработка ответа
     */
    public function scoring(){
        $xml = new PeopleXml(
            $this->nbki->memberCode,
            $this->nbki->login,
            $this->nbki->password,
            $this->firstName,
            $this->middleName,
            $this->lastName,
            $this->birthDate,
            $this->passportSeries,
            $this->passportNumber
        );
        $response = $this->nbki->send($xml->build());
        $this->handleResponse($response);
    }
}