<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 9:01
 */

namespace common\components\nbki\request;


interface XmlInterface
{
    /**
     * Формирование XML для запроса
     * @return string
     */
    public function build();
}