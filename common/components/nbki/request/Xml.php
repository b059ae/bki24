<?php
/**
 * Генерирует XML для отправки в НБКИ с целью получения КИ или скоринга субъекта от юрлица (сервис-провайдер)
 */
namespace common\components\nbki\request;

abstract class Xml
{
    /**
     * @var string MemberCode, предоставленный НБКИ
     */
    protected $memberCode;
    /**
     * @var string Логин пользователя, предоставленный НБКИ
     */
    protected $login;
    /**
     * @var string Пароль пользователя, предоставленный НБКИ
     */
    protected $password;
    /** @var string Тип отчета. Необходимо переопределить */
    protected $product;

    public function __construct(
        $memberCode,
        $login,
        $password
    )
    {
        $this->memberCode = $memberCode;
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * Генерирует секцию с типом КИ
     * @param \SimpleXMLElement $req
     * @return \SimpleXMLElement
     */
    protected function refReq(\SimpleXMLElement $req)
    {
        $refReq = $req->addChild('RefReq');
        $refReq->addChild('product', $this->product);

        return $req;
    }

    /**
     * Генерирует секцию с данными о получателе
     * @param \SimpleXMLElement $req
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    protected function requestorReq(\SimpleXMLElement $req)
    {
        $requestorReq = $req->addChild('RequestorReq');
        $requestorReq->addChild('MemberCode', $this->memberCode);
        $requestorReq->addChild('UserID', $this->login);
        $requestorReq->addChild('Password', $this->password);

        return $req;
    }

    /** Генерирует секцию с адресом
     * @param \SimpleXMLElement $req
     * @param array $data [
     *                      "zip" => "344065",
     *                      "city" => "Ростов-на-Дону",
     *                      "street" => "Малиновского",
     *                      "house" => "3д",
     *                      "apartments" => ""
     *                    ]
     * @param integer $type
     * @return \SimpleXMLElement
     */
    protected function addressReq(\SimpleXMLElement $req, $data, $type)
    {
        $addressReq = $req->addChild('AddressReq');
        $addressReq->addChild('addressType', $type);
        $addressReq->addChild('postal', $data['postal']);
        $addressReq->addChild('city', $data['city']);
        $addressReq->addChild('street', $data['street']);
        $addressReq->addChild('houseNumber', $data['house']);
        $addressReq->addChild('apartment', $data['apartments']);

        return $req;
    }



    protected static function idReqBusiness(\SimpleXMLElement $req, $data)
    {
        //ОГРН
        $idReq = $req->addChild('IdReq');
        $idReq->addChild('idType', 34);
        $idReq->addChild('idNum', $data['egrul']);

        //ИНН
        $idReq = $req->addChild('IdReq');
        $idReq->addChild('idType', 81);
        $idReq->addChild('idNum', $data['inn']);

        return $req;
    }

    /**
     * Генерирует секцию с целью запроса и согласием
     * @param \SimpleXMLElement $req
     * @return \SimpleXMLElement
     * @internal param array $data
     */
    protected function inquiryReq(\SimpleXMLElement $req)
    {
        $inquiryReq = $req->addChild('InquiryReq');
        $consentReq = $inquiryReq->addChild('ConsentReq');
        $consentReq->addChild('consentFlag', 'Y');
        $consentReq->addChild('consentDate', \Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd'));
        $consentReq->addChild('consentExpireDate', \Yii::$app->formatter->asDate(strtotime("+2 months", time()), 'yyyy-MM-dd'));
        $consentReq->addChild('consentPurpose', 2);//Проверка благонадежности
        $consentReq->addChild('otherConsentPurpose', '');
        $consentReq->addChild('reportUser', 'Общество с ограниченной ответственностью «МИКФИНАНС ПЛЮС»');
        $consentReq->addChild('liability', 'Y');
        $inquiryReq->addChild('inqPurpose', 50);//Проверка счета
        $inquiryReq->addChild('inqAmount', 0);//Сумма займа
        $inquiryReq->addChild('currencyCode', 'RUB');//Валюта займа

        return $req;
    }


}