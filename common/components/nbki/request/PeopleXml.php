<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 8:54
 */

namespace common\components\nbki\request;


class PeopleXml extends Xml implements XmlInterface
{
    /** @var string Имя */
    private $firstName;
    /** @var string Отчество */
    private $middleName;
    /** @var string Фамилия */
    private $lastName;
    /** @var string Дата рождения */
    private $birthDate;
    /** @var string Серия паспорта */
    private $passportSeries;
    /** @var string Номер паспорта */
    private $passportNumber;

    CONST ISSUECOUNTRY = 'Россия'; // Страна выдачи документа
    CONST ISSUEAUTHORITY = 'Россия'; // Кем выдан документ
    CONST PLACEOFBIRTH = 'Россия'; // Место рождения
    CONST ADDRESS = 'Россия'; // Адрес
    CONST REGADDRESS = 1; // ID адреса регистрации
    CONST REALADDRESS = 2; // ID адреса проживания
    protected $product = 'CHST'; // Тип отчета - скоринг физлица
    /** @var string Дата выдачи */
    private $issueDate;

    public function __construct(
        $memberCode,
        $login,
        $password,
        $firstName,
        $middleName,
        $lastName,
        $birthDate,
        $passportSeries,
        $passportNumber
    ) {
        parent::__construct($memberCode, $login, $password);
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
        $this->birthDate = $birthDate;
        $this->passportSeries = $passportSeries;
        $this->passportNumber = $passportNumber;
        $this->issueDate = \Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd');// Ставим текущую дату
    }

    /**
     * @return string
     */
    public function build()
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="Windows-1251" ?><product/>');

        //req section
        $prequest = $xml->addChild('prequest');
        $req = $prequest->addChild('req');
        $req->addChild('IOType', 'B2B');
        $req->addChild('OutputFormat', 'XML');
        $req->addChild('lang', 'ru');

        //IdReq section. Документ физ. лица
        $req = $this->idReqPerson($req);
        //PersonReq section. Личные данные
        $req = $this->personReq($req);

        //Address section. Прописка
        $req = $this->addressReq($req, [
            "postal" => "",
            "city" => self::ADDRESS,
            "street" => self::ADDRESS,
            "house" => "",
            "apartments" => ""
        ], self::REGADDRESS);
        //Address section. Проживание
        $req = $this->addressReq($req, [
            "postal" => "",
            "city" => self::ADDRESS,
            "street" => self::ADDRESS,
            "house" => "",
            "apartments" => ""
        ], self::REALADDRESS);
        //RefReq. Тип КО
        $req = $this->refReq($req);

        //InquiryReq. Сведения о согласии и цели запроса
        $req = $this->inquiryReq($req);

        //RequestorReq. Сведения о получателе КО
        $req = $this->requestorReq($req);

        return $xml->asXML();
    }

    /**
     * Генерирует секцию с документом
     * @param \SimpleXMLElement $req
     * @return \SimpleXMLElement
     */
    protected function idReqPerson(\SimpleXMLElement $req)
    {
        $idReq = $req->addChild('IdReq');
        $idReq->addChild('idType', 21);//Паспорт РФ

        $idReq->addChild('seriesNumber', $this->passportSeries);
        $idReq->addChild('idNum', $this->passportNumber);
        $idReq->addChild('issueCountry', self::ISSUECOUNTRY);
        $idReq->addChild('issueDate', $this->issueDate);
        $idReq->addChild('issueAuthority', self::ISSUEAUTHORITY);

        return $req;
    }

    /**
     * Генерирует секцию с личными данными субъекта
     * @param \SimpleXMLElement $req
     * @return \SimpleXMLElement
     */
    protected function personReq(\SimpleXMLElement $req)
    {
        $personReq = $req->addChild('PersonReq');
        $personReq->addChild('name1', $this->lastName);
        $personReq->addChild('first', $this->firstName);
        $personReq->addChild('paternal', $this->middleName);
        $personReq->addChild('birthDt', \Yii::$app->formatter->asDate($this->birthDate, 'yyyy-MM-dd'));
        $personReq->addChild('placeOfBirth', self::PLACEOFBIRTH);

        return $req;
    }


}