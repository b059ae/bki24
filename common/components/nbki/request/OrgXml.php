<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 8:54
 */

namespace common\components\nbki\request;


class OrgXml extends Xml implements XmlInterface
{
    /** @var string ИНН */
    private $inn;
    /** @var string ОГРН */
    private $ogrn;
    /** @var string Название организации */
    private $name;

    CONST ADDRESS = 'Россия'; // Адрес
    CONST REGADDRESS = 3; // ID юридического адреса
    CONST REALADDRESS = 4; // ID фактического адреса
    protected $product = 'BHST'; // Тип отчета - скоринг юрлица

    public function __construct(
        $memberCode,
        $login,
        $password,
        $inn,
        $ogrn,
        $name
    ) {
        parent::__construct($memberCode, $login, $password);
        $this->inn = $inn;
        $this->ogrn = $ogrn;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function build()
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="Windows-1251" ?><product/>');

        //req section
        $prequest = $xml->addChild('prequest');
        $req = $prequest->addChild('req');
        $req->addChild('IOType', 'B2B');
        $req->addChild('OutputFormat', 'XML');
        $req->addChild('lang', 'ru');

        //IdReq section. Документ юр. лица
        $req = $this->idReqPerson($req);
        //businessReq Название юрлица
        $req = $this->businessReq($req);

        //Address section. Прописка
        $req = $this->addressReq($req, [
            "postal" => "",
            "city" => self::ADDRESS,
            "street" => self::ADDRESS,
            "house" => "",
            "apartments" => ""
        ], self::REGADDRESS);
        //Address section. Проживание
        $req = $this->addressReq($req, [
            "postal" => "",
            "city" => self::ADDRESS,
            "street" => self::ADDRESS,
            "house" => "",
            "apartments" => ""
        ], self::REALADDRESS);
        //RefReq. Тип КО
        $req = $this->refReq($req);

        //InquiryReq. Сведения о согласии и цели запроса
        $req = $this->inquiryReq($req);

        //RequestorReq. Сведения о получателе КО
        $req = $this->requestorReq($req);

        return $xml->asXML();
    }

    /**
     * Генерирует секцию с документом
     * @param \SimpleXMLElement $req
     * @return \SimpleXMLElement
     */
    protected function idReqPerson(\SimpleXMLElement $req)
    {
        //ОГРН
        $idReq = $req->addChild('IdReq');
        $idReq->addChild('idType', 34);
        $idReq->addChild('idNum', $this->ogrn);

        //ИНН
        $idReq = $req->addChild('IdReq');
        $idReq->addChild('idType', 81);
        $idReq->addChild('idNum', $this->inn);

        return $req;
    }

    /**
     * Генерирует секцию с данными юр. лица
     * @param \SimpleXMLElement $req
     * @return \SimpleXMLElement
     */
    protected function businessReq(\SimpleXMLElement $req)
    {
        $personReq = $req->addChild('BusinessReq');
        $personReq->addChild('businessName', $this->name);

        return $req;
    }
}