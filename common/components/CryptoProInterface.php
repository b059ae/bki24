<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 12:57
 */

namespace common\components;


interface CryptoProInterface
{
    /**
     * Выполнение openssl комманды для снятия подписи файла
     *
     * @param string $signPath Путь к подписанному файлу
     * @param string $filePath Путь к исходному файлу
     * @return bool
     */
    public function extract($signPath, $filePath);
}