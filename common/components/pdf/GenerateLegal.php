<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.07.17
 * Time: 12:54
 */

namespace common\components\pdf;


use common\components\irbis\IrbisOrg;
use common\models\ArbitrOrg;
use common\models\ArrestOrg;
use common\models\BadContract;
use common\models\BalanceOrg;
use common\models\BankrotOrg;
use common\models\FoundersFlOrg;
use common\models\FoundersLegalOrg;
use common\models\FsspOrg;
use common\models\GcOrg;
use common\models\IrbisRequest;
use common\models\JudgeOrg;
use common\models\ManagerFlOrg;
use common\models\OkvedOrg;
use Yii;

class GenerateLegal
{
    /**
     * @var GeneratePdf
     */
    public $pdf;
    public $height = 42;


    public function generate($id)
    {
        $irRequest = IrbisRequest::findOne($id);
        $this->pdf = new GeneratePdf($irRequest->irbisOrgs, null);
        $this->pdf->SetFont('aefurat', 'BI', 20);
        $this->pdf->SetFont($this->pdf->fontRegular, '', 20, "", false);
        $this->pdf->SetTitle($this->pdf->title);
        $this->pdf->Header();
        $this->pdf->Footer();
        $this->pdf->SetMargins(10, 30, -1, true);
        $this->generatePages($irRequest);
        $this->pdf->Output('legal.pdf', 'I');
    }

    /**
     * Генерация страниц
     * @param $irRequest IrbisRequest
     */
    public function generatePages($irRequest){
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $this->generateFirstPage($irRequest);
        $this->pdf->AddPage();
        $this->tablePages($irRequest);
    }

    /**
     * @param $irRequest IrbisRequest
     */
    protected function generateFirstPage($irRequest)
    {

    }
    /**
     * Генерация страниц с таблицами
     * @param $irRequest IrbisRequest
     */
    public function tablePages($irRequest){
        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetCellPadding(0);
        $this->pdf->SetTextColor(74, 126, 230);
        $this->pdf->Cell(80, 7, 'Подробная информация', 0, 0, 'L', 0);
        $this->pdf->SetTextColor(0,0,0);
        $this->pdf->Ln();
        $this->generateTableFssp($irRequest->fsspOrgs);
        $this->generateTableArbitr($irRequest->arbitrOrgs);
        $this->generateTableManager($irRequest->managerFlOrgs);
        $this->generateTableFoundersFl($irRequest->foundersFlOrgs);
        $this->generateTableFoundersCompany($irRequest->foundersLegalOrgs);
        $this->generateTableBalance($irRequest->balanceOrgs);
        $this->generateTableArrest($irRequest->arrestOrgs);
        $this->generateTableBadContract($irRequest->badContracts);
        $this->generateTableGc($irRequest->gcOrgs);
        $this->generateTableJudge($irRequest->judgeOrgs);
        $this->generateTableOkved($irRequest->okvedOrgs);
        $this->generateTableBankrot($irRequest->bankrotOrgs, $irRequest);
    }


    /**
     * Применение стилей к шапке таблице
     * @param $name
     */
    public function setStyleHeadTable($name){
        if ($this->pdf->GetY() > 240){
            $this->pdf->AddPage();
        }
        $this->pdf->SetLeftMargin(20);
        $this->pdf->SetFontSize(14);
        $this->pdf->Cell(80, 7, $name, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFontSize(10);
        $this->pdf->SetCellPadding(0);
        $this->pdf->SetFillColor(224, 235, 255);
        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->SetDrawColor(225, 225, 225);
        $this->pdf->SetLineWidth(0.3);
    }
    /**
     * Применение стилей к шапке таблице
     */
    public function setStyleBodyTable(){
        $this->pdf->Ln();
        $this->pdf->SetFont('');
        $this->pdf->SetFontSize(8);
        $this->pdf->SetFillColor(235, 235, 235);
        $this->pdf->SetTextColor(0);
    }

    /**
     * Создание шапки талиц
     * @param $header
     * @param $w
     */
    public function setHeaderTable($header, $w){
        $num_headers = count($header);
        for ($i = 0; $i < $num_headers; ++$i) {
            $this->pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'L', 1);
        }
    }

    /**
     * @param $data FsspOrg[]
     */
    public function generateTableFssp($data){
        if ($data) {
            $this->setStyleHeadTable("ФССП");
            $header = ['Код', 'Предмет исполнения', 'Сумма', 'Дата'];
            $w = [30, 100, 20, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->code_ip, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->subject, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->sum, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->date_ip ? Yii::$app->formatter->asDate($item->date_ip, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data ArbitrOrg[]
     */
    public function generateTableArbitr($data){
        if ($data) {
            $this->setStyleHeadTable("Арбитражные суды");
            $header = ['Код', 'Участник', 'Тип участника', 'Дата'];
            $w = [30, 90, 30, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->name, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->member, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->getTypeMember(), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->date ? Yii::$app->formatter->asDate($item->date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data BankrotOrg[]
     * @param $irRequest IrbisRequest
     */
    public function generateTableBankrot($data, $irRequest){
        if ($data && $irRequest->checkTypeBankrot()) {
            $this->setStyleHeadTable("Банкротство");
            $header = ['Код', 'Банкрот', 'Суда', 'Дата'];
            $w = [30, 70, 50, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                if ($item->type_request == BankrotOrg::TYPE_REQUEST_INN) {
                    $this->pdf->Cell($w[0], 6, $item->case_number, 1, 0, 'L', $fill);
                    $this->pdf->Cell($w[1], 6, $item->bankrot, 1, 0, 'L', $fill);
                    $this->pdf->Cell($w[2], 6, $item->judge, 1, 0, 'L', $fill);
                    $this->pdf->Cell($w[3], 6, $item->date_bankrot ? Yii::$app->formatter->asDate($item->date_bankrot, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                    $this->pdf->Ln();
                    $fill = !$fill;
                }
            }
            $this->pdf->Ln();
        }
    }


    /**
     * @param $data ManagerFlOrg[]
     */
    public function generateTableManager($data){
        if ($data) {
            $this->setStyleHeadTable('Руководители');
            $header = ['ФИО', 'ИНН', 'Должность', 'Телефон', 'Дата'];
            // Header
            $w = [70, 20, 35, 25, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->fio, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->inn, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, mb_convert_case($item->role_name, MB_CASE_TITLE, "UTF-8"), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->phones, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->grn_date ? Yii::$app->formatter->asDate($item->grn_date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data FoundersFlOrg[]
     */
    public function generateTableFoundersFl($data){
        if ($data) {
            $this->setStyleHeadTable('Учредители (Люди)');
            $header = ['ФИО', 'ИНН', 'Дата', 'Доля'];
            // Header
            $w = [100, 25, 25, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->fio, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->inn, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->grn_date ? Yii::$app->formatter->asDate($item->grn_date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, round($item->share_capital, 2) * 100 . '%', 1, 0, 'R', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data FoundersLegalOrg[]
     */
    public function generateTableFoundersCompany($data){
        if ($data) {
            $this->setStyleHeadTable('Учредители (Компании)');
            $header = ['Наименование', 'ИНН', 'ОГРН', 'Дата', 'Доля'];
            // Header
            $w = [90, 20, 20, 20, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->name, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->inn, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->ogrn, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->grn_date ? Yii::$app->formatter->asDate($item->grn_date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, round($item->share_capital, 2) * 100 . '%', 1, 0, 'R', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data BalanceOrg[]
     */
    public function generateTableBalance($data){
        if ($data) {
            $this->setStyleHeadTable("Баланс");
            $header = ['Год', 'Баланс', 'Выручка', 'Чистая прибыль'];
            $w = [20, 30, 30, 30];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->year, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->balance, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->sales, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->net_profit, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data ArrestOrg[]
     */
    public function generateTableArrest($data){
        if ($data) {
            $this->setStyleHeadTable("Аресты");
            $header = ['Номер', 'БИК', 'Дата решения', 'Время размещения', 'Банк', 'Код'];
            $w = [15, 20, 25, 35, 65, 10];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->number, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->bik, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->res_date ? Yii::$app->formatter->asDate($item->res_date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->reg_date ? Yii::$app->formatter->asDate($item->reg_date, 'php:d.m.Y H:m:s') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, mb_convert_case($item->bank, MB_CASE_LOWER, "UTF-8"), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[5], 6, $item->nalog_code, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data BadContract[]
     */
    public function generateTableBadContract($data){
        if ($data) {
            $this->setStyleHeadTable('Список недобросовестных поставщиков');
            $header = ['Код', 'Внесен', 'Исключен', 'Дата контракта', 'Сумма', 'Причина'];
            // Header
            $w = [30, 20, 20, 30, 20, 50];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->registry_number, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->date_start ? Yii::$app->formatter->asDate($item->date_start, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->date_end ? Yii::$app->formatter->asDate($item->date_end, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->date_contract ? Yii::$app->formatter->asDate($item->date_contract, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->sum, 1, 0, 'R', $fill);
                $this->pdf->Cell($w[5], 6, $item->reason, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data GcOrg[]
     */
    public function generateTableGc($data){
        if ($data) {
            $this->setStyleHeadTable('Государственные контракты');
            $header = ['Заказчик', 'ИНН', 'КПП', 'Дата', 'Сумма'];
            // Header
            $w = [90, 20, 20, 20, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, mb_convert_case(mb_strlen($item->customer_name, 'UTF-8') > 60 ? mb_substr($item->customer_name, 0, 60, 'UTF-8') . '...' : $item->customer_name, MB_CASE_LOWER, "UTF-8"), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->customer_inn, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->customer_kpp, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->date ? Yii::$app->formatter->asDate($item->date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->sum, 1, 0, 'R', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data JudgeOrg[]
     */
    public function generateTableJudge($data){
        if ($data) {
            $this->setStyleHeadTable('Суды');
            $header = ['Тип дела', 'Количество'];
            // Header
            $w = [90, 30];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->type, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->count, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data OkvedOrg[]
     */
    public function generateTableOkved($data){
        if ($data) {
            $this->setStyleHeadTable('Деятельность');
            $header = ['Код', 'Наименование'];
            // Header
            $w = [10, 160];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->code, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, mb_strlen($item->name, 'UTF-8') > 110 ? mb_substr($item->name, 0, 110, 'UTF-8') . '...' : $item->name, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }
}