<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.07.17
 * Time: 12:54
 */

namespace common\components\pdf;


use common\models\ArbitrPeople;
use common\models\BadContract;
use common\models\BankrotPeople;
use common\models\FsspPeople;
use common\models\GcPeople;
use common\models\IrbisPeople;
use common\models\IrbisRequest;
use common\models\JudgePeople;
use common\models\OrganisationPeople;
use common\models\PledgePeople;
use common\models\TerroristPeople;
use Yii;

class GenerateIp
{
    const IMAGE_GOOD = 1;
    const IMAGE_BAD = 2;
    /**
     * @var GeneratePdf
     */
    public $pdf;
    public $height = 42;
    public $good;
    public $bad;
    public $needImg = false;
    /**
     * @var
     */
    public $check = 'Работать можно';
    public $fill = 0;


    /**
     * @param $id integer irbis_request_id
     */
    public function generate($id)
    {
        $irRequest = IrbisRequest::findOne($id);
        $this->pdf = new GeneratePdf($irRequest->irbisPeoples, null);
        $this->good = dirname(__FILE__) . '/image/yes.png';
        $this->bad = dirname(__FILE__) . '/image/no.png';
        $this->pdf->SetFont($this->pdf->fontRegular, '', 20, "", false);
        $this->pdf->SetTitle($this->pdf->title);
        $this->pdf->Header();
        $this->pdf->Footer();
        $this->generatePages($irRequest);
        $this->pdf->Output('individual.pdf', 'I');
    }

    /**
     * Генерация страниц
     * @param $irRequest IrbisRequest
     */
    public function generatePages($irRequest)
    {
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $this->generateFirstPage($irRequest);
        $this->pdf->AddPage();
        $this->tablePages($irRequest);
    }

    /**
     * Генерация страниц с таблицами
     * @param $irRequest IrbisRequest
     */
    public function tablePages($irRequest){
        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetCellPadding(0);
        $this->pdf->SetTextColor(74, 126, 230);
        $this->pdf->Cell(80, 7, 'Подробная информация', 0, 0, 'L', 0);
        $this->pdf->SetTextColor(0,0,0);
        $this->pdf->Ln();
        $this->generateTableFssp($irRequest->fsspPeoples);
        $this->generateTableArbitr($irRequest->arbitrPeoples);
        $this->generateTableBankrot($irRequest->bankrotPeoples);
        $this->generateTableJudge($irRequest->judgePeoples);
        $this->generateTableOrganisation($irRequest->organisationPeoples);
        $this->generateTablePledge($irRequest->pledgePeoples);
        $this->generateTableTerrorist($irRequest->terroristPeoples);
        $this->generateTableBadContract($irRequest->badContracts);
        $this->generateTableGc($irRequest->gcPeoples);
    }

    /**
     * @param $irRequest IrbisRequest
     */
    protected function generateFirstPage($irRequest)
    {
        $this->pdf->AddPage();
        $this->addName($irRequest);
        $this->pdf->SetFontSize(12);
        $this->pdf->Ln(8);
        $this->pdf->SetTextColor(74, 126, 230);
        $this->pdf->Cell(80, 7, 'Общая информация', 0, 0, 'L', 0);
        $this->pdf->Ln(1);
        $this->pdf->SetTextColor(0, 0, 0);
        $irRequest->irbisPeoples->getBirthDate() ? $this->addCell('Дата рождения', $irRequest->irbisPeoples->getBirthDate()) : null;
        $this->addCell('Регион', $irRequest->irbisPeoples->getRegion());
        $this->addCell('ИНН', $irRequest->inn);
        $irRequest->irbisPeoples->getOrg() ? $this->addCell('Компания', $irRequest->irbisPeoples->getOrg()) : null;
        $irRequest->ser_passport ? $this->addCell('Паспорт', $irRequest->ser_passport . ' ' . $irRequest->num_passport) : null;
        $irRequest->irbisPeoples->phone ? $this->addCell('Телефон', $irRequest->irbisPeoples->phone) : null;
        $this->addCell('ФМС', $this->getFmsInfo($irRequest), $this->needImg);
        $this->addCell('Терроризм', $this->getTerroristInfo($irRequest), $this->needImg);
        $irRequest->bankrotPeoples ? $this->addCell('Банкротство', 'Банкрот', self::IMAGE_BAD) : $this->addCell('Банкротство', 'Не является банкротом', self::IMAGE_GOOD);
        $this->addCell('ФССП', $this->getFsspInfo($irRequest), $this->needImg);
        $this->addCell('Поставщик', $this->getBadProviderInfo($irRequest), $this->needImg);
        $this->addCell('Залоги', $this->getPledgeInfo($irRequest), $this->needImg);
        $this->addArbitrCount('Арбитражные суды', $irRequest->arbitrPeoples);
        $this->addJudgeCount('Суды', $irRequest->judgePeoples);
    }

    /**
     * @param $irRequest IrbisRequest
     */
    protected function addName($irRequest)
    {
        $txt = $irRequest->irbisPeoples->getName();
        $this->pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
    }

    /**
     * @param $name string Наименование ячейки
     * @param $value string Значение ячейки
     */
    protected function addCell($name, $value, $needImg = false)
    {
        $this->fillCell();
        $this->drawHorizontalLine();
        $this->drawVerticalLine();
        if ($needImg) {
            if ($needImg == self::IMAGE_GOOD) {
                $this->pdf->Image($this->good, 70, $this->height - 5, 7);
            } else {
                $this->pdf->Image($this->bad, 70, $this->height - 5, 7);
            }
        }
        $this->pdf->SetCellPadding(10);
        $this->pdf->Cell(60, 7, $name, 0, 0, 'L', 0);
        $this->pdf->Cell(100, 7, $value, 0, 0, 'L', 0);
        $this->pdf->Ln(8);
        $this->height += 8;
    }

    /**
     * Заливка зеброй
     */
    public function fillCell(){
        if ($this->fill == 0) {
            $this->pdf->SetFillColorArray([240, 240, 240]);
        } else {
            $this->pdf->SetFillColorArray([255, 255, 255]);
        }
        $this->pdf->Rect(10, $this->height-6, 180, 8, 'F', []);
        $this->fill = !$this->fill;
    }

    /**
     * @param $name string Наименование ячейки
     * @param $values ArbitrPeople[]
     */
    protected function addArbitrCount($name, $values)
    {
        $this->fillCell();
        $data = [
            'count' => count($values),
            'R' => 0,
            'P' => 0
        ];
        foreach ($values as $item) {
            $data[$item->type_member]++;
        }
        $this->drawHorizontalLine();
        $this->pdf->SetX(10);
        if ($data['R'] > 0) {
            $this->pdf->Image($this->bad, 70, $this->height - 5, 7);
        } else {
            $this->pdf->Image($this->good, 70, $this->height - 5, 7);
        }
        $this->pdf->Cell(60, 7, $name, 0, 0, 'L', 0);
        if ($data['count'] > 0) {
            $wP = $data['P'] / $data['count'] * 100;
            $wR = $data['R'] / $data['count'] * 100;
            $this->pdf->SetCellPadding(10);
            $this->pdf->Cell(100, 7, 'Количество - ' . $data['count'], 0, 0, 'L', 0);
            $this->height += 6;
            $this->pdf->Ln(12);
            $this->pdf->Rect(90, $this->height, 100, 22, 'D');
            $this->pdf->Rect(90, $this->height, 100, 10, 'D');
            $this->pdf->Rect(90, $this->height, $wP, 10, 'DF', [], [0, 166, 90]);
            $this->height += 12;
            $this->pdf->Rect(90, $this->height, $wR, 10, 'DF', [], [245, 105, 84]);
            $this->pdf->Rect(90, $this->height, 100, 10, 'D');
            $this->pdf->SetLeftMargin(90);
            $this->pdf->Cell(100, 7, 'Истец - ' . $data['P'] . ' (' . round($data['P'] / $data['count'] * 100, 2) . '%)', 0, 0, 'L', 0);
            $this->pdf->Ln(12);
            $this->pdf->Cell(100, 7, 'Ответчик - ' . $data['R'] . ' (' . round($data['R'] / $data['count'] * 100, 2) . '%)', 0, 0, 'L', 0);
            $this->pdf->Ln(12);
            $this->height += 18;
            $this->drawHorizontalLine();
        } else {
            $this->drawVerticalLine();
            $this->pdf->Cell(100, 7, 'Не участвовал в судебных процессах', 0, 0, 'L', 0);
            $this->pdf->Ln(8);
            $this->height += 8;
            $this->drawHorizontalLine();
        }
    }

    /**
     * @param $name string Наименование ячейки
     * @param $values JudgePeople[]
     */
    protected function addJudgeCount($name, $values)
    {
        $this->fillCell();
        $data = [
            'count' => count($values),
            'A' => 0,
            'U' => 0,
            'G' => 0,
            'O' => 0,
            'def' => 0,
            'plan' => 0,
            'other' => 0
        ];
        foreach ($values as $item) {
            $data[$item->type_cause]++;
            $data[$item->role]++;
        }
        $this->drawHorizontalLine();
        $this->pdf->SetX(10);
        if ($data['U'] > 0) {
            $this->pdf->Image($this->bad, 70, $this->height - 5, 7);
        } else {
            $this->pdf->Image($this->good, 70, $this->height - 5, 7);
        }
        $this->pdf->Cell(60, 7, $name, 0, 0, 'L', 0);
        if ($data['count'] > 0) {
            $this->pdf->Cell(100, 7, 'Количество - ' . $data['count'], 0, 0, 'L', 0);
            $this->pdf->Ln(8);
            $this->pdf->SetLeftMargin(90);
            $y =$this->height-1.5;
            $this->pdf->Cell(100, 7, 'Административные дела - ' . $data['A'], 0, 0, 'L', 0);
            $this->pdf->Circle(93, $y+=8, 2.5, 0, 360, "F", [], [74, 126, 230]);
            $this->pdf->Ln(8);
            $this->pdf->Cell(100, 7, 'Уголовные дела - ' . $data['U'], 0, 0, 'L', 0);
            $this->pdf->Circle(93, $y+=8, 2.5, 0, 360, "F", [], [245, 105, 84]);
            $this->pdf->Ln(8);
            $this->pdf->Cell(100, 7, 'Гражданские дела - ' . $data['G'], 0, 0, 'L', 0);
            $this->pdf->Circle(93, $y+=8, 2.5, 0, 360, "F", [], [0, 166, 90]);
            $this->pdf->Ln(8);
            $this->pdf->Cell(100, 7, 'Оспаривание решений - ' . $data['O'], 0, 0, 'L', 0);
            $this->pdf->Circle(93, $y+=8, 2.5, 0, 360, "F", [], [209,209,209]);
            $this->pdf->Ln(8);
            $this->pdf->Cell(100, 7, 'Истец - ' . $data['plan'] . ' (' . round($data['plan'] / $data['count'] * 100, 2) . '%)', 0, 0, 'L', 0);
            $this->pdf->Ln(8);
            $this->pdf->Cell(100, 7, 'Ответчик - ' . $data['def'] . ' (' . round($data['def'] / $data['count'] * 100, 2) . '%)', 0, 0, 'L', 0);
            $this->pdf->Ln(8);
            $this->height += 56;
            $this->drawHorizontalLine();
        } else {
            $this->drawVerticalLine();
            $this->pdf->Cell(100, 7, 'Не участвовал в судебных процессах', 0, 0, 'L', 0);
            $this->pdf->Ln(8);
            $this->height += 8;
            $this->drawHorizontalLine();
        }
    }

    /**
     * @param $request IrbisRequest
     */
    public function getFsspInfo($request)
    {
        $data = [
            'count' => count($request->fsspPeoples),
            'open' => 0,
            'close' => 0
        ];
        $this->needImg = self::IMAGE_BAD;
        /**@var $fsspPeople FsspPeople */
        foreach ($request->fsspPeoples as $fsspPeople) {
            if ($fsspPeople->date_end) {
                $data['close']++;
            } else {
                $data['open']++;
                return 'Есть открытые исполнительные производства';
            }
        }
        if ($data['close'] > 0) {
            return 'Есть закрытые исполнительные производства';
        }
        $this->needImg = self::IMAGE_GOOD;
        return 'Не участвовал в исполнительные производства';
    }

    /**
     * @param $request IrbisRequest
     */
    public function getBadProviderInfo($request)
    {
        if (count($request->badContracts) > 0) {
            $this->needImg = self::IMAGE_BAD;
            return 'Недобросовестный поставщик';
        }
        $this->needImg = self::IMAGE_GOOD;
        return 'Добросовестный поставщик';
    }

    /**
     * Список террористов
     * @param $request IrbisRequest
     */
    public function getTerroristInfo($request)
    {
        if ($request->irbisPeoples->terrorist == IrbisPeople::TERRORIST_YES) {
            $this->needImg = self::IMAGE_BAD;
            return 'Числится в списке террористов';
        }
        $this->needImg = self::IMAGE_GOOD;
        return 'В списке террористов отсутствует';
    }

    /**
     * Сведения из ФМС
     * @param $request IrbisRequest
     * @return string
     */
    public function getFmsInfo($request)
    {
        if ($request->irbisPeoples->terrorist === IrbisPeople::FMS_NO_VALID) {
            $this->needImg = self::IMAGE_BAD;
            return 'Паспорт числится в списке не действительных';
        }
        $this->needImg = self::IMAGE_GOOD;
        return 'Паспорт действителен';
    }

    /**
     * Сведения по залогам
     * @param $request IrbisRequest
     * @return string
     */
    public function getPledgeInfo($request)
    {
        if (count($request->pledgePeoples) > 0) {
            $this->needImg = self::IMAGE_BAD;
            return 'Имеются залоги имущества';
        }
        $this->needImg = self::IMAGE_GOOD;
        return 'Залоги отсутствуют';
    }

    public function drawHorizontalLine(){
        $this->pdf->Line(10, $this->height-6, 190, $this->height-6, ['color' => [225, 225, 225]]);
    }

    public function drawVerticalLine(){
        $this->pdf->Line(70, $this->height-6, 70, $this->height+2, ['color' => [225, 225, 225]]);
    }

    /**
     * Применение стилей к шапке таблице
     * @param $name
     */
    public function setStyleHeadTable($name){
        if ($this->pdf->GetY() > 240){
            $this->pdf->AddPage();
        }
        $this->pdf->SetLeftMargin(20);
        $this->pdf->SetFontSize(14);
        $this->pdf->Cell(80, 7, $name, 0, 0, 'L', 0);
        $this->pdf->Ln();
        $this->pdf->SetFontSize(10);
        $this->pdf->SetCellPadding(0);
        $this->pdf->SetFillColor(224, 235, 255);
        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->SetDrawColor(225, 225, 225);
        $this->pdf->SetLineWidth(0.3);
    }
    /**
     * Применение стилей к шапке таблице
     */
    public function setStyleBodyTable(){
        $this->pdf->Ln();
        $this->pdf->SetFont('');
        $this->pdf->SetFontSize(8);
        $this->pdf->SetFillColor(235, 235, 235);
        $this->pdf->SetTextColor(0);
    }

    /**
     * Создание шапки талиц
     * @param $header
     * @param $w
     */
    public function setHeaderTable($header, $w){
        $num_headers = count($header);
        for ($i = 0; $i < $num_headers; ++$i) {
            $this->pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'L', 1);
        }
    }
    /**
     * @param $data FsspPeople[]
     */
    public function generateTableFssp($data){
        if ($data) {
            $this->setStyleHeadTable("ФССП");
            $header = ['Код', 'Предмет исполнения', 'Сумма', 'Дата завершения'];
            $w = [50, 60, 30, 30];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->code, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->subject, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->sum, 1, 0, 'R', $fill);
                $this->pdf->Cell($w[3], 6, $item->date_end ? Yii::$app->formatter->asDate($item->date_end, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data ArbitrPeople[]
     */
    public function generateTableArbitr($data){
        if ($data) {
            $this->setStyleHeadTable('Арбитражные суды');
            $header = ['Участник', 'Код', 'Тип участника', 'Дата завершения'];
            // Header
            $w = [90, 20, 30, 30];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, mb_strlen($item->member, 'UTF-8') > 60 ? mb_substr($item->member, 0, 60, 'UTF-8') . '...' : $item->member, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->name, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->getTypeMember(), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->date ? Yii::$app->formatter->asDate($item->date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }
    /**
     * @param $data BankrotPeople[]
     */
    public function generateTableBankrot($data){
        if ($data) {
            $this->setStyleHeadTable('Банкротство');
            $header = ['Банкрот', 'Код', 'Дата банкротства'];
            // Header
            $w = [80, 60, 30];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->bankrot, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->code, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->date_bankrot ? Yii::$app->formatter->asDate($item->date_bankrot, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data JudgePeople[]
     */
    public function generateTableJudge($data){
        if ($data) {
            $this->setStyleHeadTable('Суды');
            $header = ['Код', 'Дата создания', 'Тип дела', 'Тип участника', 'Решение', 'Дата закрытия'];
            // Header
            $w = [30, 25, 35, 25, 25, 30];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->case_number, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->date ? Yii::$app->formatter->asDate($item->date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->getType(), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->getRole(), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->resolution, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[5], 6, $item->end_date ? Yii::$app->formatter->asDate($item->end_date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data OrganisationPeople[]
     */
    public function generateTableOrganisation($data){
        if ($data) {
            $this->setStyleHeadTable('Организации');
            $header = ['Название', 'ИНН', 'ОГРН', 'Должность', 'Статус'];
            // Header
            $w = [65, 18, 22, 45, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, mb_convert_case($item->name, MB_CASE_LOWER, "UTF-8"), 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->inn, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->ogrn, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->position, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->getStatus(), 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data PledgePeople[]
     */
    public function generateTablePledge($data){
        if ($data) {
            $this->setStyleHeadTable('Залоги');
            $header = ['ФИО', 'Залогодержатель', 'Тип залога', 'Дата начала', 'Дата окончания'];
            // Header
            $w = [70, 40, 20, 20, 20];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->fio, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->pawnbroker, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->type_pledge, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->date_add ? Yii::$app->formatter->asDate($item->date_add, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->date_end ? Yii::$app->formatter->asDate($item->date_end, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data TerroristPeople[]
     */
    public function generateTableTerrorist($data){
        if ($data) {
            $this->setStyleHeadTable('Список террористов');
            $header = ['ФИО', 'Дата рождения', 'Место рождения'];
            // Header
            $w = [70, 30, 70];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->name, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->birth_date ? Yii::$app->formatter->asDate($item->birth_date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->birth_place, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data BadContract[]
     */
    public function generateTableBadContract($data){
        if ($data) {
            $this->setStyleHeadTable('Список недобросовестных поставщиков');
            $header = ['Код', 'Внесен', 'Исключен', 'Дата контракта', 'Сумма', 'Причина'];
            // Header
            $w = [30, 20, 20, 30, 20, 50];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->registry_number, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->date_start ? Yii::$app->formatter->asDate($item->date_start, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->date_end ? Yii::$app->formatter->asDate($item->date_end, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[3], 6, $item->date_contract ? Yii::$app->formatter->asDate($item->date_contract, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->sum, 1, 0, 'R', $fill);
                $this->pdf->Cell($w[5], 6, $item->reason, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }

    /**
     * @param $data GcPeople[]
     */
    public function generateTableGc($data){
        if ($data) {
            $this->setStyleHeadTable('Государственные контракты');
            $header = ['Заказчик', 'Дата заключения', 'Сумма', 'Статус', 'Результат'];
            // Header
            $w = [40, 30, 20, 25, 55];
            $this->setHeaderTable($header, $w);
            $this->setStyleBodyTable();
            $fill = 0;
            foreach ($data as $item) {
                $this->pdf->Cell($w[0], 6, $item->customer, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[1], 6, $item->date ? Yii::$app->formatter->asDate($item->date, 'php:d.m.Y') : null, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[2], 6, $item->sum, 1, 0, 'R', $fill);
                $this->pdf->Cell($w[3], 6, $item->status, 1, 0, 'L', $fill);
                $this->pdf->Cell($w[4], 6, $item->result, 1, 0, 'L', $fill);
                $this->pdf->Ln();
                $fill = !$fill;
            }
            $this->pdf->Ln();
        }
    }
}