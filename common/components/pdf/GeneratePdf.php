<?php
namespace common\components\pdf;

use common\models\IrbisOrg;
use common\models\IrbisPeople;
use TCPDF;

class GeneratePdf extends TCPDF
{
    public $pdf;
    /**
     * @var string Заголовок
     */
    public $title;
    /**
     * @var string Имя обычного шрифта
     */
    public $fontRegular;
    /**
     * @var string Имя жирного шрифта
     */
    public $fontBold;
    /**
     * @var string Имя шрифта для телефонов
     */
    public $fontConsole;

    public $name;
    public $inn;


    /**
     * GeneratePdf constructor.
     * @param IrbisPeople|IrbisOrg $irRequest
     * @param null $orientation
     */
    public function __construct($irRequest, $orientation = null)
    {
        $this->title = $irRequest->getName();
        parent::__construct($orientation);
        $this->SetAutoPageBreak(false, 17);
        $this->init();
    }

    /**
     * Инициализация настроек
     */
    public function init()
    {
        // Шрифты
        $this->fontRegular = \TCPDF_FONTS::addTTFfont(dirname(__FILE__) . '/font/Circe-Regular.ttf', 'TrueTypeUnicode', '', 96);
        $this->fontBold = \TCPDF_FONTS::addTTFfont(dirname(__FILE__) . '/font/Circe-Bold.ttf', 'TrueTypeUnicode', '', 96);
        $this->fontConsole = \TCPDF_FONTS::addTTFfont(dirname(__FILE__) . '/font/LucidaConsole.ttf', 'TrueTypeUnicode', '', 96);
    }

    public function generate(){

    }

    public function Header(){}

    public function Footer(){}

}