<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.09.17
 * Time: 12:57
 */

namespace common\components;


interface FileInterface
{
    /**
     * Сохранение файла
     * @param mixed $content Содержимое файла
     * @param string $ext Расширение файла
     * @param bool $shortName Возвращать короткое имя файла
     * @return string
     */
    public function save($content, $ext, $shortName = true);
}