<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 9:19
 */

namespace common\components\irbis;

use common\components\irbis\models\UuidResult;
use common\models\BadContract;
use yii\db\Exception;


/**
 * Получение информации по недобросовестным поставщикам
 * Class BadProvider
 * @package common\components\irbis
 */
class BadProvider
{

    public function __construct(UuidResult $config)
    {
        $this->irbisRequestId = $config->getIrbisRequestId();
    }

    public function getAllBadProvider($inn)
    {
        // Выполняем запрос в реестр недобросовестных поставщиков
        $str = $this->send(
            'http://zakupki.gov.ru/epz/dishonestsupplier/quicksearch/search.html',
            [
                'searchString'=> $inn,
                'morphology'=>'on',
                'pageNumber'=>1,
                'sortDirection'=>'false',
                'recordsPerPage'=>'_50',
                'fz94'=>'on',
                'fz223'=>'on',
                'inclusionDateFrom'=>'',
                'inclusionDateTo'=>'',
                'lastUpdateDateFrom'=>'',
                'lastUpdateDateTo'=>'',
                'sortBy'=>'UPDATE_DATE',
            ]
        );
        preg_match_all('|<table.*?>(.*?)</table>|is', $str, $matches);
        $items = [];
        // Получаем список недобросовестных поставок
        foreach ($matches[0] as $str) {
            preg_match('|<td.*?class="descriptTenderTd"*?>(.*?)</table>|is', $str, $match);
            if (!empty($match)) {
                $items[] = $match[1];
            }
        }
        // Цикл по списку полученных компаний
        foreach ($items as $item) {
            $this->saveBadContract($this->getBadProvider($item));
        }
    }

    /**
     * Получение подробной информации о заявке реестра недобросовестного поставщика
     * @param $item string
     */
    protected function getBadProvider($item)
    {
        // Получение сслыки на подробную информацию о недобросовестном поставщике
        preg_match_all('|<a href="(.*?)"|is', $item, $url);
        $company['url'] = $url[1][0];
        $str = $this->send($url[1][0]);
        preg_match_all('/<div class="noticeTabBox">[\s\S]*?<\/div>/is', $str, $match);
        $company['contract'] = array_merge($this->getMainInformation($match[0][1]),
            $this->getMainInformation($match[0][4]));
        return $company;
    }

    /**
     * Получение полей реестра недобросовестного поставщика
     * @param $str string
     */
    protected function getMainInformation($str)
    {
        preg_match_all('|<td(.*?)</td>|is', $str, $match);
        $company = [];
        foreach ($match[0] as $i) {
            $str = preg_replace('/<td[\s\S]*?>/is', '', $i);
            $str = preg_replace('/[\s]*?<\/td>/is', '', $str);
            $str = preg_replace('/[\s]{2,}/is', '', $str);
            $company[] = $str;
        }
        return $company;
    }

    /**
     * @param $company array
     */
    protected function saveBadContract($company)
    {
        if (!empty($company)) {
            $badContract = new BadContract();
            $badContract->url = $company['url'];
            $badContract->irbis_request_id = $this->irbisRequestId;
            $contract = $company['contract'];
            for ($i = 0, $cnt = count($contract); $i < $cnt; $i += 2) {
                switch ($contract[$i]) {
                    case 'Цена контракта':
                        $badContract->sum = $contract[$i + 1];
                        break;
                    case 'Дата заключения':
                        $badContract->date_contract = !empty($contract[$i + 1]) ? \Yii::$app->formatter->asDate($contract[$i + 1],
                            'php:Y-m-d') : null;
                        break;
                    case 'Причина для внесения в РНП 44-ФЗ':
                        $badContract->reason = $contract[$i + 1];
                        break;
                    case 'Реестровый номер':
                        $badContract->registry_number = $contract[$i + 1];
                        break;
                    case 'Дата включения сведений в РНП 44-ФЗ':
                        $badContract->date_start = \Yii::$app->formatter->asDate($contract[$i + 1], 'php:Y-m-d');
                        break;
                    case 'Дата для исключения':
                        $badContract->date_end = \Yii::$app->formatter->asDate($contract[$i + 1], 'php:Y-m-d');
                        break;
                    default:
                        break;
                }
            }
            if (!$badContract->save()) {
                throw new Exception(implode(',', $badContract->errors) . ' Ошибка сохранения в таблицу bad_contract');
            }
        }
    }

    /**
     * Выполнение URL
     * @param string $url
     * @param array $urlFields
     * @return string|null
     * @throws \Exception
     */
    protected function send($url, $urlFields=[])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($urlFields)));
        curl_setopt($ch, CURLOPT_HEADER, false);
        // Костыли, требуемые для отмена проверки SSL-сертификата. Потенциально опасное место для MITM-атаки
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $str = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($code != 200 && $code != 204) {
            $error = curl_error($ch);
//            throw new \Exception("Не удалось сделать запрос, код ошибки - " . $code);
            \Yii::warning("Не удалось сделать запрос, код ошибки - " . $code.': '.$error, 'irbis');
            return null;
        }
        return $str;
    }
}