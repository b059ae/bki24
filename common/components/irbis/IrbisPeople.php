<?php

namespace common\components\irbis;

use common\components\irbis\models\UuidResult;
use common\components\irbis\request\ArbitrPeopleRequest;
use common\components\irbis\request\BankrotPeopleRequest;
use common\components\irbis\request\FmsPeopleRequest;
use common\components\irbis\request\FsspPeopleRequest;
use common\components\irbis\request\GcPeopleRequest;
use common\components\irbis\request\InterestPeopleRequest;
use common\components\irbis\request\JudgeArchivePeopleRequest;
use common\components\irbis\request\JudgePeopleRequest;
use common\components\irbis\request\OrgsPeopleRequest;
use common\components\irbis\request\PeopleRequest;
use common\components\irbis\request\PledgePeopleRequest;
use common\components\irbis\request\TerroristPeopleRequest;
use Yii;
use yii\base\Exception;
use yii\base\Object;

class IrbisPeople extends Object
{
    /**
     * @var array Поля для запроса полученные из формы
     */
    public $field = [];

    /**
     * Делает запрос в Ирбис и возвращает его UUID
     * @return UuidResult
     */
    public function getUuid()
    {
        return \Yii::$app->irbis->getUuid(new PeopleRequest(), $this->field);
    }

    /**
     *  Запрос по физическому лицу
     */
    public function individual(UuidResult $result)
    {
        $this->saveIrbisPeople($result->getIrbisRequestId());
        if ($result) {
            \Yii::$app->irbis->call(new ArbitrPeopleRequest(), $result);
            \Yii::$app->irbis->call(new BankrotPeopleRequest(), $result);
            \Yii::$app->irbis->call(new FsspPeopleRequest(), $result);
            \Yii::$app->irbis->call(new GcPeopleRequest(), $result);
            \Yii::$app->irbis->call(new JudgePeopleRequest(), $result);
            \Yii::$app->irbis->call(new OrgsPeopleRequest(), $result);
            \Yii::$app->irbis->call(new FmsPeopleRequest(), $result);
            \Yii::$app->irbis->call(new TerroristPeopleRequest(), $result);
            \Yii::$app->irbis->call(new JudgeArchivePeopleRequest(), $result);
            \Yii::$app->irbis->call(new PledgePeopleRequest(), $result);
            \Yii::$app->irbis->call(new InterestPeopleRequest(), $result);
            if (!empty($this->field['inn'])) {
                $badProvider = new BadProvider($result);
                $badProvider->getAllBadProvider($this->getFio() . ' ' . $this->field['inn']);
            }
        }
        return $result->getIrbisRequestId();
    }

    private function saveIrbisPeople($idIrbisRequest)
    {
        $people = new \common\models\IrbisPeople();
        $people->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'fio' => $this->getFio(),
            'region' => !empty($this->field['regions']) ? $this->field['regions'] : null,
            'birth_date' => !empty($this->field['birthDate']) ? Yii::$app->formatter->asDate($this->field['birthDate'],
                'php:Y-m-d') : null,
            'org_id' => !empty($this->field['org_id']) ? $this->field['org_id'] : null,
            'founders_id' => !empty($this->field['founders_id']) ? $this->field['founders_id'] : null,
            'manager_id' => !empty($this->field['manager_id']) ? $this->field['manager_id'] : null,
        ]);
        if (!$people->save()) {
            throw new Exception("Не удалось сохранить в таблицу irbis_people");
        }
    }

    public function updateFields(UuidResult $result)
    {
        \Yii::$app->irbis->update(new ArbitrPeopleRequest(), $result);
        \Yii::$app->irbis->update(new BankrotPeopleRequest(), $result);
        \Yii::$app->irbis->update(new FsspPeopleRequest(), $result);
        \Yii::$app->irbis->update(new JudgePeopleRequest(), $result);
    }

    public function getFio()
    {
        return $this->field['lastName'] . ' ' . $this->field['firstName'] . ' ' . $this->field['secondName'];
    }

//    //TODO нет данных
//    /**
//     * Запрос по егрип Физ лиц
//     * @param $queryUID string Код запроса
//     */
//    protected function egripPeople($queryUID)
//    {
//        // Запрос по данным ЕГРИП
//        $url = 'https://ir-bis.org/ru/base/-/services/report/' . $queryUID . '/people-egrip.json';
//        $fields = 'token=' . $this->token .
//            '&fields=[' . $this->fieldsEGRIP . ']' .
//            '&event=result&view=jqgrid' .
//            '&page=' . $this->page . '&rows=' . $this->rows;
//        $str = $this->getJson($url, $fields);
//        $json = json_decode($str, true);
//        echo "--------------ЕГРИП--------------";
//        var_dump($json);
//    }
//
//    //TODO Если попадется проверить ключи
//    /**
//     * Запрос проверки по архиву
//     * @param $queryUID string Код запроса
//     */
//    protected function archivePeople($queryUID)
//    {
//        $url = 'https://ir-bis.org/ru/base/-/services/report/' . $queryUID . '/people-archive.json';
//        $fields = 'token=' . $this->token .
//            '&fields=[' . $this->fieldsArchive . ']' .
//            '&event=result&view=jqgrid' .
//            '&page=' . $this->page . '&rows=' . $this->rows;
//        $str = $this->getJson($url, $fields);
//        $json = json_decode($str, true);
//        echo "--------------Архив (Если попадется проверить ключи)--------------";
//        var_dump($json);
//        if (!empty($json['response']['rows'])) {
//            foreach ($json['response']['rows'] as $item) {
//                if (!empty($item['cell'])) {
//                    $archive = new ArchivePeople();
//                    $archive->setAttributes([
//                        'id_irbis_people' => $this->irbis_people_id,
//                        'full_name' => $item['cell'][0],
//                        'inn' => $item['cell'][1],
//                        'result' => $item['cell'][2],
//                    ]);
//                    if (!$archive->save()) {
//                        throw new Exception('не удалось добавить данные в таблицу archive_people');
//                    }
//                }
//            }
//        }
//    }
//
//
//    /**
//     * Запрос проверки по списку дисквалифицированных
//     * @param $queryUID string Код запроса
//     */
//    protected function disqualifiedPeople($queryUID)
//    {
//        $url = 'https://ir-bis.org/ru/base/-/services/report/' . $queryUID . '/people-disqualified.json';
//        $fields = 'token=' . $this->token .
//            '&fields=[' . $this->fieldsDisqualified . ']' .
//            '&event=result&view=jqgrid' .
//            '&page=' . $this->page . '&rows=' . $this->rows;
//        $str = $this->getJson($url, $fields);
//        $json = json_decode($str, true);
//        echo "--------------Дисквалифицированные (Если попадется проверить ключи)--------------";
//        var_dump($json);
//        if (!empty($json['response']['rows'])) {
//            foreach ($json['response']['rows'] as $item) {
//                if (empty($item['cell'])) {
//                    //TODO НЕ ИЗВЕСТНЫ КЛЮЧИ
//                    $dis = new DisqualifiedPeople();
//                    $dis->setAttributes([
//                        'id_irbis_people' => $this->irbis_people_id,
//                        'fio' => $item['cell'][1],
//                        'birth_date' => $item['cell'][2],
//                        'bornplace' => $item['cell'][3],
//                        'legal_name' => $item['cell'][4],
//                        'start_date_disq' => $item['cell'][5],
//                        'end_date_disq' => $item['cell'][6],
//                        'office' => $item['cell'][7],
//                        'department' => $item['cell'][8],
//                        'article' => $item['cell'][9],
//                        'fio_judge' => $item['cell'][10],
//                        'office_judge' => $item['cell'][11],
//                    ]);
//                    if (!$dis->save()) {
//                        throw new Exception("Не удалось добавить запись в таблицу disqualified_people");
//                    }
//                }
//            }
//        }
//    }

}