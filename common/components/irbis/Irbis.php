<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 9:19
 */

namespace common\components\irbis;


use common\components\irbis\models\UuidResult;
use common\components\irbis\request\interfaces\Request;
use common\components\irbis\request\interfaces\UuidRequest;
use Exception;
use yii\base\Component;

class Irbis extends Component
{
    /**
     * @var string Токен для irbis
     */
    public $token;

    /**
     * @var integer id компании запрошенной в ирбис
     */
    public $irbisRequestId;

    /**
     * @param $request
     * @param UuidResult $result
     */
    public function call(Request $request, UuidResult $result)
    {
        $url = $request->getUrl($result->getUuid());
        $params = $request->getParams();
        $json = $this->send($url, $params);
        if (isset($json['response']['rows'])) {
            if (isset($json['response']['result'])) {
                foreach ($json['response']['result'] as $item) {
                    $request->response($item, $result->getIrbisRequestId());
                }
            } else {
                foreach ($json['response']['rows'] as $item) {
                    if (!empty($item['cell'])) {
                        $request->response($item, $result->getIrbisRequestId());
                    }
                }
            }
        } else {
            if (!empty($json['response'])) {
                $request->response($json['response'], $result->getIrbisRequestId());
            }
        }
    }

    /**
     * @param $request
     * @param array $fields
     * @return UuidResult|null
     */
    public function getUuid(UuidRequest $request, $fields){
        $url = $request->getUrl();
        $params = $request->getParams($fields);
        $json = $this->send($url, $params);
        //UID код запроса
        if (isset($json['uuid'])) {
            $idIrbisRequest = $request->saveRequest($json['uuid'], $fields);
            return new UuidResult($json['uuid'], $idIrbisRequest);
        }
        return null;
    }

    /**
     * @param Request $request
     * @param UuidResult $result
     */
    public function update(Request $request, UuidResult $result)
    {
        $url = $request->getUrl($result->getUuid());
        $params = $request->getParams();
        $json = $this->send($url, $params);
        if (isset($json['response']['rows'])) {
            if (isset($json['response']['result'])) {
                foreach ($json['response']['result'] as $item) {
                    $request->update($item, $result->getIrbisRequestId());
                }
            } else {
                foreach ($json['response']['rows'] as $item) {
                    if (!empty($item['cell'])) {
                        $request->update($item, $result->getIrbisRequestId());
                    }
                }
            }
        } else {
            if (!empty($json['response'])) {
                $request->update($json['response'], $result->getIrbisRequestId());
            }
        }
    }

    /**
     * Выполнение URL
     * @param string $url
     * @param array $urlFields
     * @return array|null
     * @throws Exception
     */
    protected function send($url, $urlFields)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        // Не используем http_build_query из-за urlencode в нем
        curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query(array_merge(
            [
                'token' => $this->token
            ],
            $urlFields
        ))));
        curl_setopt($ch, CURLOPT_HEADER, false);
        // Костыли, требуемые для отмена проверки SSL-сертификата. Потенциально опасное место для MITM-атаки
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $str = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($code != 200 && $code != 204) {
            $error = curl_error($ch);
//            throw new \Exception("Не удалось сделать запрос, код ошибки - " . $code);
            \Yii::warning("Не удалось сделать запрос, код ошибки - " . $code.': '.$error, 'irbis');
            return null;
        }
        return json_decode($str, true);
    }
}