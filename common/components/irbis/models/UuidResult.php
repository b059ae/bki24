<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 31.08.17
 * Time: 9:41
 */

namespace common\components\irbis\models;

/**
 * Объект ответа Ирбис на получение UUID
 * Class Request
 * @package common\components\irbis\models
 */
class UuidResult
{
    private $uuid;
    private $irbisRequestId;

    public function __construct($uuid, $irbisRequestId)
    {
        $this->uuid = $uuid;
        $this->irbisRequestId = $irbisRequestId;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function getIrbisRequestId()
    {
        return $this->irbisRequestId;
    }
}