<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\PledgePeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class PledgePeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-pledge.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid',
    ];
    public $arrayFields = '"pledgers","pledgees","pledge_type","reg_date","end_date","pledges","pledgers_data","pledgees_data"';

    public function response($item, $idIrbisRequest)
    {
        $pledge = new PledgePeople();
        $pledge->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'fio' => $item['cell'][0]['peoples'][0]['name'],
            'passport' => $item['cell'][0]['peoples'][0]['passport_series'] . ' ' . $item['cell'][0]['peoples'][0]['passport_number'],
            'birth_date' => $item['cell'][0]['peoples'][0]['birth_date'],
            'pawnbroker' => $item['cell'][1]['orgs'][0]['name'] . ', ИНН:' . $item['cell'][1]['orgs'][0]['inn'] . ', ОГРН:' . $item['cell'][1]['orgs'][0]['ogrn'],
            'type_pledge' => $item['cell'][2],
            'date_add' => $item['cell'][3],
            'date_end' => $item['cell'][4],
        ]);
        if (!$pledge->save()) {
            throw new Exception();
        }
    }
}