<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\ArbitrOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class ArbitrInnOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-arbitr.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result_inn',
        'view' => 'jqgrid',
    ];
    public $arrayFields = '"case_number","name","address_val","role","court_name_val","case_date","case_id","mobCol"';

    public function response($item, $idIrbisRequest)
    {
        $arbitr = new ArbitrOrg();
        $arbitr->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'type_request' => ArbitrOrg::TYPE_REQUEST_INN,
            'address' => $item['cell'][2],
            'member' => $item['cell'][1],
            'judge' => $item['cell'][4],
            'type_member' => $item['cell'][3],
            'date' => $item['cell'][5],
            'url' => $item['cell'][6],
        ]);
        if (!$arbitr->save()) {
            throw new Exception();
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'type_request' => ArbitrOrg::TYPE_REQUEST_INN,
            'address' => $item['cell'][2],
            'member' => $item['cell'][1],
            'judge' => $item['cell'][4],
            'type_member' => $item['cell'][3],
            'date' => $item['cell'][5],
            'url' => $item['cell'][6],
        ];
        $arbitr = ArbitrOrg::findOne(['irbis_request_id' => $idIrbisRequest, 'name' => $item['cell'][0]]);
        if ($arbitr) {
            $arbitr->updateAttributes($data);
        } else {
            $arbitr = new ArbitrOrg();
            $arbitr->setAttributes($data);
        }
        if (!$arbitr->save()) {
            throw new Exception();
        }
    }
}