<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\IrbisPeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class FmsPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-passport.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event=result'
    ];

    public function response($item, $idIrbisRequest)
    {
        $people = IrbisPeople::findOne(['irbis_request_id' => $idIrbisRequest]);
        $people->updateAttributes([
            'fms' => $item,
        ]);
        if (!$people->save()) {
            throw new Exception("Не удалось сохранить данные ФМС в таблицу irbis_people");
        }
    }
}