<?php

namespace common\components\irbis\request;

use common\components\irbis\IrbisPeople;
use common\components\irbis\request\interfaces\Request;
use common\models\FoundersFlOrg;
use common\models\FoundersLegalOrg;
use common\models\JudgeOrg;
use common\models\ManagerFlOrg;
use common\models\OkvedOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class EgrulOrgRequest extends RequestAbstract implements  Request
{
    /**
     * @var bool Проверка учредителей
     */
    public $checkFounders = false;
    /**
     * @var bool Проверка руководителя
     */
    public $checkManager = false;
    /**
     * @var string Тип запроса
     */
    public $type = '/org-egrul.json';
    /**
     * @var string
     */
    public $regionCode;
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid'
    ];

    public $arrayFields = '"NAME","ADRESTEXT","INN","OGRN","KPP","DTREG","DTEND","T"';

    public function __construct($check)
    {
        $this->checkFounders = $check['founders'];
        $this->checkManager = $check['manager'];
    }

    public function response($json, $idIrbisRequest)
    {
        $item = json_decode($json, true);
        $org = new \common\models\IrbisOrg();
        $this->regionCode = $item['address']['region_code'];
        $org->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'inn' => $item['inn'],
            'ogrn' => $item['ogrn'],
            'kpp' => $item['kpp'],
            'full_name' => $item['full_name'],
            'short_name' => $item['short_name'],
            'date_born' => $item['born']['ogrn_date'],
            'address' => $item['address']['full_address'] . ' ' . $item['address']['house'],
            'capital' => $item['capital'],
            'reg_org_name' => $item['reg_org_name'],
            'email' => $item['email'],
            'death_date' => $item['death']['date'],
            'death_cause' => $item['death']['cause'],
        ]);
        if (!$org->save()) {
            throw new Exception("Не удалось сохранить в таблицу irbis_org");
        }
        $this->foundersFlOrg($item['founders']['fl'], $idIrbisRequest);
        $this->foundersLegalOrg($item['founders']['legal_rus'], $idIrbisRequest);
        $this->managerFlOrg($item['manager_fl'], $idIrbisRequest);
        $this->saveOkvedOrg($item, $idIrbisRequest);
    }

    /**
     * занесение данных в founder_fl_org по учередителям (людям)
     * @param $json
     * @throws Exception
     */
    protected function foundersFlOrg($json, $idIrbisRequest)
    {
        if (!empty($json)) {
            foreach ($json as $item) {
                if (!empty($item['face'])) {
                    $founders = new FoundersFlOrg();
                    $founders->setAttributes([
                        'irbis_request_id' => $idIrbisRequest,
                        'fio' => $item['face']['last_name'] . ' ' . $item['face']['first_name'] . ' ' . $item['face']['second_name'],
                        'inn' => $item['face']['inn'],
                        'grn_date' => $item['face']['grn_date'],
                        'share_capital' => $item['share_capital'],
                    ]);
                    if (!$founders->save()) {
                        throw  new Exception('Не удалось сохранить в таблицу founders_fl_org');
                    }
                    if ($this->checkFounders) {
                        // Запрос по учередителю
                        $irbis = new IrbisPeople([
                            'field' => [
                                'firstName' => $item['face']['first_name'],
                                'secondName' => $item['face']['second_name'],
                                'lastName' => $item['face']['last_name'],
                                'inn' => $item['face']['inn'],
                                'org_id' => $idIrbisRequest,
                                'birthDate' => '',
                                'serPassport' => '',
                                'numPassport' => '',
                                'regions' => $this->regionCode,
                                'founders_id' => $founders->id,
                            ]
                        ]);
                        $result = $irbis->getUuid();
                        $irbis->individual($result);
                    }
                }
            }
        }
    }

    /**
     * Занесение данных в manager_fl_org
     * @param $json
     * @throws Exception
     */
    protected function managerFlOrg($json, $idIrbisRequest)
    {
        if (!empty($json)) {
            foreach ($json as $item) {
                if (!empty($item['fl'])) {
                    $phones = [];
                    if (!empty($item['phones'])) {
                        foreach ($item['phones'] as $phone) {
                            $phones[] = $phone;
                        }
                    }
                    $manager = new ManagerFlOrg();
                    $manager->setAttributes([
                        'irbis_request_id' => $idIrbisRequest,
                        'fio' => $item['fl']['last_name'] . ' ' . $item['fl']['first_name'] . ' ' . $item['fl']['second_name'],
                        'inn' => $item['fl']['inn'],
                        'grn_date' => $item['fl']['grn_date'],
                        'role_name' => $item['role_name'],
                        'role_type' => $item['role_type'],
                        'phones' => implode(',', $phones),
                    ]);
                    if (!$manager->save()) {
                        throw  new Exception('Не удалось сохранить в таблицу managers_fl_org');
                    }
                    if ($this->checkManager) {
                        // Запрос по учередителю
                        $irbis = new IrbisPeople([
                            'field' => [
                                'firstName' => $item['fl']['first_name'],
                                'secondName' => $item['fl']['second_name'],
                                'lastName' => $item['fl']['last_name'],
                                'inn' => $item['fl']['inn'],
                                'org_id' => $idIrbisRequest,
                                'birthDate' => '',
                                'serPassport' => '',
                                'numPassport' => '',
                                'regions' => $this->regionCode,
                                'manager_id' => $manager->id,
                            ]
                        ]);
                        $result = $irbis->getUuid();
                        $irbis->individual($result);
                    }
                }
            }
        }
    }

    /**
     * занесение данных в founder_fl_org по учередителям (компаниям)
     * @param $json
     * @throws Exception
     */
    protected function foundersLegalOrg($json, $idIrbisRequest)
    {
        if (!empty($json)) {
            foreach ($json as $item) {
                if (!empty($item['face'])) {
                    $founders = new FoundersLegalOrg();
                    $founders->setAttributes([
                        'irbis_request_id' => $idIrbisRequest,
                        'ogrn' => $item['face']['ogrn'],
                        'inn' => $item['face']['inn'],
                        'name' => $item['face']['full_name'],
                        'grn_date' => $item['face']['grn_date'],
                        'share_capital' => $item['share_capital'],
                    ]);
                    if (!$founders->save()) {
                        throw  new Exception('Не удалось сохранить в таблицу founders_legal_org');
                    }
                }
            }
        }
    }

    /**
     * занесение данных в okved_org
     * @param $json
     * @throws Exception
     */
    protected function saveOkvedOrg($json, $idIrbisRequest)
    {
        if (!empty($json['base_okved'])) {
            $okved = new OkvedOrg();
            $okved->setAttributes([
                'irbis_request_id' => $idIrbisRequest,
                'code' => $json['base_okved']['code'],
                'name' => $json['base_okved']['name'],
                'type' => OkvedOrg::BASE,
            ]);
            if (!$okved->save()) {
                throw  new Exception('Не удалось сохранить в таблицу okved_org');
            }
        }
        if (!empty($json['additional_okved'])) {
            foreach ($json['additional_okved'] as $item) {
                $okved = new OkvedOrg();
                $okved->setAttributes([
                    'irbis_request_id' => $idIrbisRequest,
                    'code' => $item['code'],
                    'name' => $item['name'],
                    'type' => OkvedOrg::ADDITIONAL,
                ]);
                if (!$okved->save()) {
                    throw  new Exception('Не удалось сохранить в таблицу okved_org');
                }
            }
        }
    }
}
