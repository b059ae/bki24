<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\BalanceOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class BalanceOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-balance.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'preview',
    ];

    public function response($json, $idIrbisRequest)
    {
        foreach ($json as $item) {
            $balance = new BalanceOrg();
            $balance->setAttributes([
                'irbis_request_id' => $idIrbisRequest,
                'balance' => $item['balance'],
                'sales' => $item['sales'],
                'net_profit' => $item['net_profit'],
                'year' => $item['year'],
            ]);
            if (!$balance->save()) {
                throw new Exception();
            }
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'balance' => $item['balance'],
            'sales' => $item['sales'],
            'net_profit' => $item['net_profit'],
            'year' => $item['year'],
        ];
        $balance = BalanceOrg::findOne(['irbis_request_id' => $idIrbisRequest, 'year' => $item['year']]);
        if ($balance) {
            $balance->updateAttributes($data);
        } else {
            $balance = new BalanceOrg();
            $balance->setAttributes($data);
        }
        if (!$balance->save()) {
            throw new Exception();
        }
    }
}