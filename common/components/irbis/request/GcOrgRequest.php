<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\GcOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class GcOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-gc.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid',
    ];

    public $arrayFields = '"customer_name","customer_inn","customer_kpp","date","sum","key","mobCol"';

    public function response($item, $idIrbisRequest)
    {
        $gc = new GcOrg();
        $gc->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'customer_name' => $item['cell'][0],
            'customer_inn' => $item['cell'][1],
            'customer_kpp' => $item['cell'][2],
            'date' => $item['cell'][3],
            'sum' => $item['cell'][4],
            'mob_col' => $item['cell'][6],
        ]);
        if (!$gc->save()) {
            throw new Exception('не удалось добавить данные в таблицу gc_org');
        }
    }
}