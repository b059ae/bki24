<?php
namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\ArchiveJudgePeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class JudgeArchivePeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-judge-archive.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event'=>'result',
        'view'=>'jqgrid',
        'id'=>'1',
    ];
    public $arrayFields = '"head","body"';

    public function response($item, $idIrbisRequest)
    {
        $judge = new ArchiveJudgePeople();
        $judge->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'head' => $item['cell'][0],
        ]);
        if (!$judge->save()) {
            throw new Exception();
        }
    }
}