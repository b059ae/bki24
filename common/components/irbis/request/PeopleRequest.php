<?php
namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\UuidRequest;
use common\models\IrbisRequest;
use Exception;
use Yii;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class PeopleRequest implements UuidRequest
{
    public $url = 'https://ir-bis.org/ru/base/-/services/people-check.json';

    public function getUrl(){
        return $this->url;
    }

    public function getParams($field){
        return [
            'PeopleQuery.LastName'=> $field['lastName'] ,
            'PeopleQuery.FirstName' => $field['firstName'] ,
            'PeopleQuery.SecondName' => $field['secondName'] ,
            'PeopleQuery.BirthDate' => $field['birthDate'] ,
            'PeopleQuery.PassportSeries' => $field['serPassport'] ,
            'PeopleQuery.PassportNumber' => $field['numPassport'] ,
            'PeopleQuery.INN' => $field['inn'] ,
            'regions' => '[' . $field['regions'] . ']',
        ];
    }

    /**
     * @param string $uuid
     * @param array $fields
     * @return integer
     * @throws Exception
     */
    public function saveRequest($uuid, $fields)
    {
        $irbis = new \common\models\IrbisRequest();
        $irbis->setAttributes([
            'request_type' => IrbisRequest::IRBIS_REQUEST_PEOPLE,
            'uuid' => $uuid,
            'request_id' => $fields['request_id'],
            'request_data' => implode(', ', $fields),
            'first_name' => !empty($fields['firstName']) ? $fields['firstName'] : null,
            'last_name' => !empty($fields['lastName']) ? $fields['lastName'] : null,
            'second_name' => !empty($fields['secondName']) ? $fields['secondName'] : null,
            'birth_date' => !empty($fields['birthDate']) ? Yii::$app->formatter->asDate($fields['birthDate'], 'php:Y-m-d') : null,
            'region' => !empty($fields['regions']) ? $fields['regions'] : null,
            'ser_passport' => !empty($fields['serPassport']) ? $fields['serPassport'] : null,
            'num_passport' => !empty($fields['numPassport']) ? $fields['numPassport'] : null,
            'inn' => !empty($fields['inn']) ? $fields['inn'] : null,
        ]);
        if (!$irbis->save()) {
            throw new Exception("Не удалось сохранить в таблицу Irbis");
        }
        return $irbis->getAttribute('id');
    }
}