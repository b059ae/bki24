<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\ArbitrOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class ArbitrNameOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-arbitr.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result_name',
        'view' => 'jqgrid',
    ];
    public $arrayFields = '"case_number","name","address_val","role","court_name_val","case_date","case_id","mobCol"';


    public function response($item, $idIrbisRequest)
    {
        $arbitr = new ArbitrOrg();
        $arbitr->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'type_request' => ArbitrOrg::TYPE_REQUEST_NAME,
            'address' => $item['cell'][2],
            'member' => $item['cell'][1],
            'judge' => $item['cell'][4],
            'type_member' => $item['cell'][3],
            'date' => $item['cell'][5],
            'url' => $item['cell'][6],
        ]);
        if (!$arbitr->save()) {
            throw new Exception();
        }
    }
}