<?php
namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\FsspPeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class FsspPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-fssp.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event=data'
    ];

    public function response($item, $idIrbisRequest)
    {
        $fssp = new FsspPeople();
        $fssp->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['fio'],
            'code' => $item['ip'],
            'fssp_department' => $item['rosp'],
            'subject' => $item['type_ip'],
            'date_end' => $item['end_cause'],
            'sum' => $item['summ'],
        ]);
        if (!$fssp->save()) {
            throw new Exception();
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['fio'],
            'code' => $item['ip'],
            'fssp_department' => $item['rosp'],
            'subject' => $item['type_ip'],
            'date_end' => $item['end_cause'],
            'sum' => $item['summ'],
        ];
        $fssp = FsspPeople::findOne(['irbis_request_id' => $idIrbisRequest, 'code' => $item['ip']]);
        if ($fssp){
            $fssp->updateAttributes($data);
        } else {
            $fssp = new FsspPeople();
            $fssp->setAttributes($data);
        }
        if (!$fssp->save()) {
            throw new Exception();
        }
    }
}