<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\ArbitrOrg;
use common\models\ArbitrPeople;
use common\models\BankrotPeople;
use common\models\FsspPeople;
use common\models\JudgePeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class BankrotPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-bankrot.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result_people',
        'view' => 'jqgrid',
        'case_type' => 'Б',
    ];
    public $arrayFields = '"case_id","case_number","name","court_name_val","case_date"';

    public function response($item, $idIrbisRequest)
    {
        $bankrot = new BankrotPeople();
        $bankrot->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'bankrot' => $item['cell'][2],
            'judge' => $item['cell'][3],
            'code' => $item['cell'][1],
            'date_bankrot' => $item['cell'][4],
        ]);
        if (!$bankrot->save()) {
            throw new Exception();
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'bankrot' => $item['cell'][2],
            'judge' => $item['cell'][3],
            'code' => $item['cell'][1],
            'date_bankrot' => $item['cell'][4],
        ];
        $bankrot = BankrotPeople::findOne(['irbis_request_id' => $idIrbisRequest, 'code' => $item['cell'][1]]);
        if ($bankrot) {
            $bankrot->updateAttributes($data);
        } else {
            $bankrot = new BankrotPeople();
            $bankrot->setAttributes($data);
        }
        if (!$bankrot->save()) {
            throw new Exception();
        }
    }
}