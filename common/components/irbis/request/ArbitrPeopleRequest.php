<?php
namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\ArbitrPeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class ArbitrPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-arbitr.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event'=>'data',
        'search_type'=>'inn',
    ];
//    public $arrayFields = '"case_id","case_number","name","role","court_name_val","case_date"';

//    public function response($item, $idIrbisRequest)
//    {
//        $arbitr = new ArbitrPeople();
//        $arbitr->setAttributes([
//            'irbis_request_id' => $idIrbisRequest,
//            'url' => $item['cell'][0],
//            'name' => $item['cell'][1],
//            'member' => $item['cell'][2],
//            'type_member' => $item['cell'][3],
//            'judge' => $item['cell'][4],
//            'date' => $item['cell'][5],
//        ]);
//        if (!$arbitr->save()) {
//            throw new Exception();
//        }
//    }

    public function response($item, $idIrbisRequest)
    {
        $arbitr = new ArbitrPeople();
        $arbitr->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'url' => $item['case_id'],
            'name' => $item['case_number'],
            'member' => $item['name'],
            'type_member' => $item['role'],
            'judge' => $item['court_name_val'],
            'date' => $item['case_date'],
        ]);
        if (!$arbitr->save()) {
            throw new Exception();
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'url' => $item['case_id'],
            'name' => $item['case_number'],
            'member' => $item['name'],
            'type_member' => $item['role'],
            'judge' => $item['court_name_val'],
            'date' => $item['case_date'],
        ];
        $arbitr = ArbitrPeople::findOne(['irbis_request_id' => $idIrbisRequest, 'name' => $item['case_number']]);
        if ($arbitr){
            $arbitr->updateAttributes($data);
        } else {
            $arbitr = new ArbitrPeople();
            $arbitr->setAttributes($data);
        }
        if (!$arbitr->save()) {
            throw new Exception();
        }
    }

}