<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\UuidRequest;
use common\models\IrbisRequest;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class OrgRequest implements UuidRequest
{
    public $url = 'https://ir-bis.org/ru/base/-/services/org-check.json';

    public function getUrl()
    {
        return $this->url;
    }

    public function getParams($field)
    {
        return [
            'inn' => $field['inn'],
        ];
    }

    /**
     * @param string $uuid
     * @param array $fields
     * @return integer
     * @throws Exception
     */
    public function saveRequest($uuid, $fields)
    {
        $irbis = new \common\models\IrbisRequest();
        $irbis->setAttributes([
            'request_type' => IrbisRequest::IRBIS_REQUEST_LEGAL,
            'uuid' => $uuid,
            'request_id' => $fields['request_id'],
            'request_data' => implode(', ', $fields),
            'inn' => !empty($fields['inn']) ? $fields['inn'] : null,
            'check_founders' => !empty($fields['founders']) ? $fields['founders'] : false,
            'check_manager' => !empty($fields['manager']) ? $fields['manager'] : false,
        ]);
        if (!$irbis->save()) {
            throw new Exception("Не удалось сохранить в таблицу irbis_request");
        }
        return $irbis->getAttribute('id');
    }
}