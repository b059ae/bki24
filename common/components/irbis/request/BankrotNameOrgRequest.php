<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\BankrotOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class BankrotNameOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-bankrot.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result_name',
        'view' => 'jqgrid',
        'case_type' => 'Б'
    ];

    public $arrayFields = '"case_number","name","address_val","court_name_val","case_date","case_id"';

    public function response($item, $idIrbisRequest)
    {
        $bankrot = new BankrotOrg();
        $bankrot->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'type_request' => BankrotOrg::TYPE_REQUEST_NAME,
            'case_number' => $item['cell'][0],
            'bankrot' => $item['cell'][1],
            'address' => $item['cell'][2],
            'judge' => $item['cell'][3],
            'date_bankrot' => $item['cell'][4],
            'url' => $item['cell'][5],
        ]);
        if (!$bankrot->save()) {
            throw new Exception();
        }
    }
}