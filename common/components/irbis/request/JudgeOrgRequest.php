<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\JudgeOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class JudgeOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-judge.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'type-preview',
    ];

    public function response($json, $idIrbisRequest)
    {
        foreach ($json as $item) {
            $judge = new JudgeOrg();
            $judge->setAttributes([
                'irbis_request_id' => $idIrbisRequest,
                'type' => $item['type'],
                'count' => $item['count'],
            ]);
            if (!$judge->save()) {
                throw new Exception('Не удалось сохранить данные в таблицу judge_org');
            }
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'type' => $item['type'],
            'count' => $item['count'],
        ];
        $judge = JudgeOrg::findOne(['irbis_request_id' => $idIrbisRequest, 'type' => $item['type']]);
        if ($judge) {
            $judge->updateAttributes($data);
        } else {
            $judge = new JudgeOrg();
            $judge->setAttributes($data);
        }
        if (!$judge->save()) {
            throw new Exception();
        }
    }
}