<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\GcPeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class GcPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-gc-archive.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid',
    ];
    public $arrayFields = '"id","result","customer","date","sum","status","mobCol"';

    public function response($item, $idIrbisRequest)
    {
        $gc = new GcPeople();
        $gc->setAttributes([
//            "id", "result", "customer", "date", "sum", "status", "mobCol",
            'irbis_request_id' => $idIrbisRequest,
            'result' => $item['cell'][1],
            'customer' => $item['cell'][2],
            'date' => $item['cell'][3],
            'sum' => $item['cell'][4],
            'status' => $item['cell'][5],
            'mobCol' => $item['cell'][6],
        ]);
        if (!$gc->save()) {
            throw new Exception();
        }
    }
}