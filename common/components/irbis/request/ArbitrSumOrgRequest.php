<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\ArbitrSum;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class ArbitrSumOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-arbitr.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'preview',
    ];


    public function response($items, $idIrbisRequest)
    {
        foreach ($items as $item) {
            $arbitr = new ArbitrSum();
            $arbitr->setAttributes([
                'irbis_request_id' => $idIrbisRequest,
                'count' => $item['count'],
                'sum' => $item['sum'],
                'type' => $item['type'],
                'year' => $item['year'],
            ]);
            if (!$arbitr->save()) {
                throw new Exception();
            }
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'count' => $item['count'],
            'sum' => $item['sum'],
            'type' => $item['type'],
            'year' => $item['year'],
        ];
        $arbitr = ArbitrSum::findOne(['irbis_request_id' => $idIrbisRequest, 'type' => $item['type']]);
        if ($arbitr) {
            $arbitr->updateAttributes($data);
        } else {
            $arbitr = new ArbitrSum();
            $arbitr->setAttributes($data);
        }
        if (!$arbitr->save()) {
            throw new Exception();
        }
    }
}