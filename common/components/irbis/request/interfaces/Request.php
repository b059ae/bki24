<?php
namespace common\components\irbis\request\interfaces;
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 31.08.17
 * Time: 10:07
 */

interface Request
{
    /**
     * Формирование URL для запроса
     * @param string $uuid UUID запроса в Ирбисе
     * @return string
     */
    public function getUrl($uuid);

    /**
     * Возвращает параметры запроса
     * @return array
     */
    public function getParams();

    /**
     * Сохранение ответа Ирбис
     * @param array $item
     * @param integer $idIrbisRequest
     */
    public function response($item, $idIrbisRequest);

    /**
     * Обновление ответа Ирбис
     * @param array $item
     * @param integer $idIrbisRequest
     */
    public function update($item, $idIrbisRequest);
}