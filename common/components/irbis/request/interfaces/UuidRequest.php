<?php
namespace common\components\irbis\request\interfaces;
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 31.08.17
 * Time: 10:07
 */

interface UuidRequest
{
    /**
     * @param string $uuid
     * @param array $fields
     * @return integer ID запроса из таблицы irbis_request
     */
    public function saveRequest($uuid, $fields);

    /**
     * Формирование URL для запроса
     * @return string
     */
    public function getUrl();

    /**
     * Возвращает параметры запроса
     * @param array $fields Передаваемые поля
     * @return array
     */
    public function getParams($fields);
}