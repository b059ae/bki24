<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\IrbisPeople;
use common\models\TerroristPeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class TerroristPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-terrorist.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid',
    ];
    public $arrayFields = '"fio","birth_date","birth_place","mobCol"';

    public function response($item, $idIrbisRequest)
    {
        $people = new TerroristPeople();
        $people->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'birth_date' => $item['cell'][1],
            'birth_place' => $item['cell'][2],

        ]);
        if (!$people->save()) {
            throw new Exception("Не удалось сохранить в таблицу terrorist_people");
        }
        $people = IrbisPeople::findOne(['irbis_request_id' => $idIrbisRequest]);
        $people->updateAttributes([
            'terrorist' => IrbisPeople::TERRORIST_YES,
        ]);
        if (!$people->save()) {
            throw new Exception("Не удалось сохранить данные ФМС в таблицу irbis_people");
        }
    }
}