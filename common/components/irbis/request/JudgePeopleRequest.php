<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\JudgePeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class JudgePeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-judge.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'role-data',
        'filter0=full',
    ];

    public function response($item, $idIrbisRequest)
    {
        $judge = new JudgePeople();
        $judge->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'date' => $item['header']['start_date'],
            'judge' => $item['header']['judge'],
            'type_cause' => $item['header']['process_type'],
            'case_number' => $item['header']['case_number'],
            'end_date' => $item['header']['end_date'],
            'resolution' => $item['header']['resolution'],
            'court_name' => $item['header']['court_name'],
            'description' => json_encode($item['case_progress'], JSON_UNESCAPED_UNICODE),
            'role' => $item['faces'][0]['role'],
        ]);
        if (!$judge->save()) {
            throw new Exception();
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'date' => $item['header']['start_date'],
            'judge' => $item['header']['judge'],
            'type_cause' => $item['header']['process_type'],
            'case_number' => $item['header']['case_number'],
            'end_date' => $item['header']['end_date'],
            'resolution' => $item['header']['resolution'],
            'court_name' => $item['header']['court_name'],
            'description' => json_encode($item['case_progress'], JSON_UNESCAPED_UNICODE),
            'role' => $item['faces'][0]['role'],
        ];
        $judge = JudgePeople::findOne([
            'irbis_request_id' => $idIrbisRequest,
            'case_number' => $item['header']['case_number']
        ]);
        if ($judge) {
            $judge->updateAttributes($data);
        } else {
            $judge = new JudgePeople();
            $judge->setAttributes($data);
        }
        if (!$judge->save()) {
            throw new Exception();
        }
    }
}