<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\FsspOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class FsspOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-fssp.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid',
    ];

    public $arrayFields = '"name_debtor","address_debtor","inn","ogrn","number_enforcement_proceeding","date_institution_proceeding","total_number_proceeding","document_type","date_executive_ducument ","number_executive_document","object_executive_documents","object_execution","amount","departaments","address_departaments","mobCol"';


    public function response($item, $idIrbisRequest)
    {
        $fssp = new FsspOrg();
        $fssp->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'address' => $item['cell'][1],
            'code_ip' => $item['cell'][4],
            'date_ip' => $item['cell'][5],
            'subject' => $item['cell'][7],
            'sum' => $item['cell'][12],
            'fssp_department' => $item['cell'][13],
        ]);
        if (!$fssp->save()) {
            throw new Exception();
        }
    }

    public function update($item, $idIrbisRequest)
    {
        $data = [
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'address' => $item['cell'][1],
            'code_ip' => $item['cell'][4],
            'date_ip' => $item['cell'][5],
            'subject' => $item['cell'][7],
            'sum' => $item['cell'][12],
            'fssp_department' => $item['cell'][13],
        ];
        $fssp = FsspOrg::findOne(['irbis_request_id' => $idIrbisRequest, 'name' => $item['cell'][0]]);
        if ($fssp) {
            $fssp->updateAttributes($data);
        } else {
            $fssp = new FsspOrg();
            $fssp->setAttributes($data);
        }
        if (!$fssp->save()) {
            throw new Exception();
        }
    }
}