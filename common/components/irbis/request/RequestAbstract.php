<?php
namespace common\components\irbis\request;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:52
 */
abstract class RequestAbstract
{
    /**
     * @var string Часть url запроса
     */
    public $url = 'https://ir-bis.org/ru/base/-/services/report/';
    /**
     * @var int Страница для вывода в запросе
     */
    public $page = 1;
    /**
     * @var int Количество строк для вывода
     */
    public $rows = 100000;
    /**
     * @var string Тип запроса
     */
    public $type;
    /**
     * @var array поля запроса
     */
    public $field;
    /**
     * @var string
     */
    public $arrayFields = '';

    public function getUrl($uuid = ''){
        return $this->url . $uuid . $this->type;
    }

    public function getParams(){
        return array_merge(
            [
                'fields'=>'['.$this->arrayFields. ']',
                'page'=>$this->page,
                'rows'=>$this->rows,
            ],
            $this->field
        );
    }
    public function update($item, $idIrbisRequest){}

}