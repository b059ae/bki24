<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\InterestPeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class InterestPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-interest.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid'
    ];
    public $arrayFields = '"user","query","time"';

    public function response($item, $idIrbisRequest)
    {
        $interest = new InterestPeople();
        $interest->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'request' => $item['cell'][1],
            'date_request' => $item['cell'][2]
        ]);
        if (!$interest->save()) {
            throw new Exception();
        }
    }
}