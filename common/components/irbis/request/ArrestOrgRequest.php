<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\ArrestOrg;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class ArrestOrgRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/org-arrest.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid',
    ];
    public $arrayFields = '"number","res_date","nalog_code","bik","bank","reg_date","mobCol"';

    public function response($item, $idIrbisRequest)
    {
        $arrest = new ArrestOrg();
        $arrest->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'number' => $item['cell'][0],
            'res_date' => $item['cell'][1],
            'nalog_code' => $item['cell'][2],
            'bik' => $item['cell'][3],
            'bank' => $item['cell'][4],
            'reg_date' => $item['cell'][5],
            'mobCol' => $item['cell'][6],
        ]);
        if (!$arrest->save()) {
            throw new Exception('не удалось добавить данные в таблицу arrest_org');
        }
    }
}