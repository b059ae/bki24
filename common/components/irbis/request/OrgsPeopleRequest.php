<?php

namespace common\components\irbis\request;

use common\components\irbis\request\interfaces\Request;
use common\models\OrganisationPeople;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 10:17
 */
class OrgsPeopleRequest extends RequestAbstract implements Request
{
    /**
     * @var string Тип запроса
     */
    public $type = '/people-orgs.json';
    /**
     * @var array поля запроса
     */
    public $field = [
        'event' => 'result',
        'view' => 'jqgrid',
    ];
    public $arrayFields = '"name","inn","ogrn","address","role","archived","report","mobCol"';

    public function response($item, $idIrbisRequest)
    {
        $org = new OrganisationPeople();
        $org->setAttributes([
            'irbis_request_id' => $idIrbisRequest,
            'name' => $item['cell'][0],
            'inn' => $item['cell'][1],
            'ogrn' => $item['cell'][2],
            'address' => $item['cell'][3],
            'position' => $item['cell'][4],
            'status' => $item['cell'][5],
        ]);
        if (!$org->save()) {
            throw new Exception();
        }
    }
}