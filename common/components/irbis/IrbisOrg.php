<?php
namespace common\components\irbis;

use common\components\irbis\models\UuidResult;
use common\components\irbis\request\ArbitrInnOrgRequest;
use common\components\irbis\request\ArbitrSumOrgRequest;
use common\components\irbis\request\ArrestOrgRequest;
use common\components\irbis\request\BalanceOrgRequest;
use common\components\irbis\request\BankrotInnOrgRequest;
use common\components\irbis\request\BankrotNameOrgRequest;
use common\components\irbis\request\EgrulOrgRequest;
use common\components\irbis\request\FsspOrgRequest;
use common\components\irbis\request\GcOrgRequest;
use common\components\irbis\request\JudgeOrgRequest;
use common\components\irbis\request\OrgRequest;
use yii\base\Object;

class IrbisOrg extends Object
{
    /**
     * @var array Поля для запроса полученные из формы
     */
    public $field = [];

    /**
     * Делает запрос в Ирбис и возвращает его UUID
     * @return UuidResult
     */
    public function getUuid(){
        return \Yii::$app->irbis->getUuid(new OrgRequest(), $this->field);
    }
    /**
     * Запрос по юридическому лицу
     */
    public function legal(UuidResult $result)
    {
//            \Yii::$app->irbis->call(new ArbitrNameOrgRequest(), $result['uuid'], $result['irbis_request_id']);
        \Yii::$app->irbis->call(new JudgeOrgRequest(), $result);
        \Yii::$app->irbis->call(new ArbitrSumOrgRequest(), $result);
        \Yii::$app->irbis->call(new ArbitrInnOrgRequest(), $result);
        \Yii::$app->irbis->call(new BalanceOrgRequest(), $result);
        \Yii::$app->irbis->call(new BankrotInnOrgRequest(), $result);
        \Yii::$app->irbis->call(new BankrotNameOrgRequest(), $result);
        \Yii::$app->irbis->call(new ArrestOrgRequest(), $result);
        \Yii::$app->irbis->call(new FsspOrgRequest(), $result);
        \Yii::$app->irbis->call(new GcOrgRequest(), $result);
        \Yii::$app->irbis->call(new EgrulOrgRequest($this->field), $result);
        $badProvider = new BadProvider($result);
        $badProvider->getAllBadProvider($this->field['inn']);
        return $result->getIrbisRequestId();
    }

    public function updateFields(UuidResult $result){
        \Yii::$app->irbis->update(new JudgeOrgRequest(), $result);
        \Yii::$app->irbis->update(new ArbitrSumOrgRequest(), $result);
        \Yii::$app->irbis->update(new ArbitrInnOrgRequest(), $result);
        \Yii::$app->irbis->update(new BankrotInnOrgRequest(), $result);
        \Yii::$app->irbis->update(new FsspOrgRequest(), $result);
        \Yii::$app->irbis->update(new BalanceOrgRequest(), $result);
    }

//    //TODO Нужны данные
//    /**
//     * Запрос по данным залогов
//     * @param $queryUID string Код запроса
//     */
//    protected function pledgeOrg($queryUID)
//    {
//        $url = 'https://ir-bis.org/ru/base/-/services/report/' . $queryUID . '/org-pledge.json';
//        $fields = 'token=' . $this->token .
//            '&event=preview' .
//            '&page=' . $this->page . '&rows=' . $this->rows;
//        $str = $this->getJson($url, $fields);
//        $json = json_decode($str, true);
//        echo "--------------Залог--------------";
//        var_dump($json);
//    }
}