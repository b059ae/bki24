<?php
/**
 * Сохранение и чтение файла с уникальным именем в директорию год-месяц
 */

namespace common\components;


use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidParamException;

class File extends Component implements FileInterface
{

    /**
     * @var string Название папки для хранения файлов
     */
    public $folder;

    public function init()
    {
        parent::init();

        if(empty($this->folder)) {
            throw new InvalidParamException('Не указана папка для хранения файлов');
        }
        $this->folder = \Yii::getAlias($this->folder);
        if (!@mkdir($this->folder, 0755) && !is_dir($this->folder)) {
            throw new Exception('Не удалось создать папку '.$this->folder);
        }
    }

    /**
     * Сохранение файла
     * @param mixed $content Содержимое файла
     * @param string $ext Расширение файла
     * @param bool $shortName Возвращать короткое имя файла
     * @return string
     */
    public function save($content, $ext, $shortName = true){
        $filename = $this->uniqueFilename($ext);
        if (file_put_contents($filename['full'], $content) === false){
            throw new Exception('Не удалось сохранить файл '.$filename['full']);
        }
        return $shortName
            ? $filename['short']
            : $filename['full'];
    }

    /**
     * Читает файл
     * @param string $filename
     * @return mixed
     */
    public function read($filename){

        return file_get_contents($this->folder.'/'.$filename);
    }

    /**
     * Читает файлы из zip-архива
     * @param string $zipname
     * @param string $filename
     * @return mixed
     * @throws Exception
     */
    public function readFromZip($filename)
    {
        $result = [];
        $z = zip_open($this->folder.'/'.$filename);
        do {
            $entry = zip_read($z);
            if ($entry){
                $entry_name = zip_entry_name($entry);
                $entry_content = zip_entry_read($entry, zip_entry_filesize($entry));

                $result[pathinfo($entry_name, PATHINFO_EXTENSION)] = $entry_content;
            }

        } while ($entry);

        return $result;
    }

    /**
     * Уникальное имя файла в папке
     * @return string
     */
    protected function uniqueFilename($ext) {
        $dir  = date('Y-m');
        $path = $this->folder .'/'. $dir;
        // Создаем отдельную папку для каждого месяца, если она еще не создана
        if (!@mkdir($path, 0755) && !is_dir($path)) {
            throw new Exception('Не удалось создать папку '.$path);
        }
        //Уникальное имя файла
        while (true) {
            $filename = uniqid(time(), true).'.'.$ext;
            if (!file_exists($path . '/' . $filename))
                break;
        }
        return [
            'full'=>$path . '/' . $filename,
            'short'=>$dir . '/' . $filename,
        ];
    }
}