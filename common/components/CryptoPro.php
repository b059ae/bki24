<?php

namespace common\components;
use yii\base\Component;

/**
 * Аналог КриптоПро с zip архиватором в одном флаконе.
 * Работает на готом энтузиазме, при открытом разрешении на выполнение exec.
 * Требует что бы openssl дружил с ГОСТ форматом сертификатов
 */
class CryptoPro extends Component implements CryptoProInterface
{
    /**
     * @var string Имя файла с открытым ключем
     */
    public $keyPublic;
    /**
     * @var string Имя файла с закрытым ключем
     */
    public $keyPrivate;
    /**
     * @var string Имя файла с открытого ключа получателя
     */
    public $keyRecipient;
    /**
     * @var string Путь до исполняемого файла OpenSSL
     */
    public $pathToOpenSSL = 'openssl';

    /**
     * Выполнение openssl комманды на подписание файла
     *
     * @param string $filePath Путь к исходному файлу(который подписывается)
     * @param string $signPath Путь к файлу подписи
     * @return bool
     */
    public function sign($filePath, $signPath)
    {
        exec("{$this->pathToOpenSSL} cms -sign -inkey {$this->keyPrivate} -signer {$this->keyPublic} -in {$filePath} -out {$signPath} -engine gost -gost89 -outform der -noattr -binary", $output);
        return empty($output);
    }

    /**
     * Выполнение openssl комманды на шифрование файла
     *
     * @param string $filePath Путь к исходному файлу(который шифруется)
     * @param string $encPath Путь к зашифрованному файлу
     * @return bool
     */
    public function encrypt($filePath, $encPath)
    {
        exec("{$this->pathToOpenSSL} cms -encrypt -in {$filePath} -engine gost -gost89 -outform DER -noattr -binary -out {$encPath} {$this->keyPublic} {$this->keyRecipient}", $output);
        return empty($output);
    }

    /**
     * Выполнение openssl комманды для снятия подписи файла
     *
     * @param string $signPath Путь к подписанному файлу
     * @param string $filePath Путь к исходному файлу
     * @return bool
     */
    public function extract($signPath, $filePath)
    {
        exec("{$this->pathToOpenSSL} smime -verify -in {$signPath} -out {$filePath} -inform der -noverify", $output);
        return empty($output);
    }
}
