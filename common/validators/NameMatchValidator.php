<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.06.17
 * Time: 14:45
 */

namespace common\validators;


use yii\validators\RegularExpressionValidator;

class NameMatchValidator extends RegularExpressionValidator
{
    public $pattern = '/^[\s\-А-Яа-яёЁ]+$/sui';
    public $message = '{attribute} может содержать только русские буквы.';
}