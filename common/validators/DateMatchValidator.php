<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.06.17
 * Time: 14:45
 */

namespace common\validators;


use yii\validators\RegularExpressionValidator;

class DateMatchValidator extends RegularExpressionValidator
{
    public $pattern = '/^[0-9]{2}.[0-9]{2}.[0-9]{4}$/s';
    public $message = 'Формат - "дд.мм.гггг". {attribute} введена некорректно.';
}