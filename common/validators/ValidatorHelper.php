<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.06.17
 * Time: 14:20
 */

namespace common\validators;


class ValidatorHelper
{
    /**
     * Список валидаторов для значений ФИО
     *
     * @param $attributes array|string
     * @return array
     */
    public static function nameValidators($attributes)
    {
        return [
            [$attributes, 'trim'],
            [$attributes, 'filter', 'filter' => 'strip_tags'],
            [$attributes, 'string', 'max' => 32],
            [$attributes, NameMatchValidator::class],
        ];
    }

    /**
     * Список валидаторов для строчных значений
     *
     * @param $attributes array|string
     * @return array
     */
    public static function stringValidators($attributes)
    {
        return [
            [$attributes, 'filter', 'filter' => 'strip_tags'],
            [$attributes, 'string', 'max' => 255],
            [$attributes, 'trim'],
        ];
    }

    /**
     * Список валидаторов для дат
     *
     * @param $attributes array|string
     * @return array
     */
    public static function dateValidators($attributes)
    {
        return [
            [$attributes, 'trim'],
            [$attributes, 'filter', 'filter' => 'strip_tags'],
            [$attributes, DateMatchValidator::class],
            [$attributes, 'string', 'min' => 10, 'max' => 10],
            [$attributes, 'date'],
        ];
    }

    /**
     * Список валидаторов для серии паспорта
     *
     * @param $attributes array|string
     * @return array
     */
    public static function passportSeriesValidators($attributes)
    {
        return [
            [$attributes, 'trim'],
            [$attributes, 'filter', 'filter' => 'strip_tags'],
            [$attributes, 'integer'],
            [$attributes, 'string', 'min' => 4, 'max' => 4],
        ];
    }

    /**
     * Список валидаторов для номера паспорта
     *
     * @param $attributes array|string
     * @return array
     */
    public static function passportNumberValidators($attributes)
    {
        return [
            [$attributes, 'trim'],
            [$attributes, 'filter', 'filter' => 'strip_tags'],
            [$attributes, 'integer'],
            [$attributes, 'string', 'min' => 6, 'max' => 6],
        ];
    }

    /**
     * Список валидаторов для ИНН
     *
     * @param $attributes array|string
     * @return array
     */
    public static function innValidators($attributes)
    {
        return [
            [$attributes, 'trim'],
            [$attributes, 'filter', 'filter' => 'strip_tags'],
            [$attributes, 'integer'],
            [$attributes, 'string', 'min' => 10, 'max' => 12],
        ];
    }

    /**
     * Список валидаторов для ОГРН
     *
     * @param $attributes array|string
     * @return array
     */
    public static function ogrnValidators($attributes)
    {
        return [
            [$attributes, 'trim'],
            [$attributes, 'filter', 'filter' => 'strip_tags'],
            [$attributes, 'integer'],
            [$attributes, 'string', 'min' => 13, 'max' => 13],
        ];
    }
}