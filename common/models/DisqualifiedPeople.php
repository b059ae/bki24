<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "disqualified_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $fio
 * @property string $birth_date
 * @property string $bornplace
 * @property string $legal_name
 * @property string $start_date_disq
 * @property string $end_date_disq
 * @property string $office
 * @property string $department
 * @property string $article
 * @property string $fio_judge
 * @property string $office_judge
 * @property  IrbisRequest $idIrbisRequest
 */
class DisqualifiedPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disqualified_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['fio', 'birth_date', 'article', 'fio_judge', 'office_judge',
            'bornplace', 'legal_name', 'start_date_disq', 'end_date_disq', 'department'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Org',
            'fio' => 'Fio',
            'birth_date' => 'Birth_date',
            'bornplace' => 'Bornplace',
            'legal_name' => 'Legal Name',
            'start_date_disq' => 'Start Date Disq',
            'end_date_disq' => 'End Date Disq',
            'office' => 'Office',
            'department' => 'Department',
            'article' => 'Article',
            'fio_judge' => 'Fio Judge',
            'office_judge' => 'Office Judge',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
