<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "interest_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $name
 * @property string $request
 * @property string $date_request
 *
 * @property IrbisRequest $idIrbisRequest
 */
class InterestPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interest_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date_request'], 'safe'],
            [['name', 'request'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_irbis_people' => 'Id Irbis People',
            'name' => 'Name',
            'request' => 'Request',
            'date_request' => 'Date Request',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
