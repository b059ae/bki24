<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "gc_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $customer_name
 * @property string $customer_inn
 * @property string $customer_kpp
 * @property string $mob_col
 * @property string $date
 * @property string $sum
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class GcOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gc_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date', 'customer_inn', 'customer_kpp','mob_col'], 'safe'],
            [['customer_name', 'sum'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Org',
            'customer_name' => 'Customer Name',
            'date' => 'Date',
            'sum' => 'Sum',
            'customer_inn' => 'Customer INN',
            'customer_kpp' => 'Customer KPP',
            'mob_col' => 'Mob Col',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
