<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "arrest_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $res_date
 * @property string $bank
 * @property string $number
 * @property string $nalog_code
 * @property string $bik
 * @property string $reg_date
 * @property string $mobCol
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class ArrestOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arrest_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['number', 'res_date', 'nalog_code', 'bik', 'bank', 'reg_date','mobCol'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Org',
            'bank' => 'Bank',
            'number' => 'Number',
            'res_date' => 'Res Date',
            'nalog_code' => 'Nalog Code',
            'bik' => 'BIK',
            'reg_date' => 'Reg Date',
            'mobCol' => 'Mob Col',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
