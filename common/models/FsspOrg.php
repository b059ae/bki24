<?php

namespace common\models;

/**
 * This is the model class for table "fssp_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $name
 * @property string $address
 * @property string $code_ip
 * @property string $date_ip
 * @property string $subject
 * @property string $sum
 * @property string $fssp_department
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class FsspOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fssp_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['name', 'address', 'code_ip', 'date_ip', 'subject', 'sum', 'fssp_department'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id запроса',
            'name' => 'Наименование должника',
            'address' => 'Адрес должника',
            'code_ip' => 'Номер ИП',
            'date_ip' => 'Дата ИП',
            'subject' => 'Предмет исполнения',
            'sum' => 'Общая сумма',
            'fssp_department' => 'Отдел ФССП',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
