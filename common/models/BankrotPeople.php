<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "bankrot_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $bankrot
 * @property string $judge
 * @property string $code
 * @property string $date_bankrot
 *
 * @property IrbisRequest $idIrbisRequest
 */
class BankrotPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bankrot_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['bankrot', 'judge', 'code', 'date_bankrot'], 'string', 'max' => 255],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis People',
            'bankrot' => 'Bankrot',
            'judge' => 'Judge',
            'code' => 'Code',
            'date_bankrot' => 'Date Bankrot',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
