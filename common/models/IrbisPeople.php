<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "irbis_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $fio
 * @property string $birth_date
 * @property integer $fms
 * @property string $terrorist
 * @property string $region
 * @property string $org_id
 * @property integer $founders_id
 * @property integer $manager_id
 *
 * @property ArbitrPeople[] $arbitrPeoples
 * @property BankrotPeople[] $bankrotPeoples
 * @property FsspPeople[] $fsspPeoples
 * @property GcPeople[] $gcPeoples
 * @property InterestPeople[] $interestPeoples
 * @property IrbisRequest $idIrbisRequest
 * @property JudgePeople[] $judgePeoples
 * @property OrganisationPeople[] $organisationPeoples
 * @property PledgePeople[] $pledgePeoples
 * @property IrbisRequest $orgId
 */
class IrbisPeople extends ActiveRecord
{
    // Террорист
    const TERRORIST_YES = 2;
    //Не террорист
    const TERRORIST_NO = 1;
    // Паспорт не действителен
    const FMS_NO_VALID = 0;
    // Паспорт в списке не действительных не значится
    const FMS_VALID = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'irbis_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id', 'fms', 'terrorist', 'org_id', 'founders_id', 'manager_id'], 'integer'],
            [['birth_date', 'fio', 'region'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Запроса',
            'org_id' => 'Id Организации',
            'fio' => 'ФИО',
            'birth_date' => 'Дата рождения',
            'fms' => 'ФМС',
            'terrorist' => 'Терририст',
            'region' => 'Регион',
            'founders_id' => 'Id Учредителя',
            'manager_id' => 'Id Руководителя',
        ];
    }


    /**
     * Наименование
     *
     * @return string
     */
    public function getName()
    {
        return $this->fio;
    }

    /**
     * Основной идентификатор
     *
     * @return string
     */
    public function getMainNumber()
    {
        return $this->birth_date;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }

    /**
     * Сведения из ФМС
     * @return string
     */
    public function getFms()
    {
        if ($this->fms === self::FMS_NO_VALID){
            return 'Паспорт числится в списке не действительных';
        }
        return 'Паспорт действителен';
    }

    /**
     * Дата рождения
     * @return string
     */
    public function getBirthDate()
    {
        if ($this->birth_date) {
            return Yii::$app->formatter->asDate($this->birth_date, 'php:d.m.Y');
        }
        return false;
    }

    /**
     * Список террористов
     * @return string
     */
    public function getTerrorist()
    {
        if ($this->terrorist == self::TERRORIST_YES){
            return 'Числится в списке террористов';
        }
        return 'В списке террористов отсутствует';
    }

    /**
     * Организация по которой получены данные физ лица
     * @return bool|string
     */
    public function getOrg()
    {
        if ($this->manager_id) {
            $position = ManagerFlOrg::findOne(['id' => $this->manager_id]);
            if ($this->org_id) {
                $org = IrbisOrg::findOne(['irbis_request_id' => $this->org_id]);
                return $org->short_name . ' - ' . $position->role_name;
            }
        }
        return false;
    }

    /**
     * Номер телефона руководителя
     * @return bool|string
     */
    public function getPhone()
    {
        if ($this->manager_id) {
            $position = ManagerFlOrg::findOne(['id' => $this->manager_id]);
            if ($position->phones) {
                return $position->phones;
            }
        }
        return false;
    }

    /**
     * Регион
     * @return bool|string
     */
    public function getRegion()
    {
        if ($this->region){
            $region = Regions::findOne(['code' => $this->region]);
            return $region->name;
        }
        return null;
    }
}
