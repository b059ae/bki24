<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "irbis_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $inn
 * @property string $ogrn
 * @property string $kpp
 * @property string $full_name
 * @property string $short_name
 * @property string $date_born
 * @property string $address
 * @property string $capital
 * @property string $reg_org_name
 * @property string $email
 * @property string $registry_type
 * @property string $death_date
 * @property string $death_cause
 *
 * @property ArbitrOrg[] $arbitrOrgs
 * @property ArrestOrg[] $arrestOrgs
 * @property BalanceOrg[] $balanceOrgs
 * @property BankrotOrg[] $bankrotOrgs
 * @property FoundersLegalOrg[] $foundersLegalOrgs
 * @property FoundersFlOrg[] $foundersFlOrgs
 * @property FsspOrg[] $fsspOrgs
 * @property GcOrg[] $gcOrgs
 * @property IrbisRequest $idIrbisRequest
 * @property ManagerFlOrg[] $managerFlOrgs
 */
class IrbisOrg extends ActiveRecord
{
    // Содержится в реестре МФО
    const REGISTRY_TYPE_MFO = 1;
    // Содержится в реестре КПК
    const REGISTRY_TYPE_KPK = 2;
    // Содержится в реестре СелКПК
    const REGISTRY_TYPE_SKPK = 3;
    // Не записи в реестре
    const REGISTRY_TYPE_NOT = 4;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'irbis_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date_born', 'death_date', 'death_cause'], 'safe'],
            [['inn', 'ogrn', 'kpp', 'full_name', 'short_name', 'address', 'capital', 'reg_org_name', 'email', 'registry_type'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Запроса',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'kpp' => 'КПП',
            'full_name' => 'Полное наименование',
            'short_name' => 'Сокращенное наименование',
            'date_born' => 'Дата регистрации',
            'address' => 'Адрес',
            'capital' => 'Капитал',
            'reg_org_name' => 'Регистрационное имя',
            'email' => 'E-mail',
            'registry_type' => 'Тип регистрации',
            'death_date'  => 'Дата закрытия',
            'death_cause' => 'Причина закрытия',
        ];
    }

    /**
     * Наименование
     *
     * @return string
     */
    public function getName()
    {
        return $this->short_name;
    }

    /**
     * Основной идентификатор
     *
     * @return string
     */
    public function getMainNumber()
    {
        return $this->inn;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }

}
