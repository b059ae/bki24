<?php

namespace common\models;

use common\models\interfaces\RequestInterface;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "request".
 *
 * @property integer $id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $birth_date
 * @property integer $passport_series
 * @property integer $passport_number
 * @property string $inn
 * @property integer $region_id
 * @property string $note
 * @property integer $status
 * @property integer $type
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $services
 *
 * @property IrbisRequest $irbisRequest
 * @property CreditScore $creditScore
 * @property User $user
 * @property Regions $region
 */
class Request extends \yii\db\ActiveRecord implements RequestInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['birth_date'], 'safe'],
            [
                [
                    'passport_series',
                    'passport_number',
                    'region_id',
                    'inn',
                    'ogrn',
                    'status',
                    'type',
                    'user_id',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ],
            [['note', 'services'], 'string'],
            [['last_name', 'first_name', 'middle_name', 'name'], 'string', 'max' => 255],
            [['inn'], 'string', 'max' => 12],
            [['ogrn'], 'string', 'max' => 13],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
            [
                ['region_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Regions::className(),
                'targetAttribute' => ['region_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'middle_name' => 'Отчество',
            'birth_date' => 'Дата рождения',
            'passport_series' => 'Серия паспорта',
            'passport_number' => 'Номер паспорта',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'name' => 'Название организации',
            'region_id' => 'Код региона',
            'note' => 'Примечание',
            'status' => 'Статус выполнения',
            'type' => 'Тип запроса (физлицо, юрлицо)',
            'user_id' => 'Пользователь из users, который сделал запрос',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'services' => 'Список услуг',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreditScore()
    {
        return $this->hasOne(CreditScore::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }

    /**
     * Название статуса
     *
     * @return string
     */
    public function getStatusName()
    {
        return ArrayHelper::getValue([
            self::STATUS_NOT_READY => "Не готов",
            self::STATUS_PARTIALLY_READY => "Частично готов",
            self::STATUS_FULL_READY => "Полностью готов",
            self::STATUS_ERROR => "Ошибка",
        ], $this->status);
    }
}
