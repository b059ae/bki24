<?php

namespace common\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "irbis_request".
 *
 * @property integer $id
 * @property string $uuid
 * @property integer $request_type
 * @property string $request_data
 * @property string $request_id
 * @property string $inn
 * @property string $first_name
 * @property string $last_name
 * @property string $second_name
 * @property string $num_passport
 * @property string $ser_passport
 * @property string $birth_date
 * @property string $region
 * @property string $check_manager
 * @property string $check_founders
 * @property integer $created_at
 *
 * @property IrbisOrg $irbisOrgs
 * @property IrbisPeople $irbisPeoples
 * @property IrbisPeople|IrbisOrg $subject
 * @property OkvedOrg[] $okvedOrgs
 * @property ArbitrOrg[] $arbitrOrgs
 * @property ArrestOrg[] $arrestOrgs
 * @property BalanceOrg[] $balanceOrgs
 * @property BankrotOrg[] $bankrotOrgs
 * @property FoundersLegalOrg[] $foundersLegalOrgs
 * @property FoundersFlOrg[] $foundersFlOrgs
 * @property FsspOrg[] $fsspOrgs
 * @property GcOrg[] $gcOrgs
 * @property IrbisRequest $idIrbisRequest
 * @property ManagerFlOrg[] $managerFlOrgs
 * @property ArbitrPeople[] $arbitrPeoples
 * @property BankrotPeople[] $bankrotPeoples
 * @property FsspPeople[] $fsspPeoples
 * @property GcPeople[] $gcPeoples
 * @property InterestPeople[] $interestPeoples
 * @property JudgePeople[] $judgePeoples
 * @property JudgeOrg[] $judgeOrgs
 * @property OrganisationPeople[] $organisationPeoples
 * @property PledgePeople[] $pledgePeoples
 * @property TerroristPeople[] $terroristPeoples
 * @property ArbitrSum[] $arbitrSum
 * @property BadContract[] $badContracts
 * @property Request $request
 */
class IrbisRequest extends ActiveRecord
{
    /**
     * Запрос по юр лицам
     */
    const IRBIS_REQUEST_LEGAL = 2;
    /**
     * Запрос по физ лицам
     */
    const IRBIS_REQUEST_PEOPLE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'irbis_request';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_type', 'request_id', 'created_at'], 'integer'],
            [
                [
                    'uuid',
                    'request_data',
                    'inn',
                    'first_name',
                    'last_name',
                    'second_name',
                    'num_passport',
                    'ser_passport',
                    'birth_date',
                    'region',
                    'check_manager',
                    'check_founders'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'Ключ',
            'request_type' => 'Тип Субъекта',
            'request_data' => 'Даные запроса',
            'inn' => 'ИНН',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'second_name' => 'Отчество',
            'num_passport' => 'Номер паспорта',
            'ser_passport' => 'Серия паспорта',
            'birth_date' => 'Дата рождения',
            'region' => 'Регион',
            'check_manager' => 'Проверить руководителя',
            'check_founders' => 'Проверить учредителей',
        ];
    }

    /**
     * Получение строки с типом лица по запросу
     *
     * @return string
     */
    public function getRequestType()
    {
        $types = self::getRequestsTypes();
        return isset($types[$this->request_type])
            ? $types[$this->request_type]
            : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        if ($this->request_type == self::IRBIS_REQUEST_LEGAL) {
            return $this->hasOne(IrbisOrg::className(), ['irbis_request_id' => 'id']);
        } else {
            return $this->hasOne(IrbisPeople::className(), ['irbis_request_id' => 'id']);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIrbisOrgs()
    {
        return $this->hasOne(IrbisOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkvedOrgs()
    {
        return $this->hasMany(OkvedOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIrbisPeoples()
    {
        return $this->hasOne(IrbisPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * Список типов субъектов по запросам
     *
     * @return array
     */
    public static function getRequestsTypes()
    {
        return [
            self::IRBIS_REQUEST_PEOPLE => 'Физ. лицо',
            self::IRBIS_REQUEST_LEGAL => 'Юр. лицо',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArbitrPeoples()
    {
        return $this->hasMany(ArbitrPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankrotPeoples()
    {
        return $this->hasMany(BankrotPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFsspPeoples()
    {
        return $this->hasMany(FsspPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGcPeoples()
    {
        return $this->hasMany(GcPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterestPeoples()
    {
        return $this->hasMany(InterestPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgePeoples()
    {
        return $this->hasMany(JudgePeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisationPeoples()
    {
        return $this->hasMany(OrganisationPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPledgePeoples()
    {
        return $this->hasMany(PledgePeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArbitrOrgs()
    {
        return $this->hasMany(ArbitrOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArbitrSum()
    {
        return $this->hasMany(ArbitrSum::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArrestOrgs()
    {
        return $this->hasMany(ArrestOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalanceOrgs()
    {
        return $this->hasMany(BalanceOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankrotOrgs()
    {
        return $this->hasMany(BankrotOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoundersLegalOrgs()
    {
        return $this->hasMany(FoundersLegalOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoundersFlOrgs()
    {
        return $this->hasMany(FoundersFlOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFsspOrgs()
    {
        return $this->hasMany(FsspOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGcOrgs()
    {
        return $this->hasMany(GcOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagerFlOrgs()
    {
        return $this->hasMany(ManagerFlOrg::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerroristPeoples()
    {
        return $this->hasMany(TerroristPeople::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBadContracts()
    {
        return $this->hasMany(BadContract::className(), ['irbis_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgeOrgs()
    {
        return $this->hasMany(JudgeOrg::className(), ['irbis_request_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'request_id']);
    }

    public function checkTypeBankrot()
    {
        foreach ($this->bankrotOrgs as $item) {
            if ($item->type_request == BankrotOrg::TYPE_REQUEST_INN) {
                return true;
            }
        }
        return false;
    }

}
