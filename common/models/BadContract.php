<?php

namespace common\models;

/**
 * This is the model class for table "bad_contract".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $date_start
 * @property string $date_end
 * @property string $date_contract
 * @property string $sum
 * @property string $url
 * @property string $reason
 * @property string $registry_number
 *
 * @property IrbisRequest $irbisRequest
 */
class BadContract extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bad_contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date_start', 'date_end', 'date_contract'], 'safe'],
            [['sum', 'url', 'reason', 'registry_number'], 'string', 'max' => 255],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Irbis Request ID',
            'date_start' => 'Дата внесения в реестр',
            'date_end' => 'Дата исключения',
            'date_contract' => 'Дата заключения контракта',
            'sum' => 'Сумма',
            'url' => 'Url',
            'reason' => 'Причина',
            'registry_number' => 'Реестровый номер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
