<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "arbitr_sum".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property integer $type
 * @property string $sum
 * @property integer $count
 * @property string $year
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class ArbitrSum extends \yii\db\ActiveRecord
{
    /**
     * Истец
     */
    const TYPE_PLAINTIFF = 1;
    /**
     * Ответчик
     */
    const TYPE_DEFENDANT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arbitr_sum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['year', 'sum', 'type', 'count'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Request',
            'type' => 'Тип участника',
            'count' => 'Количество',
            'sum' => 'Сумма',
            'year' => 'Год',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }

    /**
     * Тип участника
     *
     * @return string
     */
    public function getTypeMember()
    {
        if ($this->type == self::TYPE_PLAINTIFF){
            return 'Истец';
        }
        return 'Ответчик';
    }
}
