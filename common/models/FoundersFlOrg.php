<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "founders_fl_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $fio
 * @property string $inn
 * @property string $grn_date
 * @property string $share_capital
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class FoundersFlOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'founders_fl_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['grn_date'], 'safe'],
            [['fio', 'inn', 'share_capital'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Org',
            'fio' => 'Fio',
            'inn' => 'Inn',
            'grn_date' => 'Grn Date',
            'share_capital' => 'Share Capital',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
