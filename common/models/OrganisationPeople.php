<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "organisation_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $name
 * @property string $inn
 * @property string $ogrn
 * @property string $address
 * @property string $position
 * @property string $status
 *
 * @property IrbisRequest $idIrbisRequest
 */
class OrganisationPeople extends \yii\db\ActiveRecord
{
    const STATUS_ACTION = 0;
    const STATUS_DISABLE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['name', 'inn', 'ogrn', 'address', 'position', 'status'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_irbis_people' => 'Id Irbis People',
            'name' => 'Name',
            'inn' => 'Inn',
            'ogrn' => 'Ogrn',
            'address' => 'Address',
            'position' => 'Position',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }

    public function getStatus(){
        if ($this->status == self::STATUS_ACTION){
            return 'Действующий';
        }
        return 'Исключен';
    }
}
