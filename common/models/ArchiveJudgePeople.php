<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "archive_judge_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $head
 *
 * @property IrbisRequest $idIrbisRequest
 */
class ArchiveJudgePeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'archive_judge_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['head'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis People',
            'head' => 'Head',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
