<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "credit_score".
 *
 * @property integer $id
 * @property integer $score
 * @property string $reason_1
 * @property string $reason_2
 * @property string $reason_3
 * @property string $reason_4
 * @property integer $request_id
 * @property integer $created_at
 *
 * @property Request $request
 */
class CreditScore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'credit_score';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id'], 'required'],
            [['request_id'], 'integer'],
            [['reason_1', 'reason_2', 'reason_3', 'reason_4'], 'string'],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::className(), 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'score' => 'Скоринг',
            'reason_1' => 'Причина снижения балла 1',
            'reason_2' => 'Причина снижения балла 2',
            'reason_3' => 'Причина снижения балла 3',
            'reason_4' => 'Причина снижения балла 4',
            'request_id' => 'ID запроса из request',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'request_id']);
    }
}
