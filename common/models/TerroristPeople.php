<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "arbitr_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $name
 * @property string $birth_date
 * @property string $birth_place
 *
 * @property IrbisRequest $idIrbisRequest
 */
class TerroristPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terrorist_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['birth_place', 'name', 'birth_date'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis People',
            'name' => 'Name',
            'birth_date' => 'Birth Date',
            'birth_place' => 'Birth Place',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
