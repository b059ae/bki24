<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "judge_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $date
 * @property string $judge
 * @property string $type_cause
 * @property string $description
 * @property string $case_number
 * @property string $end_date
 * @property string $resolution
 * @property string $court_name
 * @property string $role
 *
 * @property IrbisRequest $idIrbisRequest
 */
class JudgePeople extends \yii\db\ActiveRecord
{
    /**
     * Административные дела
     */
    const TYPE_ADMINISTRATION_CASE = "A";
    /**
     * Уголовные дела
     */
    const TYPE_CRIMINAL_CASE = "U";
    /**
     * Граждансие дела
     */
    const TYPE_CIVIL_CASE = "G";
    /**
     * Оспаривание решений
     */
    const TYPE_CHALLENGE_CASE = "O";

    /**
     * Подозреваемый/Ответчик
     */
    const ROLE_DEF = 'def';
    /**
     * Истец
     */
    const ROLE_PLAINTIFF = 'plan';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judge_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['description', 'type_cause', 'case_number', 'end_date', 'resolution', 'court_name', 'role'], 'safe'],
            [['date', 'judge'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_irbis_people' => 'Id Irbis People',
            'date' => 'Дата начала',
            'judge' => 'Судья',
            'type_cause' => 'Тип процесса',
            'description' => 'Ход дела',
            'case_number' => 'Номер дела',
            'end_date' => 'Дата окончания',
            'resolution' => 'Постановление',
            'court_name' => 'Суд',
            'role' => 'Роль',
        ];
    }

    public function getTypeArray(){
        return [
            self::TYPE_ADMINISTRATION_CASE => 'Административное дело',
            self::TYPE_CRIMINAL_CASE => 'Уголовное дело',
            self::TYPE_CIVIL_CASE => 'Гражданское дело',
            self::TYPE_CHALLENGE_CASE => 'Оспаривание решений'
        ];
    }
    /**
     * Получение типа
     */
    public function getType()
    {
        return ArrayHelper::getValue($this->getTypeArray(), $this->type_cause);
    }

    public function getRoleArray(){
        return [
            self::ROLE_DEF => 'Ответчик',
            self::ROLE_PLAINTIFF => 'Истец',
        ];
    }
    /**
     * Получение Имени статуса
     */
    public function getRole()
    {
        return ArrayHelper::getValue($this->getRoleArray(), $this->role);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
