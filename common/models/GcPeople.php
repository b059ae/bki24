<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "gc_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $date
 * @property string $sum
 * @property string $status
 * @property string $result
 * @property string $customer
 * @property string $mobCol
 *
 * @property IrbisRequest $idIrbisRequest
 */
class GcPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gc_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date'], 'safe'],
            [['result', 'customer', 'sum', 'status', 'mob_col'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis People',
            'Result' => 'Result',
            'Customer' => 'Customer',
            'date' => 'Date',
            'sum' => 'Sum',
            'status' => 'Status',
            'mob_col' => 'Mob Col',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
