<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 05.09.17
 * Time: 16:54
 */

namespace common\models;


class Services
{
    /** Ирбис ir-bis.org */
    CONST IRBIS = 1;
    /** Скоринг НБКИ nbki.ru */
    CONST NBKI = 2;

    public static function getArray(){
        return [
            self::IRBIS,
            self::NBKI,
        ];
    }
}