<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "fssp_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $name
 * @property string $code
 * @property string $fssp_department
 * @property string $subject
 * @property string $date_end
 * @property string $sum
 *
 * @property IrbisRequest $idIrbisRequest
 */
class FsspPeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fssp_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date_end'], 'safe'],
            [['name', 'code', 'fssp_department', 'subject', 'sum'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis People',
            'name' => 'Name',
            'code' => 'Code',
            'fssp_department' => 'Fssp Department',
            'subject' => 'Subject',
            'date_end' => 'Date End',
            'sum' => 'Sum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
