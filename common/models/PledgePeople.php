<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "pledge_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $fio
 * @property string $passport
 * @property string $birth_date
 * @property string $pawnbroker
 * @property string $type_pledge
 * @property string $date_add
 * @property string $date_end
 *
 * @property IrbisRequest $idIrbisRequest
 */
class PledgePeople extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pledge_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['birth_date', 'date_add', 'date_end'], 'safe'],
            [['fio', 'passport', 'pawnbroker', 'type_pledge'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Request',
            'fio' => 'Fio',
            'passport' => 'Passport',
            'birth_date' => 'Birth Date',
            'pawnbroker' => 'Pawnbroker',
            'type_pledge' => 'Type Pledge',
            'date_add' => 'Date Add',
            'date_end' => 'Date End',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
