<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "balance_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $sales
 * @property string $net_profit
 * @property string $year
 * @property string $balance
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class BalanceOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['sales', 'net_profit', 'year', 'balance'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_irbis_request' => 'Id Запроса',
            'sales' => 'Выручка',
            'net_profit' => 'Чистая прибыль',
            'balance' => 'Баланс',
            'year' => 'Год',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
