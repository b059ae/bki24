<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "manager_fl_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $fio
 * @property string $inn
 * @property string $grn_date
 * @property string $role_name
 * @property string $role_type
 * @property string $phones
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class ManagerFlOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manager_fl_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['grn_date'], 'safe'],
            [['fio', 'inn', 'role_name', 'role_type', 'phones'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_irbis_org' => 'Id Irbis Org',
            'fio' => 'Fio',
            'inn' => 'Inn',
            'grn_date' => 'Grn Date',
            'role_name' => 'Role Name',
            'role_type' => 'Role Type',
            'phones' => 'Phones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }

    public function getManagerUrl(){
        $irbisPeople = IrbisPeople::findOne(['manager_id' => $this->id]);
        if ($irbisPeople){
            return $irbisPeople->irbis_request_id;
        }
        return false;
    }
}
