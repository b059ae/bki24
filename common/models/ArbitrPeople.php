<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "arbitr_people".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $member
 * @property string $type_member
 * @property string $judge
 * @property string $date
 * @property string $url
 * @property string $name
 * @property IrbisRequest $idIrbisRequest
 */
class ArbitrPeople extends \yii\db\ActiveRecord
{
    const URL = 'http://kad.arbitr.ru/Card/';
    const TYPE_MEMBER_R = 'R';
    const TYPE_MEMBER_P = 'P';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arbitr_people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date', 'url', 'name'], 'safe'],
            [['member', 'type_member', 'judge'], 'string', 'max' => 255],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis People',
            'member' => 'Member',
            'type_member' => 'Type Member',
            'judge' => 'Judge',
            'date' => 'Date',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }

    /**
     * Тип участника арбитражного суда
     * @return string
     */
    public function getTypeMember(){
        if ($this->type_member == self::TYPE_MEMBER_P){
            return 'Истец';
        }
        return 'Ответчик';
    }

    /**
     * Тип участника арбитражного суда
     *
     * @return string
     */
    public function getFullUrl()
    {
        return self::URL . $this->url;
    }
}
