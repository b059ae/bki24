<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "founders_legal_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $ogrn
 * @property string $inn
 * @property string $name
 * @property string $grn_date
 * @property string $share_capital
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class FoundersLegalOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'founders_legal_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['grn_date', 'share_capital'], 'safe'],
            [['ogrn', 'inn', 'name'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Org',
            'ogrn' => 'Ogrn',
            'inn' => 'Inn',
            'name' => 'Name',
            'grn_date' => 'Grn Date',
            'share_capital' => 'Доля',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
