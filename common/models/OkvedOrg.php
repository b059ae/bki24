<?php

namespace common\models;


use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "okved_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $code
 * @property string $name
 * @property int $type
 *
 * @property IrbisRequest $idIrbisRequest
 */
class OkvedOrg extends \yii\db\ActiveRecord
{
    /**
     * Основная деятельность
     */
    const BASE = 1;
    /**
     * Дополнительная деятельность
     */
    const ADDITIONAL = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'okved_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['code', 'name', 'type'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Irbis Request',
            'code' => 'Code',
            'name' => 'Name',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }

    /**
     * Название типа
     *
     * @return string
     */
    public function getTypeName()
    {
        return ArrayHelper::getValue([
            self::BASE => "Основная деятельность",
            self::ADDITIONAL => "Дополнительная деятельность",
        ], $this->type);
    }
}
