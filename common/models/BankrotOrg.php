<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bankrot_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property integer $type_request
 * @property string $bankrot
 * @property string $address
 * @property string $judge
 * @property string $date_bankrot
 * @property string $case_number
 * @property string $url
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class BankrotOrg extends \yii\db\ActiveRecord
{
    const TYPE_REQUEST_NAME = 1;
    const TYPE_REQUEST_INN = 2;
    const URL = 'http://kad.arbitr.ru/Card/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bankrot_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['bankrot', 'address', 'judge', 'date_bankrot', 'type_request', 'case_number', 'url'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id запроса',
            'type_request' => 'Тип запроса',
            'bankrot' => 'Банкрот',
            'address' => 'Адрес',
            'judge' => 'Судебный участок',
            'date_bankrot' => 'Дата',
            'case_number' => 'Номер дела',
            'url' => 'url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
