<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "arbitr_org".
 *
 * @property integer $id
 * @property integer $irbis_request_id
 * @property string $type_request
 * @property string $member
 * @property string $address
 * @property string $type_member
 * @property string $judge
 * @property string $date
 * @property string $url
 * @property string $name
 *
 * @property  IrbisRequest $idIrbisRequest
 */
class ArbitrOrg extends \yii\db\ActiveRecord
{
    const TYPE_REQUEST_NAME = 1;
    const TYPE_REQUEST_INN = 2;
    const TYPE_MEMBER_R = 'R';
    const TYPE_MEMBER_P = 'P';

    const SITE_URL = 'http://kad.arbitr.ru/Card/';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arbitr_org';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['irbis_request_id'], 'integer'],
            [['date', 'url', 'name', 'type_request', 'member', 'address', 'type_member', 'judge'], 'safe'],
            [['irbis_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => IrbisRequest::className(), 'targetAttribute' => ['irbis_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'irbis_request_id' => 'Id Запроса',
            'type_request' => 'Тип запроса',
            'name' => 'Номер дела',
            'member' => 'Участник',
            'address' => 'Адрес',
            'type_member' => 'Тип участника',
            'judge' => 'Судебный участок',
            'date' => 'Дата',
            'url' => 'Url',
        ];
    }

    /**
     * Тип участника арбитражного суда
     *
     * @return string
     */
    public function getTypeMember()
    {
        if ($this->type_member == self::TYPE_MEMBER_P){
            return 'Истец';
        }
        return 'Ответчик';
    }

    /**
     * Тип участника арбитражного суда
     *
     * @return string
     */
    public function getFullUrl()
    {
        return self::SITE_URL . $this->url;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIrbisRequest()
    {
        return $this->hasOne(IrbisRequest::className(), ['id' => 'irbis_request_id']);
    }
}
