<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.08.17
 * Time: 12:56
 */

namespace common\models\interfaces;


interface RequestInterface
{
    // Не готов
    const STATUS_NOT_READY = 0;
    // Частично готов
    const STATUS_PARTIALLY_READY = 2;
    // Полностью готов
    const STATUS_FULL_READY = 3;
    // Ошибка
    const STATUS_ERROR = 99;
    // Запрос по физ лицам
    const REQUEST_PEOPLE = 1;
    // Запрос по юр лицам
    const REQUEST_LEGAL = 2;
}