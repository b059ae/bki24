<?php

namespace common\queue;

use common\components\irbis\IrbisOrg;
use common\components\irbis\IrbisPeople;
use common\components\nbki\NbkiPeople;
use common\models\interfaces\QueryInterface;
use common\models\Org;
use common\models\Request;
use common\models\Services;
use yii\base\Object;

/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.08.17
 * Time: 17:57
 */

/**
 * Запрос информации о юрлице
 * Class OrgJob
 * @package common\queue
 */
class PeopleJob extends Object implements \yii\queue\Job
{
    /** @var integer */
    public $request_id;

    public function execute($queue)
    {
        try {
            /** @var Request $res */
            $res = Request::find()
                ->andWhere(['id' => $this->request_id])
                ->andWhere(['type' => Request::REQUEST_PEOPLE])
                ->andWhere(['status' => Request::STATUS_NOT_READY])
                ->limit(1)
                ->one();
            if (!$res) {
                throw new \Exception('Не найден request_id' . $this->request_id);
            }
            // Cтатус выполняется
            $res->status = Request::STATUS_PARTIALLY_READY;
            $res->save();

            // Извлекаем список услуг
            $services = json_decode($res->services);

            if (in_array(Services::IRBIS, $services)) {
                // Получение данных из Ирбис
                $irbis = new IrbisPeople([
                    'field' => [
                        'request_id' => $res->id,
                        'firstName' => $res->first_name,
                        'secondName' => $res->middle_name,
                        'lastName' => $res->last_name,
                        'serPassport' => $res->passport_series,
                        'numPassport' => $res->passport_number,
                        'regions' => $res->region_id,
                        'birthDate' => \Yii::$app->formatter->asDate($res->birth_date, 'php:d.m.Y'),
                        'inn' => $res->inn
                    ]
                ]);
                $result = $irbis->getUuid();
                $irbis->individual($result);
            }

            if (in_array(Services::NBKI, $services)) {
                // Получение данных из НБКИ
                $nbki = new NbkiPeople(
                    $res->id,
                    $res->first_name,
                    $res->middle_name,
                    $res->last_name,
                    $res->birth_date,
                    $res->passport_series,
                    $res->passport_number,
                    \Yii::$app->nbkiSp,
                    \Yii::$app->cryptoPro,
                    \Yii::$app->file
                );
                $nbki->scoring();
            }

            // Статус готов
            $res->status = Request::STATUS_FULL_READY;
            $res->save();
        } catch (\Exception $e) {
            // Отправка сообщения админку
            throw $e;
        }
    }
}