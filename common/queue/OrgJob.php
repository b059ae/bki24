<?php

namespace common\queue;

use common\components\irbis\IrbisOrg;
use common\models\interfaces\QueryInterface;
use common\models\Org;
use common\models\Request;
use common\models\Services;
use yii\base\Object;

/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.08.17
 * Time: 17:57
 */

/**
 * Запрос информации о юрлице
 * Class OrgJob
 * @package common\queue
 */
class OrgJob extends Object implements \yii\queue\Job
{
    /** @var integer */
    public $request_id;

    public function execute($queue)
    {
        try {
            /** @var Request $res */
            $res = Request::find()
                ->andWhere(['id' => $this->request_id])
                ->andWhere(['type' => Request::REQUEST_LEGAL])
                ->andWhere(['status' => Request::STATUS_NOT_READY])
                ->limit(1)
                ->one();
            if (!$res) {
                throw new \Exception('Не найден request_id' . $this->request_id);
            }

            // Cтатус выполняется
            $res->status = Request::STATUS_PARTIALLY_READY;
            $res->save();

            // Извлекаем список услуг
            $services = json_decode($res->services);

            if (in_array(Services::IRBIS, $services)){
                // Получение данных из Ирбис
                $irbis = new IrbisOrg([
                    'field' => [
                        'request_id' => $res->id,
                        'inn' => $res->inn,
                        'founders' => false,
                        'manager' => false
                    ]
                ]);
                $result = $irbis->getUuid();
                $id = $irbis->legal($result);
            }

           /*if (in_array(Services::NBKI, $services)){
                // Получение данных из НБКИ
            }*/

            // Статус готов
            $res->status = Request::STATUS_FULL_READY;
            $res->save();
        } catch (\Exception $e) {
            // Отправка сообщения админу
            throw $e;
        }
    }

}